# ------------------------------------------------------------------------------
# Perfrom analysis of state estimation algorithm performance
# ------------------------------------------------------------------------------
# ASEN 4028: VISION
# ------------------------------------------------------------------------------
# Author: Ben Hagenau
# Created: 4/8/20
import numpy as np
import statistics as stats
from itertools import product
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib import rcParams
from matplotlib import rc
from itertools import combinations
from scipy.integrate import odeint
import sys
from StateEstimation.analysis_support import *

# --- Select -------------------------------------------------------------------
runIDs = [1, 2, 3, 4, 5]
scenarios = ["sats1_ecc0_00", "sats1_ecc0_05", "sats1_ecc0_10", "sats1_ecc0_15", "sats1_ecc0_20"]
# scenarios = ["sats1_ecc0_00", "sats1_ecc0_05", "sats1_ecc0_10", "sats1_ecc0_15"]
# scenarios = ["sats1_ecc0_00", "sats1_ecc0_05", "sats1_ecc0_10"]
# scenarios = ["sats1_ecc0_00", "sats1_ecc0_05"]
# scenarios = ["sats1_ecc0_00"]

colors = ['red', 'blue', 'orange', 'purple', 'green']
title = 'Filter Performance with Eccentricity'
labels = ['Deployer Eccentricity', 'Final Error [m]']
# --- Initialize ---------------------------------------------------------------
NEES = [] # NEES for each X0
error0 = [] # initial estimates
std0 = [] # store
oeError = []
finalPropError = []
# fig, ax = plt.subplots(1,1)
for itScenario, scenario in enumerate(scenarios):
    for itRun, runID in enumerate(runIDs):
    # --- Import Data ----------------------------------------------------------
        satellites, deployer, trueSatellites, t, trueOrbitFrame, centroids, trueSensorFrame = ImportData(scenario, runID)
    # --- Initialize Filter Parameters -----------------------------------------
        R, P0, X0 = InitializeFilterParams(trueOrbitFrame, [0.018e-3, 0.028e-3, 0.038e-3], rSigEst=[], vSigEst=[]) # 2.9e-5
        # R, P0, X0 = InitializeFilterParams(trueOrbitFrame, [0.014e-3, 0.025e-3, 0.036e-3], rSigEst=[], vSigEst=[]) # 2.9e-5
    # --- Estimate State -------------------------------------------------------
        orbitFrameMeasurements, orbitFrameEstimates, orbitFrameCovariance, inertialFrameEstimates, nlb =\
        RunStateEstimation(satellites[0], trueSatellites[0], deployer, X0, P0, R)
    # --- Compute Errors -------------------------------------------------------
        coeEstimateAbsoluteError = ComputeEstimateError(satellites[0].coe, trueSatellites[0].coe)
        orbitFrameEstimateError = ComputeEstimateError(orbitFrameEstimates, trueOrbitFrame[:,[1,2,3,4,5,6]])
        oeError.append(np.abs(coeEstimateAbsoluteError))
    # --- NEES Testing ----------------------------------------------------------------
        NEES.append(np.dot(orbitFrameEstimateError[0], np.linalg.inv(orbitFrameCovariance[0])) @ orbitFrameEstimateError[0])
        error0.append(orbitFrameEstimateError[0])
        std0.append(np.sqrt(np.diag(orbitFrameCovariance[0])))
        print("SMA: {} [m]".format(np.asarray(coeEstimateAbsoluteError[0])*1000))
        # i = 0
        # ax.plot(orbitFrameEstimateError[:,i]*1000)
        # ax.plot([2*np.asarray([np.sqrt(P[i,i])])*1000 for P in orbitFrameCovariance],'k--')
        # ax.plot([-2*np.asarray([np.sqrt(P[i,i])])*1000 for P in orbitFrameCovariance],'k--')
        if itRun==0 and itScenario == 0:
            measurementError = (centroids[:,[1,2,3]] - trueSensorFrame[:,[1,2,3]])*1000
        else:
            measurementError = np.vstack((measurementError, (centroids[:,[1,2,3]] - trueSensorFrame[:,[1,2,3]])*1000))
        # nlb.PlotRVEstimateError(trueOrbitFrame[:,[1,2,3]], trueOrbitFrame[:,[4,5,6]])
        # ax.plot(centroids[:,-1], centroids[:,3]*1000 - trueSensorFrame[:,3]*1000)
        # plt.show()
        # print('-------------------')
        # print('({0},  {1})'.format(np.sqrt(orbitFrameCovariance[0][1,1])*1000,\
        #                     np.sqrt(orbitFrameCovariance[0][0,0] + orbitFrameCovariance[0][2,2])*1000))
        satellite = satellites[0]
        trueSatellite = trueSatellites[0]
        def from_vec(r_vec, v_vec):
            r = np.linalg.norm(r_vec)
            # unit orbit radial in inertial coordinates
            r_hat = r_vec/r
            # unit orbit normal in inertial coordinates
            h_vec = np.cross(r_vec,v_vec)
            h = np.linalg.norm(h_vec)
            h_hat = h_vec/h
            # unit orbit tangent in inertial coordinates
            theta_hat = np.cross(h_hat, r_hat)
            # construct DCM
            ON = np.array([r_hat, theta_hat, h_hat])

            return ON
        def dfdz(X, mu, J2, r_e): # X: [x,y,z,xd,yd,zd] (satellite ECI state)
            x = X[0]
            y = X[1]
            z = X[2]
            xd = X[3]
            yd = X[4]
            zd = X[5]
            A = np.array(([[0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1], [mu*(12*J2*r_e**2*x**4 + 9*J2*r_e**2*x**2*y**2 - 81*J2*r_e**2*x**2*z**2 - 3*J2*r_e**2*y**4 + 9*J2*r_e**2*y**2*z**2 + 12*J2*r_e**2*z**4 + 4*x**6 + 6*x**4*y**2 + 6*x**4*z**2 - 2*y**6 - 6*y**4*z**2 - 6*y**2*z**4 - 2*z**6)/(2*np.sqrt(x**2 + y**2 + z**2)*(x**8 + 4*x**6*y**2 + 4*x**6*z**2 + 6*x**4*y**4 + 12*x**4*y**2*z**2 + 6*x**4*z**4 + 4*x**2*y**6 + 12*x**2*y**4*z**2 + 12*x**2*y**2*z**4 + 4*x**2*z**6 + y**8 + 4*y**6*z**2 + 6*y**4*z**4 + 4*y**2*z**6 + z**8)), 3*mu*x*y*(-12*J2*r_e**2*z**2*(x**2 + y**2)**2 - 8*J2*r_e**2*z**2*(z**4 - 2*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) - 5*J2*r_e**2*(x**2 + y**2)**2*(-x**2 - y**2 + 2*z**2) + 2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**(9/2)), 3*mu*x*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0], [3*mu*x*y*(-12*J2*r_e**2*z**2*(x**2 + y**2)**2 - 8*J2*r_e**2*z**2*(z**4 - 2*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) - 5*J2*r_e**2*(x**2 + y**2)**2*(-x**2 - y**2 + 2*z**2) + 2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**(9/2)), mu*(-3*J2*r_e**2*x**4 + 9*J2*r_e**2*x**2*y**2 + 9*J2*r_e**2*x**2*z**2 + 12*J2*r_e**2*y**4 - 81*J2*r_e**2*y**2*z**2 + 12*J2*r_e**2*z**4 - 2*x**6 - 6*x**4*z**2 + 6*x**2*y**4 - 6*x**2*z**4 + 4*y**6 + 6*y**4*z**2 - 2*z**6)/(2*np.sqrt(x**2 + y**2 + z**2)*(x**8 + 4*x**6*y**2 + 4*x**6*z**2 + 6*x**4*y**4 + 12*x**4*y**2*z**2 + 6*x**4*z**4 + 4*x**2*y**6 + 12*x**2*y**4*z**2 + 12*x**2*y**2*z**4 + 4*x**2*z**6 + y**8 + 4*y**6*z**2 + 6*y**4*z**4 + 4*y**2*z**6 + z**8)), 3*mu*y*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0], [3*mu*x*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 3*mu*y*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), mu*(36*J2*r_e**2*z**2*(x**2 + y**2) - 15*J2*r_e**2*z**2*(-x**2 - y**2 + 2*z**2) + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2)*(x**2 + y**2 + z**2) - 6*J2*r_e**2*(4*z**4 - 5*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 6*z**2*(x**2 + y**2 + z**2)**2 - 2*(x**2 + y**2 + z**2)**3)/(2*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0]]))
            return A
        def odefun(z_vec, t, mu, J2, r_e):
            x = z_vec[0]
            y = z_vec[1]
            z = z_vec[2]
            xd = z_vec[3]
            yd = z_vec[4]
            zd = z_vec[5]
            Phi = np.reshape(z_vec[6:],(6,6))
            # nonlinear dynamics
            f = np.array([xd, yd, zd,\
                 mu*x*(6*J2*r_e**2*z**2 + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2)),\
                 mu*y*(6*J2*r_e**2*z**2 + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2)),\
                 mu*z*(-6*J2*r_e**2*(x**2 + y**2) + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2))])
            # propogate state transition matrix
            A = dfdz(z_vec[0:6], mu, J2, r_e)
            Phid = A@Phi
            zd = np.hstack((f, np.reshape(Phid,(-1))))
            return zd
        params = {"mu" : 3.986004415e5, "r_e" : 6378, "J2" : 0.0010826269}
        X0_estimate = np.hstack((satellite.cartesian.r[0], satellite.cartesian.v[0]))
        X0_true = np.hstack((trueSatellite.cartesian.r[0], trueSatellite.cartesian.v[0]))
        T = 2*np.pi*np.sqrt(trueSatellite.coe[0]**3/params["mu"])
        # n = 10
        days = 1
        t_vec = np.linspace(0, days*86400, 1000)
        day_vec = t_vec/86400
        z0_estimate = np.hstack((X0_estimate, np.reshape(np.identity(6),(-1))))
        z0_true = np.hstack((X0_true, np.reshape(np.identity(6),(-1))))
        X_estimate = odeint(lambda X, t: odefun(X,t, **params), z0_estimate, t_vec, rtol=1e-10, atol=1e-10)
        X_true = odeint(lambda X, t: odefun(X,t, **params), z0_true, t_vec, rtol=1e-10, atol=1e-10)
        # rotate covariance estimate into truth orbit frame
        DN = from_vec(np.asarray(deployer.cartesian.r[0]), np.asarray(deployer.cartesian.v[0]))
        ON = from_vec(X_true[0,:3], X_true[0,[3,4,5]])
        OD = ON @ DN.transpose()
        P0 = nlb.P[0][:3,:3] # position P0 in deployer orbit frame
        P0 = OD @ P0 @ OD.transpose() # position P0 in truth orbit frame
        # propogation error in truth orbit frame
        rho = []
        P = []
        std = []
        e =[]
        e_std = []
        for i in range(0, len(t_vec)):
            Phi = np.reshape(X_true[i,6:], (6,6))
            # truth frame stm
            Pi = Phi[:3,:3] @ P0 @ Phi[:3,:3].transpose()
            std.append(np.sqrt(np.diag(Pi).tolist())*1000)
            # truth orbit frame error
            dR = X_estimate[i,:3] - X_true[i,:3]
            ON = from_vec(X_true[i,:3], X_true[i,[3,4,5]])
            rho.append(ON @ dR)
            e.append(np.linalg.norm(rho[-1]))
            e_std.append(np.linalg.norm(std[-1]))
        std = np.asarray(std)
        rho = np.asarray(rho)*1000
        e = np.asarray(e)*1000
        e_std = np.asarray(e_std)

        finalPropError.append(e[-1])

        # font = {'size':'20'}
        # rcParams['axes.labelpad'] = 0
        # title = 'Propogation Error'
        # fig = plt.figure()
        # plt.plot(day_vec*24, e, 'k', label='net error')
        # plt.plot(day_vec*24, 3*e_std+e,'--k', label='+3$\sigma$')
        # plt.plot(day_vec*24, -3*e_std+e,'--k')
        # plt.xlabel('Hours Since Deployment', **font)
        # plt.ylabel('Error [m]', **font)
        # plt.title('Propogation Error: $\delta$SMA = {:3.3}: '.format(np.abs(coeEstimateAbsoluteError[0])*1000), **font)
        # plt.legend()

print(np.mean(np.asarray(finalPropError)))
print(np.std(np.asarray(finalPropError)))
print(np.max(np.asarray(finalPropError)))
print(np.min(np.asarray(finalPropError)))
# print(np.mean(np.asarray(oeError)[:,0])*1000)
# print(np.std(np.asarray(oeError)[:,0]))
# print(np.mean(np.asarray(oeError)[:,1]))
# print(np.std(np.asarray(oeError)[:,1]))
# print(np.mean(np.asarray(oeError)[:,2]))
# print(np.std(np.asarray(oeError)[:,2]))
# print(np.mean(np.asarray(oeError)[:,3]))
# print(np.std(np.asarray(oeError)[:,3]))
# print(np.mean(np.asarray(oeError)[:,4]))
# print(np.std(np.asarray(oeError)[:,4]))
# print(np.mean(np.asarray(oeError)[:,5]))
# print(np.std(np.asarray(oeError)[:,5]))


# print(np.mean(np.asarray(error0)[:,0]))
# print(np.mean(np.asarray(error0)[:,1]))
# print(np.mean(np.asarray(error0)[:,2]))
# print(np.mean(np.asarray(error0)[:,3]))
# print(np.mean(np.asarray(error0)[:,4]))
# print(np.mean(np.asarray(error0)[:,5]))
# print(np.linalg.norm([np.mean(np.asarray(error0)[:,5]), np.mean(np.asarray(error0)[:,4]), np.mean(np.asarray(error0)[:,3]),\
# np.mean(np.asarray(error0)[:,2]), np.mean(np.asarray(error0)[:,1]), np.mean(np.asarray(error0)[:,0])]))

# print(np.mean(NEES))
# print("SMA: {} [m]".format(np.asarray(coeEstimateAbsoluteError[0])*1000))

# font = {'size':'20'}
# rc('text', usetex=True)
# estimate error
# fig, ax = plt.subplots(3,1)
# for i in range(0,3):
#     ax[i].plot(np.asarray(error0)[:,i]*1000, label='Error')
#     ax[i].plot(3*np.asarray(std0)[:,i]*1000,'--k', label=r'$\pm 3 \sigma$')
#     ax[i].plot(-3*np.asarray(std0)[:,i]*1000,'--k')
# fig.suptitle('Initial Velocity Estimate Error', **font)
# ax[2].set_xlabel('Monte-Carlo Iteration', **font)
# ax[1].set_ylabel('Error [m]', **font)
# ax[0].legend()
#
# fig, ax = plt.subplots(3,1)
# for i in range(0,3):
#     ax[i].plot(np.asarray(error0)[:,i+3]*1000, label='Error')
#     ax[i].plot(3*np.asarray(std0)[:,i+3]*1000,'--k', label=r'$\pm 3 \sigma$')
#     ax[i].plot(-3*np.asarray(std0)[:,i+3]*1000,'--k')
# fig.suptitle('Initial Velocity Estimate Error', **font)
# ax[2].set_xlabel('Monte-Carlo Iteration', **font)
# ax[1].set_ylabel('Error [m/s]', **font)
# ax[0].legend()

# NEES Estimation Results
# fig, ax = plt.subplots(1,1)
# ax.hlines(4.7194, 0, 25, linestyle='--',color='k')
# ax.hlines(7.4320, 0, 25, linestyle='--',color='k')
# ax.plot(NEES,'.')
plt.show()













# # --- Print --------------------------------------------------------------------
#     print('-------------- COE Absolute Error --------------')
#     print("SMA: {} [m]".format(np.asarray(coeEstimateAbsoluteError[0])*1000))
#     print("ECC: {}".format(coeEstimateAbsoluteError[1]))
#     print("INC: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[2])))
#     print("RAAN: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[3])))
#     print("AOP: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[4])))
#     print("TA: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[5])))
