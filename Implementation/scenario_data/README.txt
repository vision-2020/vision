Store files required to perform state estimation

(1) measurements.csv - Sensor Frame centeroids for each satellite and meta-data
	[satID, x, y, z, time]
(2) deployer_data.csv - Deployer data in the inertial frame
	[x, y, z, xd, yd, zd, thetax, thetay, thetaz, omegax, omegay, omegaz, time]
(3) mission_data.csv - additional information about the mission scenario
	[position, orientation]
	[satID, TLE meta-data...]
(4) true_orbit_frame.csv - true satellite states in the orbit frame
	[satID, x, y, z, time]
(5) true_inertial_frame.csv - true satellite states in the inertial frame
	[satID, x, y, z, time]
