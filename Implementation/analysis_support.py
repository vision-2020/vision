# ------------------------------------------------------------------------------
# Methods used to perform state estimation algorithm analysis
# ------------------------------------------------------------------------------
# ASEN 4028: VISION
# ------------------------------------------------------------------------------
# Author: Ben Hagenau
# Created: 4/8/20

import numpy as np
from random import gauss
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
from .Format import format_satellite_data, format_deployer

# ------------------------------------------------------------------------------
# Compute estimate errors
# ------------------------------------------------------------------------------
# INPUTS:
#   estimated: etimated quantity
#   true: true quantity
# OUTPUTS:
#   error: estiamte error
def ComputeEstimateError(estimated, true):
    error = estimated - true
    return error

# ------------------------------------------------------------------------------
# Run state estimation algorithm
# ------------------------------------------------------------------------------
# INPUTS:
#   satellites: list of populated satellite objects with centroids
#   trueSatellites: list of satellite objects with true coe
#   deployer: populated deployer object with interpolated states
#   R: measuremet covariance
#   P0: initial estimate covariance
#   X0: nominal initial state
# OUTPUTS:
#   orbitFrameMeasurements: measurements in the orbit frame
#   orbitFrameEstimatesinertial: list of orbit frame state estimates for each satellite
#   orbitFrameCovariance: list of orbit frame estimate covariance for each satellite
#   inertialFrameEstimates: list of inertial frame estimates for each satellite
#   nlb: list of filte robjects for each satellite
def RunStateEstimation(satellite, trueSatellite, deployer, X0, P0, R):
    # sensor frame -> orbit frame
    orbitFrameMeasurements = satellite.sensor_to_orbit(deployer).r
    # estimation
    # state = satellite.kalman(deployer)
    # orbitFrameEstimates.append(np.hstack((state.r, state.v)))
    # orbitFrameCovariance.append([])
    nlb = satellite.estimate_state(deployer, X0, R, P0)
    orbitFrameEstimates = nlb.estimates
    orbitFrameCovariance = nlb.P
    # orbit frame -> inertial frame
    inertialFrameEstimates = satellite.orbit_to_inertial(deployer)
    # orbital elements
    satellite.get_coe()
    satellite.estimate_coe(deployer)
    # two-line elements
    satellite.get_tle()

    return orbitFrameMeasurements, orbitFrameEstimates, orbitFrameCovariance, inertialFrameEstimates, nlb
# ------------------------------------------------------------------------------
# Initialize filter parameters
# ------------------------------------------------------------------------------
# INPUTS:
#   trueOrbitFrame: true orbit frame data (from ImportData)
#   rSigMeas: measurement relative position standard deviation [km]
#   rSigEst: estimate relative position standard deviation
#   VSigEst: estimate relative velocity standard deviation
# OUTPUTS:
#   R: measuremet covariance
#   P0: initial estimate covariance
#   X0: nominal initial state
def InitializeFilterParams(trueOrbitFrame, rSigMeas, rSigEst=[], vSigEst=[]):
    # measurement covariance
    R = np.array([[rSigMeas[0]**2, 0, 0], [0, rSigMeas[1]**2, 0] ,[0, 0, rSigMeas[2]**2]])
    # state covariance and initial guess
    if rSigEst == [] or vSigEst == []:
        P0 = []
    else:
        P0 = np.array([[rSigEst**2, 0, 0, 0, 0, 0], [0, rSigEst**2, 0, 0, 0, 0], [0, 0, rSigEst**2, 0, 0, 0],\
                 [0, 0, 0, vSigEst**2, 0, 0], [0, 0, 0, 0, vSigEst**2, 0], [0, 0, 0, 0, 0, vSigEst**2]])
    dX0 = np.array([gauss(0,0.005e-3), gauss(0,0.005e-3), gauss(0,0.005e-3),\
                    gauss(0,0.01e-3), gauss(0,0.01e-3), gauss(0,0.01e-3)])
    # nominal iniital state
    X0 = np.hstack((trueOrbitFrame[0,[1,2,3]], trueOrbitFrame[0,[4,5,6]]))# + dX0

    return R, P0, X0

# ------------------------------------------------------------------------------
# Import and format measuremet and scenario data
# ------------------------------------------------------------------------------
# INPUTS:
#   scenario: scenario folder name in Data folder
#   simRun: simulation run number for this mission scenario
#   satNum: satellite number
# OUTPUTS:
#   satellites: list of populated satellite objects with centroids
#   deployer: populated deployer object with interpolated states
#   trueSatellites: list of satellite objects with true coe
#   t: measurement time stamps
def ImportData(scenario, simRun, satNum=0):
    # --- read data from files ---
    # deployer inertial data
    deployer_data = np.genfromtxt("Data/"+scenario+"/scenario_data/deployer_data.csv", delimiter=',')
    # mission specific data
    mission_data = np.genfromtxt("Data/"+scenario+"/scenario_data/mission_data.csv", delimiter=',')
    # true relative satellite states in the sensor frame
    true_sensor_frame = np.genfromtxt("Data/"+scenario+"/scenario_data/true_sensor_frame.csv", delimiter=',')
    # true relative satellite states in the deployer orbit frame
    true_orbit_frame = np.genfromtxt("Data/"+scenario+"/scenario_data/true_orbit_frame.csv", delimiter=',')
    # true inerital satellite states in the inertial frame
    true_inertial_frame = np.genfromtxt("Data/"+scenario+"/scenario_data/true_inertial_frame.csv", delimiter=',')
    # satellite centroids in the sensor frame
    centroids = np.genfromtxt("Data/"+scenario+"/centroids/Satellite"+str(satNum)+"_0"+str(simRun)+".csv", delimiter=',')
    centroids[:,[1,2,3]] = (centroids[:,[1,2,3]] - [-0.13306033, 0.003589451452, -0.00313957005])/1000 # m -> km
    # --- truncate truth data --- (cuts truth time stamps that occur before satellite enters field of view)
    rows = np.isin(true_sensor_frame[:,-1], centroids[:,-1])
    true_sensor_frame = true_sensor_frame[rows,:]
    true_orbit_frame = true_orbit_frame[rows,:]
    true_inertial_frame = true_inertial_frame[rows,:]
    rows = np.isin(centroids[:,-1], true_sensor_frame[:,-1])
    centroids = centroids[rows,:]
    # --- build satellite ---
    satellites = format_satellite_data(centroids, frame='S')
    # --- build deployer ---
    deployer = format_deployer(centroids, deployer_data, mission_data)
    deployer.get_coe()
    # --- build trueth satellite ---
    trueSatellites = format_satellite_data(true_inertial_frame, frame='N')
    for sat in trueSatellites:
        sat.get_coe()
        sat.estimate_coe(deployer)
    # --- additional outputs ---
    t = centroids[:,-1]

    return satellites, deployer, trueSatellites, t, true_orbit_frame, centroids, true_sensor_frame
