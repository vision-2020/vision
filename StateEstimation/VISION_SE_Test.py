# ----------------
# Testing framework for VISION state estimation code
# ----------------
# VISION: state estimation
# ----------------
# Author: Ben Hagenau
# Edited: 1/7/20

# Command Line: python VISION_SE_Test.py -v -c

import unittest # https://docs.python.org/3/library/unittest.html
import numpy as np
import scipy.io as spio
from random import uniform as rand
from .Deployer import Deployer
from .Satellite import Satellite
from .State import State
from .Format import *

# ----------------------------------------------------------------
# test sensor_to_orbit() method
# ----------------------------------------------------------------
class state_transformation(unittest.TestCase):
    def setUp(self):
        # scenario definition
        mu = 3.986004415*10**5 # that of earth (VALLADO)
        self.deployer = Deployer()
        self.deployer.attitude.frame = 'D'
        self.deployer.attitude.relative = 'N'
        self.deployer.cartesian.frame = 'N'
        self.deployer.cartesian.relative = 'N'
        self.deployer.coe = np.array([36126.6428, 0.01, np.deg2rad(87.87), np.deg2rad(227.89), np.deg2rad(53.38), np.deg2rad(92.335)]) # [SMA, ECC, INC, RAAN, AOP, TA]
        self.deployer.position = np.array([2, 1, 4])
        self.deployer.orientation = np.deg2rad(np.array([1, 0, 3]))
        self.deployer.attitude.r = [np.deg2rad(np.array([1, 0, 2]))]
        self.deployer.attitude.v = [np.deg2rad(np.array([0, 3, 0]))]
        self.truth_satellite = Satellite(1)
        self.truth_satellite.coe = np.array([36122.6428, 0.015, np.deg2rad(87.7), np.deg2rad(224.89), np.deg2rad(50.38), np.deg2rad(90.335)]) # [SMA, ECC, INC, RAAN, AOP, TA]
        self.satellite = Satellite(0)
        self.truth_orbit_state = State()
        self.truth_inertial_state = State()

        # comptute satellite truth inertial (validated with example in VALLADO p.119)
        p = self.truth_satellite.coe[0]*(1-self.truth_satellite.coe[1]**2) # semi-latus rectum
        R_P = np.array([(p*np.cos(self.truth_satellite.coe[5]))/(1+self.truth_satellite.coe[1]*np.cos(self.truth_satellite.coe[5])), \
                        (p*np.sin(self.truth_satellite.coe[5]))/(1+self.truth_satellite.coe[1]*np.cos(self.truth_satellite.coe[5])), \
                        0]) # perifocal frame position inertial of satellite
        V_P = np.array([-np.sqrt(mu/p)*np.sin(self.truth_satellite.coe[5]), \
                        np.sqrt(mu/p)*(self.truth_satellite.coe[1]+np.cos(self.truth_satellite.coe[5])), \
                        0]) # perifocal frame velocity inertial of satellite
        NP = self.deployer.from_euler('313', [-self.truth_satellite.coe[4], -self.truth_satellite.coe[2], -self.truth_satellite.coe[3]]) # DCM from perifocal to inertial
        self.truth_satellite.cartesian.r = [np.dot(NP, R_P)]
        self.truth_satellite.cartesian.v = [np.dot(NP, V_P)]
        self.truth_satellite.cartesian.frame = 'N'
        self.truth_satellite.cartesian.relative = 'N'
        self.truth_inertial_state = self.truth_satellite.cartesian

        # deployer inertial state from orbital elemenets (validated with example in VALLADO p.119)
        p = self.deployer.coe[0]*(1-self.deployer.coe[1]**2) # semi-latus rectum
        R_P = np.array([(p*np.cos(self.deployer.coe[5]))/(1+self.deployer.coe[1]*np.cos(self.deployer.coe[5])), \
                        (p*np.sin(self.deployer.coe[5]))/(1+self.deployer.coe[1]*np.cos(self.deployer.coe[5])), \
                        0]) # perifocal frame position inertial of satellite
        V_P = np.array([-np.sqrt(mu/p)*np.sin(self.deployer.coe[5]), \
                        np.sqrt(mu/p)*(self.deployer.coe[1]+np.cos(self.deployer.coe[5])), \
                        0]) # perifocal frame velocity inertial of satellite
        NP = self.deployer.from_euler('313', [-self.deployer.coe[4], -self.deployer.coe[2], -self.deployer.coe[3]]) # DCM from perifocal to inertial
        self.deployer.cartesian.r = [np.dot(NP, R_P)]
        self.deployer.cartesian.v = [np.dot(NP, V_P)]

        # frame conversion parameters (validated with Matlab)
        ON = self.deployer.from_vec(self.deployer.cartesian.r[0], self.deployer.cartesian.v[0])
        h = np.linalg.norm(np.cross(self.deployer.cartesian.r[0], self.deployer.cartesian.v[0]))
        f_dot = h/np.linalg.norm(self.deployer.cartesian.r[0])**2

        # satellite state relative to the deployer in the deployer frame from inertial(validated with Matlab functions)
        relative = State()
        relative.frame = 'O'
        relative.relative = 'O'
        relative.r = [np.dot(ON, self.truth_satellite.cartesian.r[0]-self.deployer.cartesian.r[0])]
        relative.v = [np.dot(ON, self.truth_satellite.cartesian.v[0]-self.deployer.cartesian.v[0]) - np.cross(np.array([0,0,f_dot]), relative.r[0])]
        self.truth_satellite.cartesian = relative
        self.truth_orbit_state = relative

        # satellite state relative to the sensor frame in the sensor frame from orbit frame
        DS = self.truth_satellite.from_euler('321',self.deployer.orientation)
        ND = self.truth_satellite.from_euler('321', self.deployer.attitude.r[0])
        SO = np.dot(DS.T,np.dot(ND.T,ON.T))
        omega_SO = np.dot(np.dot(SO.T,DS.T), self.deployer.attitude.v[0]) - np.array([0,0,f_dot]) # angular rate between S and O in O frame

        temp = State()
        temp.frame = 'S'
        temp.relative = 'S'
        temp.r = [np.dot(SO, self.truth_satellite.cartesian.r[0]) - self.deployer.position]
        temp.v = [np.dot(SO, self.truth_satellite.cartesian.v[0]) - np.dot(SO, np.cross(omega_SO, self.truth_satellite.cartesian.r[0] - np.dot(SO.T,self.deployer.position)))]
        #temp.v = [np.dot(SO, self.truth_satellite.cartesian.v[0]) - np.dot(SO, np.cross(omega_SO, self.truth_satellite.cartesian.r[0] - np.dot(SO.T,self.deployer.position)))]

        self.satellite.cartesian = temp

    # sensor_to_orbit() output
    def test_sensor_to_orbit(self):
        self.satellite.sensor_to_orbit(self.deployer)
        self.assertTrue(np.linalg.norm(self.satellite.cartesian.r[0]-self.truth_orbit_state.r[0]) < 1e-10)

    # orbit_to_inertial() output
    def test_orbit_to_inertial(self):
        self.satellite.sensor_to_orbit(self.deployer)
        self.satellite.orbit_to_inertial(self.deployer)
        r_error = np.linalg.norm(self.satellite.cartesian.r[0]-self.truth_inertial_state.r[0])
        v_error = np.linalg.norm(self.satellite.cartesian.v[0]-self.truth_inertial_state.v[0])
        self.assertTrue((r_error + v_error) < 1e-10)

# ----------------------------------------------------------------
# test method DCM construction
# ----------------------------------------------------------------
class dcm_test(unittest.TestCase):
    def setUp(self):
        self.test_deployer = Deployer()

    # check that determinant is 1 for euler angle DCM
    def test_euler_det(self):
        dcm = self.test_deployer.from_euler('321', [rand(0,2*np.pi), rand(0,2*np.pi), rand(0,2*np.pi)])
        self.assertTrue(np.absolute(np.linalg.det(dcm)-1) < 1e-10)
    # check that determinant is 1 for [ON] DCM
    def test_vec_det(self):
        r_vec = np.array([6524.834, 6862.875, 6448.296])
        v_vec = np.array([4.901327, 5.533756, -1.976341])
        dcm = self.test_deployer.from_vec(r_vec, v_vec)
        self.assertTrue(np.absolute(np.linalg.det(dcm)-1) < 1e-10)
    # check 3-2-1 euler angle case
    def test_euler321(self): # truth dcm found using matlab angle2dcm
        dcm = self.test_deployer.from_euler('321', [0.7854, 0.1, 0])
        check = np.matrix([[0.7036, 0.7036, -0.0998], [-0.7071, 0.7071, 0.000], [0.0706, 0.0706, 0.9950]])
        self.assertTrue((np.round(dcm,4) == check).all())
    # check 3-1-3 equler angle case
    def test_euler313(self): # truth dcm found using matlab angle2dcm
        dcm = self.test_deployer.from_euler('313', [0.7854, 0.1, 0])
        check = np.matrix([[0.7071, 0.7071, 0.0000], [-0.7036, 0.7036, 0.0998], [0.0706, -0.0706, 0.9950]])
        self.assertTrue((np.round(dcm,4)== check).all())
    # check vector case ([ON])
    def test_vec(self):  # truth dcm found using matlab function Inertial2Hill()
        r_vec = np.array([6524.834, 6862.875, 6448.296])
        v_vec = np.array([4.901327, 5.533756, -1.976341])
        dcm = self.test_deployer.from_vec(r_vec, v_vec)
        check = np.matrix([[0.5695, 0.5990, 0.5628],[0.3548, 0.4385, -0.8257],[-0.7414, 0.6700, 0.0372]])
        self.assertTrue((np.round(dcm,4) == check).all())

    def tearDown(self):
        del self.test_deployer


# ----------------------------------------------------------------
# test format methods
# ----------------------------------------------------------------
class format_test(unittest.TestCase):
    def setUp(self):
        self.test_deployer = Deployer()
        self.data_dir = 'StateEstimation/test_data/'

    def test_read_csv(self):
        # verify function for an unprocessed trajectory file
        data = read_csv(self.data_dir+'trajectory')
        self.assertTrue(len(data) == 500 and len(data[0]) == 14,
                        'Incorrect size for measurement (trajectory) satellite data.')
        # verify function for a processed centroid file
        data = read_csv(self.data_dir + 'Satellite1')
        self.assertTrue(len(data) == 52 and len(data[0]) == 5,
                        'Incorrect size for processed satellite data.')
        # TODO: verify function for a deployer data file

    # check that the satellite object populates correctly for individually adding satellites
    def test_format_satellites_individual(self):
        test_sats = []
        # test adding an unprocessed trajectory file
        sat_data = read_csv(self.data_dir + 'measurements')
        format_satellite_data(sat_data, test_sats)
        self.assertTrue(len(test_sats) == 1, 'Failed to add first satellite.')
        for i in range(len(sat_data)):
            self.assertTrue(all(test_sats[0].cartesian.r[0] == sat_data[0][1:4]),
                            'Copy error - cartesian.r data mismatch.')
        # test adding a processed centroid file
        sat_data = read_csv(self.data_dir + 'Satellite1')
        test_sats = format_satellite_data(sat_data, test_sats)
        self.assertTrue(len(test_sats) == 2, 'Failed to add second satellite.')
        for i in range(len(sat_data)):
            self.assertTrue(all(test_sats[1].cartesian.r[0] == sat_data[0][1:4]),
                            'Copy error - cartesian.r data mismatch.')

    # check that the satellite object populates correctly for bulk adding satellites
    def test_format_satellites_multiple(self):
        test_sats = []
        # test adding 2 processed centroid files simultaneously
        sat_data0 = read_csv(self.data_dir + 'Satellite0')
        sat_data1 = read_csv(self.data_dir + 'Satellite1')
        combined_data = sat_data0 + sat_data1
        format_satellite_data(combined_data, test_sats)
        self.assertTrue(len(test_sats) == 2, 'Failed to add both satellites.')
        # check data integrity for both satellites
        for i in range(len(sat_data0)):
            self.assertTrue(all(test_sats[0].cartesian.r[0] == sat_data0[0][1:4]),
                            'Copy error - cartesian.r data mismatch for Satellite0.')
        for i in range(len(sat_data1)):
            self.assertTrue(all(test_sats[1].cartesian.r[0] == sat_data1[0][1:4]),
                            'Copy error - cartesian.r data mismatch for Satellite1.')



    def tearDown(self):
        del self.test_deployer
        del self.data_dir


# ----------------------------------------------------------------
# test method estimate_state()
# ----------------------------------------------------------------

# data that will eventually be loaded from files
class estimate_state_test(unittest.TestCase):
    def setUp(self):  # this is run at the beggining of every test method in this class
        self.test_deployer = Deployer()
        self.test_deployer.cartesian.r = [np.array([6524.834, 6862.875, 6448.296])]
        self.test_deployer.cartesian.v = [np.array([4.901327, 5.533756, -1.976341])]
        self.test_deployer.cartesian.frame = 'N'
        self.test_deployer.cartesian.relative = 'N'
        self.test_deployer.get_coe(3.986004415*10**5)

        self.test_satellite = Satellite(0)

    def testNoEcc(self):
        eta = (3.986e5 / 7000 ** 3) ** .5

        # load .m file with data as [t, position x, ..., velocity x, ...]
        true_data = np.mat(spio.loadmat('test_data/relative_data_0.mat')['rel_data'])

        new_state = State()
        new_state.t = [true_data[i, 0] for i in range(len(true_data))]
        new_state.r = [np.array([true_data[i, j]+np.random.normal(0, 1e-3) for j in [1, 2, 3]]) for i in range(len(true_data))]
        new_state.sensor = [0 for i in range(len(true_data))]
        new_state.frame = 'O'
        new_state.relative = 'O'
        self.test_satellite.cartesian = new_state

        self.test_satellite.estimate_state(self.test_deployer)
        self.assertEqual(new_state.t, self.test_satellite.cartesian.t,
                         "Time vector failed to propagate correctly.")
        self.assertEqual(len(new_state.r)-1, len(self.test_satellite.cartesian.r),
                         "Position vector list has wrong size.")
        self.assertEqual(len(new_state.r)-1, len(self.test_satellite.cartesian.v),
                         "Velocity vector list has wrong size.")
        self.assertEqual(self.test_satellite.cartesian.frame, 'O', "Wrong measurement frame.")
        self.assertEqual(self.test_satellite.cartesian.relative, 'O', "Wrong reference frame.")

    def tearDown(self):  # runs at the end of each test method in this class
        del self.test_deployer
        del self.test_satellite


# ----------------------------------------------------------------
# test method get_coe()
# ----------------------------------------------------------------
class get_coe_test(unittest.TestCase):
    def setUp(self): # this is run at the beggining of every test method in this class
        self.test_deployer = Deployer()
        self.test_deployer.cartesian.r = [np.array([6524.834, 6862.875, 6448.296])]
        self.test_deployer.cartesian.v = [np.array([4.901327, 5.533756, -1.976341])]

        self.test_deployer.cartesian.frame = 'N'
        self.test_deployer.cartesian.relative = 'N'
        self.test_deployer.get_coe()

    # frame Exception: frame
    def test_frame(self):
        self.test_deployer.cartesian.frame = 'S'
        with self.assertRaises(Exception) as check:
            self.test_deployer.get_coe()
        self.assertEqual(check.exception.args[0], "state must be represented inertially")
    # test Exception: relative
    def test_relative(self):
        self.test_deployer.cartesian.relative = 'S'
        with self.assertRaises(Exception) as check:
            self.test_deployer.get_coe()
        self.assertEqual(check.exception.args[0], "state state must be relative to inertial frame")
    # test coe append
    def test_append(self):
        self.test_deployer.cartesian.r.append(np.array([6524.834, 6862.875, 6448.296]))
        self.test_deployer.cartesian.v.append(np.array([4.901327, 5.533756, -1.976341]))
        self.test_deployer.get_coe()
        self.assertEqual(len(self.test_deployer.coe),2, "Append failed")
    # semi-major axis
    def test_SMA(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][0],4), 3.61273378e4, "SMA failed")
    # eccentricity
    def test_ECC(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][1],9), 8.32853399e-01, "ECC failed")
    # inclination
    def test_INC(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][2],8), 1.53360556, "INC failed")
    # right ascension of the ascending node
    def test_RAAN(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][3],8), 3.97757500, "RAAN failed")
    # argument of periapsis
    def test_AOP(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][4],9), 9.31742811e-01, "AOP failed")
    # true anomaly
    def test_TA(self):
        self.assertEqual(np.round(self.test_deployer.coe[0][5],8), 1.61155250, "TA failed")

    def tearDown(self): # runs at the end of each test method thin this class
        del self.test_deployer
# ----------------------------------------------------------------
# group tests cases to be run together
# ----------------------------------------------------------------
def test_suite():
    suite = unittest.TestSuite()
    # cartesian to coe
    suite.addTest(get_coe_test('test_frame'))
    suite.addTest(get_coe_test('test_relative'))
    suite.addTest(get_coe_test('test_append'))
    suite.addTest(get_coe_test('test_SMA'))
    suite.addTest(get_coe_test('test_ECC'))
    suite.addTest(get_coe_test('test_INC'))
    suite.addTest(get_coe_test('test_RAAN'))
    suite.addTest(get_coe_test('test_AOP'))
    suite.addTest(get_coe_test('test_TA'))
    # rotations
    suite.addTest(dcm_test('test_euler_det'))
    suite.addTest(dcm_test('test_vec_det'))
    suite.addTest(dcm_test('test_euler321'))
    suite.addTest(dcm_test('test_euler313'))
    suite.addTest(dcm_test('test_vec'))
    # state transformations
    suite.addTest(state_transformation('test_sensor_to_orbit'))
    suite.addTest(state_transformation('test_orbit_to_inertial'))
    # formatting objects
    suite.addTest(format_test('test_read_csv'))
    suite.addTest(format_test('test_format_satellites_individual'))
    suite.addTest(format_test('test_format_satellites_multiple'))

    return suite

if __name__ == '__main__':
    test_manager = unittest.TextTestRunner()
    test_manager.run(test_suite())
