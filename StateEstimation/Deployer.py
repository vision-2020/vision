# ----------------
# Deployer Class
# ----------------
# VISION: state estimation
# ----------------
# Author: Ben Hagenau
# Edited: 1/5/20

import numpy as np
from .State import State

class Deployer():
    def __init__(self):
        self.cartesian   = State()      # deployer cartesian state
        self.attitude    = State()      # deployer attitude state in deployer body frame coordinates
        self.coe         = []  # array of arrays containting orbital elements of deployer [SMA, ECC, INC, RAAN, AOP, TA]
        self.orientation = np.array([0,0,0],dtype=float)  # orientation of sensor frame relative to deployer body frame in sensor frame coordinates
        self.position    = np.array([0,0,0],dtype=float)  # position of sensor frame relative to deployer body frame in sensor frame coordinates

# ----------------------------------------------------------------
# convert all Cartesian state inertial state to orbital elements
# ----------------------------------------------------------------
# INPUTS: mu: graviational parameter
# OUTPUTS: classiacal orbital elements (array)
# ASSUMPTIONS: elliptical orbit, non-circular, non-equitorial
    def get_coe(self, mu=3.986004415*10**5):
        self.coe.clear()
        if self.cartesian.frame != 'N':
            raise Exception("state must be represented inertially")
        if self.cartesian.relative != 'N':
            raise Exception("state state must be relative to inertial frame")

        for row in range (0,len(self.cartesian.r)):
            # cartesial state
            r_vec = self.cartesian.r[row]
            r = np.linalg.norm(r_vec)
            v_vec = self.cartesian.v[row]
            v = np.linalg.norm(v_vec)
            # angular momentums
            h_vec = np.cross(r_vec, v_vec)
            h = np.linalg.norm(h_vec)
            # line of nodes
            n_vec = np.cross(np.array([0,0,1]), h_vec)
            n = np.linalg.norm(n_vec)
            # eccentricity
            ECC_vec = (1/mu)*( (v**2 - (mu/r))*r_vec - np.dot(r_vec, v_vec)*v_vec )
            ECC = np.linalg.norm(ECC_vec)
            # specific mechanical energy
            SME = v**2/2 - mu/r
            # orbit geometry
            SMA = -mu/(2*SME)
            # inclination
            INC = np.arccos(h_vec[2]/h)
            # right ascension of the ascending node (singular if equitorial)
            RAAN = np.arccos(n_vec[0]/n)
            if n_vec[1] < 0:
                RAAN = 2*np.pi - RAAN
            # argument of periapsis (singular if circular)
            AOP = np.arccos(np.dot(n_vec,ECC_vec)/(ECC*n))
            if ECC_vec[2] < 0:
                AOP = 2*np.pi - AOP
            # true anomaly
            TA = np.arccos(np.dot(ECC_vec,r_vec)/(ECC*r))
            if np.dot(r_vec,v_vec) < 0:
                TA = 2*np.pi - TA
            # construct output
            self.coe.append([SMA, ECC, INC, RAAN, AOP, TA])

        return self.coe

    # ----------------------------------------------------------------
    # Construct DCM using Euler angles
    # ----------------------------------------------------------------
    # INPUTS: seq: rotation sequence (i.e. '321' first-second-third), theta: rotation angles [rot1, rot2, rot3] [rad] (Euler angles defined in the frame you start in)
    # OUTPUTS: DCM
    # ASSUMPTIONS: N/A
    def from_euler(self, seq, theta):
        i = 0
        dcm = np.array([[1,0,0],[0,1,0],[0,0,1]])
        for rot in seq:
            angle = theta[i]
            if rot == '1':
                R = np.array([[1,0,0],[0,np.cos(angle),np.sin(angle)],[0,-np.sin(angle),np.cos(angle)]])
            elif rot == '2':
                R = np.array([[np.cos(angle),0,-np.sin(angle)],[0,1,0],[np.sin(angle),0,np.cos(angle)]])
            elif rot == '3':
                R = np.array([[np.cos(angle),np.sin(angle),0],[-np.sin(angle),np.cos(angle),0],[0,0,1]])
            dcm = np.dot(R,dcm)
            i+=1

        return dcm

    # ----------------------------------------------------------------
    # Construct inertial-to-orbit frame DCM using inertial state vecotors
    # ----------------------------------------------------------------
    # INPUTS: r_vec: inertial position vector, v_vec: inertial velocity vector (numpy.array)
    # OUTPUTS: DCM from inertial to orbit frame
    # ASSUMPTIONS: N/A
    def from_vec(self, r_vec, v_vec):
        r = np.linalg.norm(r_vec)
        # unit orbit radial in inertial coordinates
        r_hat = r_vec/r
        # unit orbit normal in inertial coordinates
        h_vec = np.cross(r_vec,v_vec)
        h = np.linalg.norm(h_vec)
        h_hat = h_vec/h
        # unit orbit tangent in inertial coordinates
        theta_hat = np.cross(h_hat, r_hat)
        # construct DCM
        ON = np.array([r_hat, theta_hat, h_hat])

        return ON
