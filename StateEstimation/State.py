# ----------------
# State class
# ----------------
# VISION: state estimation
# ----------------
# Author: Ben Hagenau
# Edited: 1/7/20

import numpy as np

class State:
    def __init__(self):
        self.frame = ''   # flag specifying frame represenation ('S':sensor, 'O':orbit, 'N':inertial)
        self.relative = '' # flag specifying what state is relative to ('S':sensors, 'O':deployer, 'N':Earth)
        self.r = [] # array of array of position vectors (can be attitude)
        self.v = [] # array of array of velocity vectors (can be attitude rate)
        self.t = [] # array of times corrosponding to each state
