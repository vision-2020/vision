# ------------------------------------------------------------------------------
# Nonlinear batch filter implementation class
# ------------------------------------------------------------------------------
# ASEN 4028: VISION
# ------------------------------------------------------------------------------
# Author: Ben Hagenau
# Created: 3/4/20
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

class NLB():
    # INPUTS:
    #   odefun_dfdz: ode function for state and state transition matrix (f(z,t,args))
    #   dhdz: evaluate measurement partials dhdz a.k.a H (measurement sensitivity matrix) (function handle f(z,t,args))
    #   h: nonlinear measurement function (function handle h(t,z))
    def __init__(self, odefun_dfdz, dhdz, h):
        self.odefun_dfdz = odefun_dfdz
        self.dhdz = dhdz
        self.h = h
    # ------------------------------------------------------------------------------
    # run filter on data provided using the given initialization parameters
    # ------------------------------------------------------------------------------
    # INPUTS:
    #   Y: measurements (list of np.array())
    #   t: measurement times (list)
    #   x0, P0: initial state estimate and covaraince (np.array()) (P0=[] if no P0)
    #   dx0: initial expected perturbation in the initial state from the truth
    #   R: measurement covariance matrix (np.array())
    #   env_params: dictionairy of additional parameters required in integration
    #   deployer: deployer object with states interpolated to measurement times
    def run(self, Y, t, x0, R, deployer, P0_init=[], env_params={}):
        dx0 = np.zeros((1,6))[0]
        # unpack
        self.n = x0.size
        X0 = [x0] # initial state
        dX0 = [dx0] # initial sate deviation
        Phi0 = np.identity(self.n) # initial state transition matrix
        if P0_init == []:
            Lambda0 = np.zeros((self.n, self.n))
        else:
            cov_chol = np.linalg.cholesky(P0_init) # cholesky factorization (for ill-conditioned Lambda)
            Lambda0 = np.linalg.inv(cov_chol @ cov_chol.transpose())
        N = np.zeros(6)
        R_inv = np.linalg.inv(R)
        P = []
        # convergence check parameters
        dX0_jm1 = 11 # previous step dX0
        dX0_j = 10 # current step dX0
        maxIt = 20 # max number of iterations
        currIt = 0 # number of iterations
        # outer loop
        while np.linalg.norm(dX0_jm1) >= np.linalg.norm(dX0_j) and maxIt > currIt:
            # inner loop
            Lambda = Lambda0 # reset amount of information for next iteration
            Phi = np.identity(self.n) # reset cumulative STM
            N = Lambda @ dX0[-1]
            X_jm1 = X0[-1]
            r = []
            for j in range(1, len(t)):
                # propogate
                z0 = np.hstack((X_jm1,
                               deployer.cartesian.r[j-1].T, deployer.cartesian.v[j-1].T,\
                               np.reshape(Phi0, (-1))))
                dt = np.linspace(t[j-1], t[j], 10) # time span to next measurement
                X_j, Phi_j, _ = self.propogate(dt, z0, env_params) # state at current measurement
                Phi = Phi_j @ Phi # extend cumulative Phi (t0 to tj)
                # accumulate measurements
                r.append(Y[j] - self.h(X_j, t[j]))
                H_j = self.dhdz() # measurement sensitivity matrix
                H = H_j @ Phi # combined STM and H
                Lambda = Lambda + H.transpose() @ R_inv @ H # update information matrix
                N = N + H.transpose() @ R_inv @ r[-1] # update deviation accumulation vector
                X_jm1 = X_j
            # solve batch equations
            lam_chol = np.linalg.cholesky(Lambda) # cholesky factorization (for ill-conditioned Lambda)
            P0 = np.linalg.inv(lam_chol @ lam_chol.transpose())
            dX0_jm1 = dX0_j
            dX0_j = P0 @ N
            dX0.append(dX0[-1] + dX0_j)
            X0.append(X0[-1] + dX0_j)
            currIt = currIt+1

        # propogate estimates
        z0 = np.hstack((X0[-1],
                       deployer.cartesian.r[0].T, deployer.cartesian.v[0].T,\
                       np.reshape(Phi0, (-1))))
        _, _, z = self.propogate(t, z0, env_params)
        self.estimates = z[:,:self.n]
        for k in range(0, len(t)):
            Phi_k = np.reshape(z[k, 2*self.n:], (self.n, self.n))
            P.append(Phi_k @ P0 @ Phi_k.transpose())
        self.P = P
        self.t = t
        self.r = r


        return self.estimates, self.P

# ------------------------------------------------------------------------------
# --- Plotting functions -------------------------------------------------------
# ------------------------------------------------------------------------------
    # --------------------------------------------------------------------------
    # nonlinear prpogation of dynamical state and state transition matrix
    # --------------------------------------------------------------------------
    # INPUTS:
    #   dt: integration times (np.array()) ranging from ti to ti+1
    #   z0: initial state for integration (np.array())
    #   params: additional parameters required for integration
    # OUTPUTS:
    #   x: state at final times
    #   Phi: STM at final time (effects depend on initial Phi)
    #   z: all integration outputs
    def propogate(self, dt, z0, env_params={}):
        z = odeint(lambda z, t: self.odefun_dfdz(z,t,**env_params), z0, dt, rtol=1e-13, atol=1e-13)
        t = (dt[0], dt[-1])
        x = z[-1][:self.n] # state at next time step
        Phi = np.reshape(z[-1][2*self.n:], (self.n, self.n)) # state transition matrix from ti to ti+1
        return x, Phi, z
    # --------------------------------------------------------------------------
    # PLOT: position and vecloity estimate error
    # --------------------------------------------------------------------------
    # INPUTS: R_truth, V_truth: truth position and velocity [x,y,z]/[xd,yd,zd] (list of np.array())
    def PlotRVEstimateError(self, R_truth, V_truth):
        font = {'size':'15'}
        # compute std
        std = []
        for P_i in self.P:
            std.append(np.array([np.sqrt(P_i[0,0]), np.sqrt(P_i[1,1]), np.sqrt(P_i[2,2]), np.sqrt(P_i[3,3]), np.sqrt(P_i[4,4]), np.sqrt(P_i[5,5])]))
        # compute error
        R_error = np.asarray(self.estimates)[:,[0,1,2]] - R_truth
        Rd_error = np.asarray(self.estimates)[:,[3,4,5]] - V_truth
        # plot labels
        axis = ['x', 'y', 'z']
        # plot: position error
        fig, ax = plt.subplots(3,1)
        for i in range(0,3):
            ax[i].plot(self.t, R_error[:,i]*1000,'k', label='estimate error')
            ax[i].plot(self.t, 3*np.asarray(std)[:,i]*1000,'--k', label='$\pm$3$\sigma$')
            ax[i].plot(self.t, -3*np.asarray(std)[:,i]*1000,'--k')
            ax[i].set_ylabel("$\epsilon_{}$ [m]".format(axis[i]), **font)
        # plt.suptitle('Position Estimate Error', **font)
        ax[2].set_xlabel('Time [s]', **font)
        ax[0].legend()
        # plot: velocity error
        fig, ax = plt.subplots(3,1)
        for i in range(0,3):
            ax[i].plot(self.t, Rd_error[:,i]*1000,'k', label='estimate error')
            ax[i].plot(self.t, 3*np.asarray(std)[:,i+3]*1000,'--k', label='$\pm$3$\sigma$')
            ax[i].plot(self.t, -3*np.asarray(std)[:,i+3]*1000,'--k')
            ax[i].set_ylabel("$\epsilon_{}$ [m/s]".format(axis[i]), **font)
        # plt.suptitle('Velocity Estimate Error', **font)
        ax[2].set_xlabel('Time [s]', **font)
        ax[0].legend()
    # # --------------------------------------------------------------------------
    # # PLOT: estimate residuals
    # # --------------------------------------------------------------------------
    # # INPUTS: Y: measurements, r_sig: expected position standard deviation, v_sig: expected velocity standard deviation
    # def PlotPositionResiduals(self, Y, r_sig):
    #     font = {'size':'20'}
    #     x_lab = ['$x$ [m]', '$y$ [m]', '$z$ [m]']
    #     n = len(self.t)
    #     # position
    #     fig, ax = plt.subplots(3,1)
    #     for i in range(0,3):
    #         ax[i].plot(self.t, (np.asarray(Y)[:,i]-self.estimates[:,i])*1000,'.b', label='residuals')
    #         ax[i].plot(self.t, 3*r_sig*np.ones(n)*1000,'--k', label='$\pm$3$\sigma$')
    #         ax[i].plot(self.t, -3*r_sig*np.ones(n)*1000, '--k')
    #         ax[i].set_ylabel(x_lab[i], **font)
    #     ax[i].set_xlabel('Time [s]', **font)
    #     plt.suptitle('Position Estimate Residuals', **font)
    #     ax[0].legend()
    # # --------------------------------------------------------------------------
    # # PLOT: estimate residuals
    # # --------------------------------------------------------------------------
    # # INPUTS: Y: measurements, r_sig: expected position standard deviation, v_sig: expected velocity standard deviation
    # def PlotVelocityResiduals(self, Y, r_sig):
    #     font = {'size':'20'}
    #     x_lab = ['$xd$ [m/s]', '$yd$ [m/s]', '$zd$ [m/s]']
    #     n = len(self.t[1:-1])
    #     # position
    #     fig, ax = plt.subplots(3,1)
    #     for i in range(0,3):
    #         ax[i].plot(self.t[1:-1], (np.asarray(Y)[:,i]-self.estimates[1:-1,i])*1000,'.b')
    #         ax[i].plot(self.t[1:-1], 3*r_sig*np.ones(n)*1000,'--k')
    #         ax[i].plot(self.t[1:-1], -3*r_sig*np.ones(n)*1000, '--k')
    #         ax[i].set_ylabel(x_lab[i], **font)
    #     ax[i].set_xlabel('Time [s]', **font)
    #     plt.suptitle('Velocity Estimate Residuals', **font)
    # --------------------------------------------------------------------------
    # show plots (run after all plot funcations have been called)
    # --------------------------------------------------------------------------
    def ShowPlots(self):
        plt.show()
