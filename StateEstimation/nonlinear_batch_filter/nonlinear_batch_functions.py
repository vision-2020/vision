# ------------------------------------------------------------------------------
# Define measurement and dynamics functions required for nlb (assuming Keplarian orbit with J2)
# ------------------------------------------------------------------------------
# ASEN 4028: VISION
# ------------------------------------------------------------------------------
# Author: Ben Hagenau
# Edited: 3/4/20

import numpy as np

# -----------------------------------------------------------------------------
# ODE functions: general nonlinear relative orbit equations of motion
# -----------------------------------------------------------------------------
# INPUTS:
#   z: state vector [np.array(x,y,z,xd,yd,zd)]
#   t: integration time
#   mu: Earth gravitational parameter [km^3/s^2] (same used to generate data)
#   r_e: radius of Earth [km] (same used to generate data)
# OUTPUTS:
#   zd: derivative of state vector np.array([xd,yd,zd,xdd,ydd,zdd])
def ode_relative_orbit(z, t, mu, r_e):
        # unpack
        X  = z[0]
        Y  = z[1]
        Z  = z[2]
        Xd = z[3]
        Yd = z[4]
        Zd = z[5]
        R_deployer = np.array([z[6], z[7], z[8]])
        V_deployer = np.array([z[9], z[10], z[11]])
        Phi = np.reshape(z[12:],(6,6))
        # inertial state of deployer
        r_c = np.linalg.norm(R_deployer)
        rd_c = np.dot(V_deployer, R_deployer/r_c)
        h = np.linalg.norm(np.cross(R_deployer, V_deployer))
        fd = h/r_c**2
        f_dep = np.hstack((V_deployer, -mu/r_c**3 * R_deployer))
        r_d = np.linalg.norm(np.array([(r_c + X), Y, Z]))
        # nonlinear relaitve orbital dynamics
        f = [Xd,\
             Yd,\
             Zd,\
             X*fd**2 + mu/r_c**2 + 2*fd*(Yd - (rd_c*Y)/r_c) - (mu*(r_c + X))/r_d**3,\
             Y*fd**2 - 2*fd*(Xd - (rd_c*X)/r_c) - (mu*Y)/r_d**3,\
             -(mu*Z)/r_d**3]
        # State transition matrix
        A = dfdz(z, r_c, rd_c, fd, mu, r_e)
        Phid = np.dot(A, Phi)
        Phid_vec = np.reshape(Phid, (36,1))
        # format output
        zd = np.hstack((f, f_dep, Phid_vec.T[0]))

        return zd
# -----------------------------------------------------------------------------
# Evaluate relative orbital motion Jacobian matrix (partial of zd w.r.t. z)
# -----------------------------------------------------------------------------
# INPUTS:
#   z: state vector [z,y,z,xd,yd,zd]
#   r_c: chief (deployer) orbital radius
#   r_cd: chief (deployer) orbitial speed
#   fd: chief (deployer) true anomaly rate
#   mu: earth gravitational parameters
#   r_e: earth radius
# OUTPUTS:
#   dfdz: jacobian
def dfdz(z, r_c, r_cd, fd, mu, r_e):
    # unpack
    X  = z[0]
    Y  = z[1]
    Z  = z[2]
    Xd = z[3]
    Yd = z[4]
    Zd = z[5]
    # compute Jacobian (generated symbolically in MATLAB)
    dfdz = np.array([[0,0,0,1,0,0],\
    [0,0,0,0,1,0],\
    [0,0,0,0,0,1],\
    [fd**2 - mu/((r_c + X)**2 + Y**2 + Z**2)**(3/2) + (3*mu*(2*r_c + 2*X)*(r_c + X))/(2*((r_c + X)**2 + Y**2 + Z**2)**(5/2)),(3*mu*Y*(r_c + X))/((r_c + X)**2 + Y**2 + Z**2)**(5/2) - (2*fd*r_cd)/r_c,(3*mu*Z*(r_c + X))/((r_c + X)**2 + Y**2 + Z**2)**(5/2),0,2*fd,0],\
    [(2*fd*r_cd)/r_c + (3*mu*Y*(2*r_c + 2*X))/(2*((r_c + X)**2 + Y**2 + Z**2)**(5/2)),fd**2 - mu/((r_c + X)**2 + Y**2 + Z**2)**(3/2) + (3*mu*Y**2)/((r_c + X)**2 + Y**2 + Z**2)**(5/2),(3*mu*Y*Z)/((r_c + X)**2 + Y**2 + Z**2)**(5/2),-2*fd,0,0],\
    [(3*mu*Z*(2*r_c + 2*X))/(2*((r_c + X)**2 + Y**2 + Z**2)**(5/2)),(3*mu*Y*Z)/((r_c + X)**2 + Y**2 + Z**2)**(5/2),-(mu*(2*r_c*X + r_c**2 + X**2 + Y**2 - 2*Z**2))/((r_c + X)**2 + Y**2 + Z**2)**(5/2),0,0,0]])

    return dfdz
# -----------------------------------------------------------------------------
# Measurement function: conversion between state and measurements
# -----------------------------------------------------------------------------
# INPUTS: z: state vector [z,y,z,xd,yd,zd]
# OUTPUTS: np.array([x,y,z])
def h(z, t):
    # directly measuring position
    return z[:3]
# -----------------------------------------------------------------------------
# Evalute measurement sensitivity matrix (partial of h w.r.t. z)
# -----------------------------------------------------------------------------
def dhdz():
    # directly measuring position
    # H = np.array([[1, 0, 0, 0, 0, 0],
    #             [0, 1, 0, 0, 0, 0],
    #             [0, 0, 1, 0, 0, 0],
    #             [0, 0, 0, 0, 0, 0],
    #             [0, 0, 0, 0, 0, 0],
    #             [0, 0, 0, 0, 0, 0]])
    H = np.hstack((np.identity(3), np.zeros((3,3))))
    return H
