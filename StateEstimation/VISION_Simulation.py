# ------------------------------
# Simulate deployement scenario
# ------------------------------
# VISION: state estimation
# ------------------------------
# Author: Ben Hagenau
# Created: 1/20/20
# Edited: 1/29/20

import numpy as np
from itertools import product
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from scipy.integrate import odeint
from .Deployer import Deployer
from .Satellite import Satellite
from .State import State
from .Format import *

# -----------------------------------------------------------------------------
# --- CLASS DEFINITION --------------------------------------------------------
# -----------------------------------------------------------------------------
class Simulation():
# -----------------------------------------------------------------
# initialize deployment scenario
# -----------------------------------------------------------------
# INPUTS: R0-initial deployment position as seen by sensors in sensor frame [[sat1], [sat2],...]
#         V0-initial deployment velocity as seen by sensors in sensor frame [[sat1], [sat2],...]
#         n-number of satellites deployed
#         dt-deployment interval
#         deployer-Deployer() object (with single cartesian state at first deplyment time)
#         t_sim-simulation time
# OUTPUTS: N/A
# NOTES:
    def __init__(self, sat_ICs, deployer, n, dt, t_sim, fps):
        # assign
        self.deployer = deployer
        self.n = n
        self.dt = dt
        self.fps = fps
        self.t_sim = t_sim
        # pre-allocate
        self.truth_satellites = []
        self.satellites = []
        self.true_inertial = []
        self.true_orbit = []
        self.true_sensor = []
        self.true_coe = []
        self.estimated_inertial = []
        self.estimated_orbit = []
        self.estimated_sensor = []
        self.estimated_coe = []

        # construct truth satellite object(s)
        for i in range(0, n):
            # initialize satellite object
            sat = Satellite(i)
            sat.cartesian.frame = 'S'
            sat.cartesian.relative = 'S'
            sat.cartesian.r = [np.array(sat_ICs[i]['r0'], dtype=float)]
            sat.cartesian.v = [np.array(sat_ICs[i]['v0'], dtype=float)]
            # orbit frame initial conditions
            sat.sensor_to_orbit(deployer)
            # inertial initial conditions
            sat.orbit_to_inertial(self.deployer)
            # store
            self.truth_satellites.append(sat)
            # pre-allocate to store truth and estimated quantities
            self.true_inertial.append(State()); self.true_inertial[i].frame = 'N'; self.true_inertial[i].relative = 'N'
            self.true_orbit.append(State()); self.true_orbit[i].frame = 'O'; self.true_orbit[i].relative = 'O'
            self.true_sensor.append(State()); self.true_sensor[i].frame = 'S'; self.true_sensor[i].relative = 'S'
            self.estimated_inertial.append(State()); self.estimated_inertial[i].frame = 'N'; self.estimated_inertial[i].relative = 'N'
            self.estimated_orbit.append(State()); self.estimated_orbit[i].frame = 'O'; self.estimated_orbit[i].relative = 'O'
            self.estimated_sensor.append(State()); self.estimated_sensor[i].frame = 'S'; self.estimated_sensor[i].relative = 'S'
            self.true_coe.append([])
            self.estimated_coe.append([])

# -----------------------------------------------------------------
# integrate initial conditions to obtain inertial
# -----------------------------------------------------------------
# INPUTS: R0-initial deployment velocity as seen by sensors in sensor frame
#         V0-initial deployment position as seen by sensors in sensor frame
#         n-number of satellites deployed
#         dt-deployment interval
#         deployer-Deployer() object
# OUTPUTS: true_inertial-true inertial State() of satellite
# NOTES: 2-body dyanmics
    def generate_truth(self):
        # Earth gravitational parameter
        mu = 3.986004415*10**5 # [km^3/s^2]
        # integrate deployer inertial state
        # t_dep = np.linspace(0, self.t_sim+self.dt*self.n, 1000)
        t_dep = np.arange(0, self.t_sim+self.dt*self.n, 1.0/self.fps)
        y0_dep = np.concatenate((self.deployer.cartesian.r[0], self.deployer.cartesian.v[0]))
        y_dep = odeint(KeplarianOrbit, y0_dep, t_dep, args=(mu,))
        self.deployer.cartesian.r = y_dep[:,[0,1,2]]
        self.deployer.cartesian.v = y_dep[:,[3,4,5]]
        self.deployer.cartesian.t = t_dep
        self.deployer.get_coe()

        # generate truth for each satellite
        for i in range(0,self.n):
            # initial conditions
            y0_sat = np.concatenate((self.truth_satellites[i].cartesian.r[0], self.truth_satellites[i].cartesian.v[0]))
            # integration time vector
            t_vec = np.linspace(self.dt*i, self.t_sim+self.dt*i, 100)
            t_vec = t_dep
            # integrate satellite state
            y_sat = odeint(KeplarianOrbit, y0_sat, t_vec, args=(mu,))
            # store true inertial states
            self.true_inertial[i].r = y_sat[:,[0,1,2]]
            self.true_inertial[i].v = y_sat[:,[3,4,5]]
            # compute true classical orbital elements
            self.truth_satellites[i].cartesian = self.true_inertial[i]
            self.true_coe[i] = self.truth_satellites[i].get_coe()
            # convert from inertial to sensor frame
            for it in range(0, len(t_vec)):
                ON = self.deployer.from_vec(self.deployer.cartesian.r[it], self.deployer.cartesian.v[it])
                h = np.linalg.norm(np.cross(self.deployer.cartesian.r[it], self.deployer.cartesian.v[it]))
                f_dot = np.array([0, 0, h/np.linalg.norm(self.deployer.cartesian.r[it])**2])
                # frame transformation: inertial to orbit
                self.true_orbit[i].r.append( np.dot(ON, self.true_inertial[i].r[it]-self.deployer.cartesian.r[it]) )
                self.true_orbit[i].v.append( np.dot(ON, self.true_inertial[i].v[it]-self.deployer.cartesian.v[it])\
                            - np.cross(f_dot, self.true_orbit[i].r[it]))
                # frame transformation: orbit to sensor
                DS = self.truth_satellites[i].from_euler('321',self.deployer.orientation)
                ND = self.truth_satellites[i].from_euler('321', self.deployer.attitude.r[it])
                SO = np.dot(DS.T,np.dot(ND.T,ON.T))
                omega_SO = np.dot(np.dot(SO.T,DS.T), self.deployer.attitude.v[it]) - f_dot
                self.true_sensor[i].r.append(np.dot(SO, self.true_orbit[i].r[it]) - self.deployer.position)
                self.true_sensor[i].v.append(np.dot(SO, self.true_orbit[i].v[it]) - np.dot(SO, np.cross(omega_SO, self.true_orbit[i].r[it] - np.dot(SO.T,self.deployer.position))))
                # define deployer attitude - only for first satellite
                if i == 0 and it < len(t_vec)-1:
                    self.deployer.attitude.r.append([0,0,0])
                    self.deployer.attitude.v.append([0,0,0])
            self.true_inertial[i].t = t_vec
            self.true_orbit[i].t = t_vec
            self.true_sensor[i].t = t_vec

        return self.true_inertial, self.true_orbit, self.true_sensor, self.true_coe, self.deployer
# -----------------------------------------------------------------
# generate truth data file to be passed into C4D/Blensor
# -----------------------------------------------------------------
# INPUTS: measurment_file_name-name of mesurement data file
#         truth_file_name-name of truth data file
#         flag_noise-1:add noise to measurment data, 0:no noise to measurement data
# OUTPUTS: N/A
# NOTES: N/A
    def generate_files(self, data_dir, flag_noise):
        # standard deviation
        r_sig = 1 / 1000   # [km]
        # sample multivariate normal
        R = [[r_sig**2, 0, 0],
             [0, r_sig**2, 0],
             [0, 0, r_sig**2]]
        mu = [0,0,0]
        for i in range(0,self.n):
            l = len(self.truth_satellites[i].cartesian.t)
            # id vector
            sat_id_vec = np.ones(l)*self.truth_satellites[i].id
            sensor_id_vec = np.ones(l)*self.truth_satellites[i].id
            # add noise to truth
            # if flag_noise == 1:
            #     r_noise = np.random.multivariate_normal(mu, R, (l,))
            #     r = self.true_sensor[i].r + r_noise
            #     rd = self.true_sensor[i].v
            # else:
            r = self.true_sensor[i].r
            rd = self.true_sensor[i].v
            # generate measurements file (for C4D and Blensor, or VISION)
            if i == 0:
                measurements = np.append(r, rd, 1)
                measurements = np.hstack((np.expand_dims(sat_id_vec, 1),\
                                measurements, np.expand_dims(self.true_sensor[i].t, 1)))
            else:
                meas = np.append(r, rd, 1)
                meas = np.hstack((np.expand_dims(sat_id_vec, 1),\
                                meas, np.expand_dims(self.true_sensor[i].t, 1)))
                measurements = np.vstack((measurements, meas))
            orbit_frame = np.hstack((np.expand_dims(sat_id_vec, 1), self.true_orbit[i].r, self.true_orbit[i].v, np.expand_dims(self.true_orbit[i].t,1)))
            inertial_frame = np.hstack((np.expand_dims(sat_id_vec, 1), self.true_inertial[i].r, self.true_inertial[i].v, np.expand_dims(self.true_inertial[i].t,1)))
            deployer_data = np.hstack((self.deployer.cartesian.r, self.deployer.cartesian.v, self.deployer.attitude.r, self.deployer.attitude.v, np.expand_dims(self.deployer.cartesian.t,1)))
            # save files
            np.savetxt(data_dir+"measurements.csv", measurements, delimiter=",", fmt='%.16f')
            np.savetxt(data_dir+"true_orbit_frame.csv", orbit_frame, delimiter=",", fmt='%.16f')
            np.savetxt(data_dir+"true_inertial_frame.csv", inertial_frame, delimiter=",", fmt='%.16f')
            np.savetxt(data_dir+"deployer_data.csv", deployer_data, delimiter=",", fmt='%.16f')
            np.savetxt(data_dir+"mission_data.csv", np.array([self.deployer.position, self.deployer.orientation]), delimiter=",", fmt='%.16f')
# -----------------------------------------------------------------
# run vision state estimation algorithm on sensor data in measurement file
# -----------------------------------------------------------------
# INPUTS: measurement_file-name of file with sensor measurements
# OUTPUTS: N/A
# NOTES: N/A
    def run_vision(self, measurement_file, deployer_file):
        # load measurements file
        measurements = np.loadtxt(measurement_file, delimiter=',')
        measurements[:,0] = measurements[:,0].astype(int)
        # load deployer file
        # format deployer state
        # deployer = format_deployer(measurements, deployer_data, physical_data_filename):
        # construct satellite
        self.satellites = format_satellite_data(measurements, None)
        # run VISION state estimation algorithm
        for i in range(0, self.n):
            self.preCKF  = self.satellites[i].sensor_to_orbit(self.deployer)
            self.estimated_orbit[i] = self.satellites[i].estimate_state(self.deployer)
            self.estimated_inertial[i] = self.satellites[i].orbit_to_inertial(self.deployer)
            self.estimated_coe[i] = self.satellites[i].get_coe()
            self.satellites[i].estimate_coe(self.deployer)
            # need to still use format deployer
# -----------------------------------------------------------------
# quantify vision algorithm performance
# -----------------------------------------------------------------
# INPUTS: measurement_file-name of file with sensor measurements
# OUTPUTS: N/A
# NOTES: N/A
    def generate_results(self):
        # COE: error
        print('-------------- COE Error --------------')
        print("SMA: {}".format(self.satellites[0].coe[0]-self.truth_satellites[0].coe[0][0]))
        print("ECC: {}".format(self.satellites[0].coe[1]-self.truth_satellites[0].coe[0][1]))
        print("INC: {}".format(self.satellites[0].coe[2]-self.truth_satellites[0].coe[0][2]))
        print("RAAN: {}".format(self.satellites[0].coe[3]-self.truth_satellites[0].coe[0][3]))
        print("AOP: {}".format(self.satellites[0].coe[4]-self.truth_satellites[0].coe[0][4]))
        print("TA: {}".format(self.satellites[0].coe[5]-self.truth_satellites[0].coe[0][5]))
        # COE: deployer
        print('------------ Deployer COE ------------')
        print("SMA: {}".format(self.deployer.coe[0][0]))
        print("ECC: {}".format(self.deployer.coe[0][1]))
        print("INC: {}".format(self.deployer.coe[0][2]))
        print("RAAN: {}".format(self.deployer.coe[0][3]))
        print("AOP: {}".format(self.deployer.coe[0][4]))
        print("TA: {}".format(self.deployer.coe[0][5]))
        return 0
# -----------------------------------------------------------------
# plot results
# -----------------------------------------------------------------
# INPUTS: measurement_file-name of file with sensor measurements
# OUTPUTS: N/A
# NOTES: N/A
    def plot_results(self):
        # COE: truth and measured
        coe_names = ['SMA', 'ECC', 'INC', 'RAAN', 'AOP', 'TA']
        units = ['[m]', '', '[rad]', '[rad]', '[rad]', '[rad]']
        for sat in range(0,self.n):
            t = self.true_inertial[sat].t
            ind = list(product((0, 1, 2), repeat=2))
            fig, ax = plt.subplots(2,3)
            for i in range(0,6):
                ax[ind[i][0], ind[i][1]].plot(t, np.asarray(self.estimated_coe[sat])[:,i])
                ax[ind[i][0], ind[i][1]].plot(t, np.asarray(self.true_coe[sat])[:,i],'k')
                ax[ind[i][0], ind[i][1]].set_title(coe_names[i])
                ax[ind[i][0], ind[i][1]].set_ylabel(units[i])
            ax[1,1].set_xlabel('time [s]')

        # ORBIT FRAME: truth vs estimates
        for sat in range(0,self.n):
            t = self.true_inertial[sat].t
            fig, ax = plt.subplots(2,1)
            ax[0].plot(t, (np.asarray(self.estimated_orbit[sat].r)[:,0] - np.asarray(self.true_orbit[sat].r)[:,0])*1000, 'r')
            ax[0].plot(t, (np.asarray(self.estimated_orbit[sat].r)[:,1] - np.asarray(self.true_orbit[sat].r)[:,1])*1000, 'g')
            ax[0].plot(t, (np.asarray(self.estimated_orbit[sat].r)[:,2] - np.asarray(self.true_orbit[sat].r)[:,2])*1000, 'b')
            ax[0].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,0])*1000, '--r')
            ax[0].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,1])*1000, '--g')
            ax[0].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,2])*1000, '--b')
            ax[0].set_title('Orbit Frame Position Error')
            ax[0].set_ylabel('[m]')
            ax[1].plot(t, (np.asarray(self.estimated_orbit[sat].v)[:,0] - np.asarray(self.true_orbit[sat].v)[:,0])*1000, 'r')
            ax[1].plot(t, (np.asarray(self.estimated_orbit[sat].v)[:,1] - np.asarray(self.true_orbit[sat].v)[:,1])*1000, 'g')
            ax[1].plot(t, (np.asarray(self.estimated_orbit[sat].v)[:,2] - np.asarray(self.true_orbit[sat].v)[:,2])*1000, 'b')
            ax[1].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,3])*1000, '--r')
            ax[1].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,4])*1000, '--g')
            ax[1].plot(t, np.sqrt(np.asarray(self.satellites[sat].cov)[:,5])*1000, '--b')
            ax[1].set_title('Orbit Frame Velocity Error')
            ax[1].set_ylabel('[m/s]')
            ax[1].set_xlabel('Time [s]')

            # # INERTIAL FRAME: truth vs estimates
            # for sat in range(0,self.n):
            #     t = self.true_inertial[sat].t
            #     fig, ax = plt.subplots(2,1)
            #     ax[0].plot(t, np.asarray(self.estimated_inertial[sat].r)[:,0] - np.asarray(self.true_inertial[sat].r)[:,0], 'r')
            #     ax[0].plot(t, np.asarray(self.estimated_inertial[sat].r)[:,1] - np.asarray(self.true_inertial[sat].r)[:,1], 'g')
            #     ax[0].plot(t, np.asarray(self.estimated_inertial[sat].r)[:,2] - np.asarray(self.true_inertial[sat].r)[:,2], 'b')
            #     ax[0].set_title('Inertial Frame Position Error')
            #     ax[0].set_ylabel('[km]')
            #     ax[1].plot(t, np.asarray(self.estimated_inertial[sat].v)[:,0] - np.asarray(self.true_inertial[sat].v)[:,0], 'r')
            #     ax[1].plot(t, np.asarray(self.estimated_inertial[sat].v)[:,1] - np.asarray(self.true_inertial[sat].v)[:,1], 'g')
            #     ax[1].plot(t, np.asarray(self.estimated_inertial[sat].v)[:,2] - np.asarray(self.true_inertial[sat].v)[:,2], 'b')
            #     ax[1].set_title('Inertial Frame Velocity Error')
            #     ax[1].set_ylabel('[km/s]')
            # # MOTION: Inertial frame, orbit frame (relaitve motion), sensor frame (true relaitve motion)
            # fig = plt.figure(figsize=plt.figaspect(0.5))
            # ax = fig.add_subplot(1, 3, 1, projection='3d')
            # ax.plot( np.asarray(self.estimated_inertial[0].r)[:,0], np.asarray(self.estimated_inertial[0].r)[:,1], np.asarray(self.estimated_inertial[0].r)[:,2])
            # ax.set_title('Estimated Inertial Motion [km]')
            # ax = fig.add_subplot(1, 3, 2, projection='3d')
            # ax.plot( np.asarray(self.estimated_orbit[0].r)[:,0], np.asarray(self.estimated_orbit[0].r)[:,1], np.asarray(self.estimated_orbit[0].r)[:,2])
            # ax.set_title('Estimated Relative Motion in Orbit Frame [km]')
            # ax = fig.add_subplot(1, 3, 3, projection='3d')
            # ax.set_title('True Relative Motion in Sensor Frame [km]')
            # ax.plot( np.asarray(self.true_sensor[0].r)[:,0], np.asarray(self.true_sensor[0].r)[:,1], np.asarray(self.true_sensor[0].r)[:,2])

        # CKF performance
        fig = plt.figure(figsize=plt.figaspect(0.5))
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        ax.plot(np.asarray(self.preCKF.r)[:,0]*1000, np.asarray(self.preCKF.r)[:,1]*1000, np.asarray(self.preCKF.r)[:,2]*1000)
        ax.plot(np.asarray(self.estimated_orbit[0].r)[:,0]*1000, np.asarray(self.estimated_orbit[0].r)[:,1]*1000, np.asarray(self.estimated_orbit[0].r)[:,2]*1000)
        ax.plot(np.asarray(self.true_orbit[0].r)[:,0]*1000, np.asarray(self.true_orbit[0].r)[:,1]*1000, np.asarray(self.true_orbit[0].r)[:,2]*1000,'k')

        plt.show()


# -----------------------------------------------------------------------------
# --- SUPPORTING FUNCTIONS ---------------------------------    -------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------
# ODEINT FUNCTION: describes motion of a 2-body orbit
# -----------------------------------------------------------------
# INPUTS: state-state vector, t-time vector, mu_s-gravitational parameter of central body
def KeplarianOrbit(state, t, mu):
    # unpack
    R = state[:3]
    V = state[3:]
    # differential equations
    dRdt = V
    dVdt = -mu/np.linalg.norm(R)**3 * R
    # output
    return np.concatenate((dRdt, dVdt))
