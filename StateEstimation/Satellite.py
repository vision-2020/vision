# ----------------
# Satellite Class
# ----------------
# VISION: state estimation
# ----------------
# Author: Ben Hagenau
# Edited: 1/7/20

import numpy as np
import math as m
from .Deployer import Deployer
from .State import State
from .nonlinear_batch_filter.NLB_VISION import NLB
from .nonlinear_batch_filter.nonlinear_batch_functions import dfdz, dhdz, h, ode_relative_orbit

class Satellite(Deployer):

    def __init__(self, id):
        Deployer.__init__(self)
        self.id    = int(id)                              # satellite ID
        self.cov   = []        # list of covariance matrices
        self.tle   = np.array([], dtype=float)                # store final tle

# ----------------------------------------------------------------
# FRAME CONVERSION: sensor frame measurements to deployer orbit frame relative to deployer
# ----------------------------------------------------------------
# INPUTS: deplpyer: deployer object,
# OUTPUTS: satellite State() relative to deployer in deployer orbit frame
# ASSUMPTIONS: N/A
    def sensor_to_orbit(self, deployer):
        if self.cartesian.frame != 'S':
            raise Exception("satellite state must be represented in the sensor frame")
        if self.cartesian.relative != 'S':
            raise Exception("satellite state must be relative to the sensor frame")
        if len(self.cartesian.r) != len(deployer.cartesian.r):
            raise Exception("deployer state must be interpolated to satellite state epoch")

        temp = State() # to store new state values
        temp.frame = 'O'
        temp.relative = 'O'
        temp.t = self.cartesian.t
        for row in range (0,len(self.cartesian.r)):
            # satellite position relative to deployer in sensor frame
            r_S = self.cartesian.r[row] + deployer.position
            # DCM: sensor frame to deployer body frame
            DS = self.from_euler('321', deployer.orientation)
            # DCM: inertial frame to deployer orbit frame
            ON = self.from_vec(deployer.cartesian.r[row], deployer.cartesian.v[row])
            # DCM: deployer orbit frame to inertial frame
            ND = self.from_euler('321', deployer.attitude.r[row])
            # DCM: deployer body frame to orbit frame
            OD = np.dot(ON,ND)
            # DCM: sensor frame to deployer orbit frame
            OS = np.dot(OD,DS)
            # rotate relative position vector
            r_O = np.dot(OS,r_S)
            # orbit fame position relative to orbit frames
            temp.r.append(r_O)
            # convert velocity (if defined)
            if len(self.cartesian.v) != 0:
                h = np.linalg.norm(np.cross(deployer.cartesian.r[row], deployer.cartesian.v[row]))
                f_dot = h/np.linalg.norm(deployer.cartesian.r[row])**2
                # angular rate between frames (in the orbit frame)
                omega_SO = np.dot(np.dot(OS,DS.T), deployer.attitude.v[row]) - np.array([0,0,f_dot])
                # veclocity in orbit frame relative to orbit frame
                temp.v.append(np.dot(OS, self.cartesian.v[row]) + np.cross(omega_SO, np.dot(OS, self.cartesian.r[row])))
                # temp.v.append(np.dot(OS, self.cartesian.v[row]) + np.cross(omega_SO, r_O))
        self.cartesian = temp

        return self.cartesian

# ----------------------------------------------------------------
# FRAME CONVERSION: deployer orbit frame to inertial frame
# ----------------------------------------------------------------
# INPUTS: deployer: deployer object
# OUTPUTS: satellite State() relative to inertial frame relative to inertial frame
# ASSUMPTIONS: N/A
    def orbit_to_inertial(self, deployer):
        if self.cartesian.frame != 'O':
            raise Exception("satellite state must be represented in the deployer orbit frame")
        if self.cartesian.relative != 'O':
            raise Exception("satellite state must be relative to the deployer orbit frame")
        if deployer.cartesian.frame != 'N':
            raise Exception("deployer state must be represented in the inertial frame")
        if deployer.cartesian.relative != 'N':
            raise Exception("deployer state must be represented relative to the inertial frame")
        if len(self.cartesian.r) != len(deployer.cartesian.r):
            raise Exception("deployer state must be interpolated to satellite state epoch")

        temp = State() # to store new state values
        temp.frame = 'N'
        temp.relative = 'N'
        temp.t = self.cartesian.t
        for row in range(0,len(self.cartesian.r)):
            # satellite state vectors relaitve to orbit frame in orbit frame
            r_O = self.cartesian.r[row]
            v_O = self.cartesian.v[row]
            # DCM: inertial frame to deployer orbit frame
            ON = self.from_vec(deployer.cartesian.r[row], deployer.cartesian.v[row])
            # satellite position relative to orbit frame in inertial frame
            r_N = np.dot(ON.T, r_O.T)
            # angular rate of orbit frame relative to inertial frames
            h = np.linalg.norm(np.cross(deployer.cartesian.r[row], deployer.cartesian.v[row]))
            f_dot = h/np.linalg.norm(deployer.cartesian.r[row])**2
            # satellite velocity relative orbit frame in inertial frame
            v_N = np.dot(ON.T, v_O + np.cross(np.array([0,0,f_dot]), r_O))
            # satellite state vectors relative to inertial frame in inertial frame
            temp.r.append(r_N + deployer.cartesian.r[row])
            temp.v.append(v_N + deployer.cartesian.v[row])

        self.cartesian = temp

        return self.cartesian

# ----------------------------------------------------------------
# estimate orbital elements
# ----------------------------------------------------------------
# INPUTS: deployer: Deployer() with states at each measurement time
# OUTPUTS: coe: orbital elements of satellite
# ASSUMPTIONS: N/A
    def estimate_coe(self, deployer):
        self.coe = np.asarray(self.coe[0])
        return self.coe

# ----------------------------------------------------------------
# NONLINEAR BATCH FILTER: filter relative measurements to obtain relative estimates
# ----------------------------------------------------------------
# INPUTS:
# OUTPUTS:
# ASSUMPTIONS: N/A
    def estimate_state(self, deployer, x0, R, P0):
        # define environment parameters
        env_params = {"mu" : 3.986004415*10**5, "r_e" : 6378}
        # initialize filter
        nlb = NLB(ode_relative_orbit, dhdz, h)
        # run filter
        _, _ = nlb.run(self.cartesian.r, self.cartesian.t, x0, R, deployer, P0_init=P0, env_params=env_params)
        self.cartesian.r = nlb.estimates[:,[0,1,2]]
        self.cartesian.v = nlb.estimates[:,[3,4,5]]
        self.cov = nlb.P

        return nlb

# ----------------------------------------------------------------
# assemble satellite two-line elements (http://www.stltracker.com/resources/tle)
# ----------------------------------------------------------------
# INPUTS:
# OUTPUTS:
# ASSUMPTIONS: N/A
    def get_tle(self):
        em = 2*np.arctan(np.sqrt((1-self.coe[1])/1+self.coe[1])*np.tan(self.coe[-1]/2))
        ma = em - self.coe[1]*np.sin(em)
        # line 1 content
        lineNumber1 = '1'
        catalogueNumber1 = '25544'
        classification = 'U'
        internationalDesignator = '20067' + 'A' # last two digits of launch year+launch number that year + peice of launch
        epoch = '20' + '264.51782528'# last two digits of year + day of the year and fractional portion of the day
        nd = '-.00002182'
        ndd = '00000-0'
        bstar = '-11606-4'
        ephemerisType = '0'
        setNumber = '1'
        checkSum1 = 7
        # line 2 content
        lineNumber2 = '2'
        catalogueNumber2 = catalogueNumber1
        inc = str(np.rad2deg(self.coe[2]))[:7]
        raan = str(np.rad2deg(self.coe[3]))[:8]
        ecc = str(self.coe[1])[2:9]
        aop = str(self.coe[4])[:8]
        ma = str(ma)[:8]
        n = str(np.sqrt(3.986004415*10**5/self.coe[0]**3)*(86400/(2*np.pi)))[:11]
        revolutionNumber = '1'
        checkSum2 = '4'

        return 0

# NOTATION:
#   r_#, where # is the frame representation of vector r
# FLAGS:
#   sensor IDs: 0: TOF, 1: monochrome
#   frames: sensor: 0, deployer orrbit: 1, inertial: 2

# ----------------------------------------------------------------
# KALMAN FILTER: filter relative measurements to obtain relative estimates
# ----------------------------------------------------------------
# INPUTS:
# OUTPUTS:
# ASSUMPTIONS: N/A
    def kalman(self, deployer):

        # initial conditions - replace with external file reference
        v_nom = 1.75 / 1000  # m/s -> km/s ~ nominal velocity estimate
        v_sig = 1e-6# 0.15 / 1000  # m/s -> km/s ~ nominal velocity variance estimate
        r_sig = 1e-3
        eta = (3.986004415*10**5 / deployer.coe[0][0] ** 3) ** .5  # deployer (chief) mean motion ### changed to deployer specific eta

        # initial estimate uncertainty covariance. needs tuning. eventually from file/deployer object. (square)
        P0 = np.mat([[r_sig**2, 0, 0, 0, 0, 0],
                     [0, r_sig**2, 0, 0, 0, 0],
                     [0, 0, r_sig**2, 0, 0, 0],
                     [0, 0, 0, v_sig ** 2, 0, 0],
                     [0, 0, 0, 0, v_sig ** 2, 0],
                     [0, 0, 0, 0, 0, v_sig ** 2]])
        # measurement translation matrix. we only measure position, so velocity -> 0. eventually from file/deployer.
        H = np.mat([[1, 0, 0, 0, 0, 0],
                    [0, 1, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0]])

        # other initial covariances & identity matrix. needs tuning. eventually form file/deployer.
        R = np.identity(6) * 1e-5 # see Ward thesis / sensor data (spec sheets/collected)
        I = np.identity(6)

        y = [np.concatenate((ri, [0, 0, 0])) for ri in self.cartesian.r]
        t = self.cartesian.t

        dir0 = y[-1][0:3]
        dir0 = (dir0 / np.linalg.norm(dir0)).tolist()

        # x0 = np.array([[0, 0, 0, dir0[0] * v_nom, dir0[1] * v_nom, dir0[2] * v_nom]]).T ### removed double brackets...
        # x0 = np.array([[1.12729360e-07, 3.29823171e-07, 1.96939301e-06, 0.00055126, 0.00101687, 0.00081369]]).T
        x0 = np.array([-1.3e-04, 4.4e-04, -2.3e-04, 1.5e-03, -8.6e-04, 5.2e-05])

        # set initial state/covariance to the a posteriori estimates for previous step 0
        xk = x0
        Pk = P0

        r = [x0[:3]]  # collect all state estimates for position    ### initialized with x0
        v = [x0[3:]]  # collect all state estimates for velocity    ### initialized with x0
        P = [np.diag(P0)]  # collect all estimate covariances for analytics   ### initialized with x0

        for k in range(1, len(t)):
            # previous estimates
            xj = np.copy(xk) # 6x1 np.array
            Pj = np.copy(Pk) # 6x6 np.array
            # next measurement
            yk = np.copy([y[k]]).T # 6x1 np.array
            # dynamics update
            psi = eta * float(t[k] - t[k - 1])
            Fj = [[4 - 3 * m.cos(psi), 0, 0, m.sin(psi) / eta, 2 / eta * (1 - m.cos(psi)), 0],
                  [6 * (m.sin(psi) - psi), 1, 0, 2 / eta * (m.cos(psi) - 1), 4 / eta * m.sin(psi) - 3 * psi / eta, 0],
                  [0, 0, m.cos(psi), 0, 0, m.sin(psi) / eta],
                  [3 * eta * m.sin(psi), 0, 0, m.cos(psi), 2 * m.sin(psi), 0],
                  [6 * eta * (m.cos(psi) - 1), 0, 0, -2 * m.sin(psi), -3 + 4 * m.cos(psi), 0],
                  [0, 0, -eta * m.sin(psi), 0, 0, m.cos(psi)]]
            Fj = np.array(Fj)
            # time update
            Pk_ = Fj @ Pj @ Fj.T # 6x6
            Kk = Pk_ @ H.T @ np.linalg.pinv(H @ Pk_ @ H.T + R)
            # measurement update
            xk_ = Fj @ xj
            # xk_ = Fj @ x0
            xk = xk_ + Kk @ (yk - H @ xk_)
            Pk = (I - Kk @ H) @ Pk_ @ (I - Kk @ H).T + Kk @ R @ Kk.T
            r_i = np.array([xk[i, 0] for i in [0, 1, 2]])
            v_i = np.array([xk[i, 0] for i in [3, 4, 5]])
            r.append(r_i)
            v.append(v_i)
            P.append(np.diag(Pk))

        filtered_states = State()
        filtered_states.frame = 'O'
        filtered_states.relative = 'O'
        filtered_states.r = r
        filtered_states.v = v
        filtered_states.t = t
        filtered_states.sensor = self.cartesian.sensor

        self.cartesian = filtered_states
        self.cov = P

        return self.cartesian
