import numpy as np
from scipy.interpolate import interp1d
import csv
from .Deployer import Deployer
from .Satellite import Satellite
from .State import State

# read in a csv file of satellite or deployer data
def read_csv(filename, id=None):
    with open(filename+'.csv') as file:
        stream = csv.reader(file, delimiter=',')
        if 'Satellite' in filename:
            id = int(filename[-1])  # id should be the last character in the filename
            data = [[id]+[float(elem) for elem in row] for row in stream]
        else:
            data = [[float(elem) for elem in row] for row in stream]
    return data

# data is list of lists
# inner list: ["satellite id", "sensor id", "x", "y", "z", "t"]
def format_satellite_data(satellite_data, satellites=None, frame='S'):
    if satellites is None:
        satellites = [Satellite(0)]
        satellites[0].cartesian.frame = frame
        satellites[0].cartesian.relative = frame
    for element in satellite_data:
        sat_id = int(element[0])
        while sat_id > len(satellites)-1:
            satellites.append(Satellite(len(satellites)))
            satellites[-1].cartesian.frame = frame
            satellites[-1].cartesian.relative = frame
        satellites[sat_id].cartesian.r.append(np.array(element[1:4]))
        if len(element) > 7:
            satellites[sat_id].cartesian.v.append(np.array(element[4:7]))
        satellites[sat_id].cartesian.t.append(element[-1])
    return satellites


# ------------------------------------------------------------------------------
# format deployer object and interpolate states
# ------------------------------------------------------------------------------
# INPUTS:
#   satellite_data: satellite data (from read_csv()) -[satID, x,y,z,xd,yd,zd,t]
#   deployer_data: deployer data (from read_csv()) -[x,y,z,xd,yd,zd,t]
#   mission_data: mission data (from read_csv()) -[position, orientation]
def format_deployer(satellite_data, deployer_data, mission_data):
    # initialize deployer object
    deployer = Deployer()
    deployer_data = np.asarray(deployer_data)
    # get measurement times
    measurementTimes = np.asarray(satellite_data)[:,-1]
    # define interpolating functions for deployer states
    deployerX = interp1d(deployer_data[:,-1], deployer_data[:,0], kind='cubic')
    deployerY = interp1d(deployer_data[:,-1], deployer_data[:,1], kind='cubic')
    deployerZ = interp1d(deployer_data[:,-1], deployer_data[:,2], kind='cubic')
    deployerXd = interp1d(deployer_data[:,-1], deployer_data[:,3], kind='cubic')
    deployerYd = interp1d(deployer_data[:,-1], deployer_data[:,4], kind='cubic')
    deployerZd = interp1d(deployer_data[:,-1], deployer_data[:,5], kind='cubic')
    deployerThetaX = interp1d(deployer_data[:,-1], deployer_data[:,6], kind='cubic')
    deployerThetaY = interp1d(deployer_data[:,-1], deployer_data[:,7], kind='cubic')
    deployerThetaZ = interp1d(deployer_data[:,-1], deployer_data[:,8], kind='cubic')
    deployerOmegaX = interp1d(deployer_data[:,-1], deployer_data[:,9], kind='cubic')
    deployerOmegaY = interp1d(deployer_data[:,-1], deployer_data[:,10], kind='cubic')
    deployerOmegaZ = interp1d(deployer_data[:,-1], deployer_data[:,11], kind='cubic')
    # interpolate deployer states to measurement times
    deployer.cartesian.frame = 'N'; deployer.cartesian.relative = 'N'
    deployer.cartesian.t = measurementTimes; deployer.attitude.t = measurementTimes
    deployer.cartesian.r = np.column_stack((deployerX(measurementTimes),\
                         deployerY(measurementTimes), deployerZ(measurementTimes)))
    deployer.cartesian.v = np.column_stack((deployerXd(measurementTimes),\
                         deployerYd(measurementTimes), deployerZd(measurementTimes)))
    deployer.attitude.r = np.column_stack((deployerThetaX(measurementTimes),\
                         deployerThetaY(measurementTimes), deployerThetaZ(measurementTimes)))
    deployer.attitude.v = np.column_stack((deployerOmegaX(measurementTimes),\
                         deployerOmegaY(measurementTimes), deployerOmegaZ(measurementTimes)))
    # assign position and orientation of VISION on deployer
    deployer.position = mission_data[0]
    deployer.orientation = mission_data[1]

    return deployer
# deployer_data format:
# [i][0]: array of timestamp that correlates to [1:7]
# [i][1:4]: vector representing x, y, and z position of deployer in Earth frame relative to Earth
# [i][4:7]: vector representing Euler angles for deployer in Earth frame relative to Earth
# def format_deployer(satellite_data, deployer_data, physical_data_filename):
#     # Create new deployer object.
#     deployer = Deployer()
#     # Interpolate data such that timestamps for deployer data match timestamps for satellite data.
#     # Assume timestamps are relatively synchronous.
#     # Assumes small time differences such that linear interpolation is relatively valid.
#     t_dep = deployer_data[0]
#     t_sat = list(set(satellite_data[:, 5]))
#     i = 0
#     for ts in t_sat:
#         while i < len(t_dep)-1 and t_dep[i] < ts:
#             i += 1
#         if i == 0:
#             r = (ts - t_dep[i])/(t_dep[i+1] - t_dep[i])
#         else:
#             r = (ts - t_dep[i-1])/(t_dep[i] - t_dep[i-1])
#         dep_interp = deployer_data[i-1, 0:7] + r*deployer_data[i, 0:7]
#         deployer.cartesian.t.append(dep_interp[0])
#         deployer.cartesian.r.append(dep_interp[:, 1:4])
#         deployer.attitude.t.append(dep_interp[:, 0])
#         deployer.attitude.v.append(dep_interp[:, 4:7])
#     # Get data from external file, assumed to be set before launch based on how VISION is attached to the deployer.
#     with open(physical_data_filename, newline='\n') as csv_file:
#         physical_data = csv.reader(csv_file, delimiter=',')
#         deployer.position = physical_data[0]
#         deployer.orientation = physical_data[1]
#     # Return new deployer object.
#     return deployer
