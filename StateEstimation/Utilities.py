import numpy as np
import yaml
import os
from pathlib import Path
from .Deployer import Deployer
from.Satellite import Satellite
from .VISION_SE_Test import *
from .VISION_Simulation import Simulation


def get_settings():
    f = open("currSimConfigFile")
    configFileName = f.read()
    configFileName = os.getcwd()+'\\config\\'+configFileName+'.yaml'
    with open(configFileName, 'r') as stream:
        settings = yaml.load(stream)
    settings['CONFIG_PATH'] = configFileName
    return settings


def get_transl_vec(settings):
    VISION_tube_num = settings['VANTAGE_POS']
    launch_tube_num = settings['DESIRED_DEPLOYMENT_TUBE']
    if launch_tube_num == VISION_tube_num:
        msg = 'You can''t launch (tube ' + \
              str(launch_tube_num) + \
              ') from the same tube as VISION (tube ' + \
              str(VISION_tube_num) + ')'
        raise ValueError(msg)
    rho = settings['RHO']
    nabla = settings['NABLA']
    # convert focal length from [mm] to [cm]
    f = settings['FOCAL_LENGTH'] / 10.
    p_optical = settings['P_OPTICAL']
    # need to figure out where the VCF origin is relative to the deployer
    # origin [cm]
    d_optical = p_optical - f
    z_loc_of_VCF = d_optical
    return [nabla, rho, z_loc_of_VCF]


def get_tube_origin(settings, translVec):

    # get deployment tube
    desired_tube = int(settings['DESIRED_DEPLOYMENT_TUBE'])

    # save deployer geometry variables
    mu = settings['MU']
    psi = settings['PSI']
    gamma = settings['GAMMA']
    L = settings['L']

    tube_coeffs = settings['TUBE_ORIGIN_COEFFS'][desired_tube - 1]
    x_coeff = tube_coeffs['x']
    y_coeff = tube_coeffs['y']
    z_coeff = tube_coeffs['z']

    des_tube_x = (x_coeff[0] * psi + 0.5 * x_coeff[1] * gamma)
    des_tube_y = y_coeff[0] * mu
    des_tube_z = (0.5 * L) * z_coeff[0]

    # now, translate this tube origin to its correct place in the global
    # coordinate system by adding the translation that moves the tube origin by
    # the desired final translation vector
    des_tube_x += translVec[0]
    des_tube_y += translVec[1]
    des_tube_z += translVec[2]

    # package output
    return [des_tube_x, des_tube_y, des_tube_z]

#
# @brief      Adds all cubesats to the current simulation, places them in the
#             desired tube in the right order, and prescribes their motion
#
# @warning    assumes the deployer tube origin is given at the planar center at
#             the back of the tube and that the cubesat origin in C4D is at the
#             CubeSat's origin
#
# @param      settings         Settings dict from YAML config
# @param      des_tube_origin  a list containing the desired deployer tube's
#                              origin in x,y,z coords in the global simulation
#                              coord system
#                              [x, y, z] ~ [cm]
# @param      doc              current C4D document to add cubesats to
#
# @return     a list of each C4D cubesat object added to the simulation
def setup_cubesats(settings, tube_origin):
    sat_ICs = []
    simLoc = os.getcwd()

    cubeSatSizes_strs = settings['CUBESATS_SIZES']
    cubeSatSizes_conversion = settings['U_STR_TO_U']

    # [cm]
    u_size = settings['U_SIZE_DEF']

    # the length of the little tabs on either end of a cubesat
    # [cm]
    cubeSat_tab_size = settings['CUBESAT_TAB_LENGTH']

    # initialize the back wall to be the back of the deployer tube
    # [cm]
    curr_back_wall = tube_origin[2]

    # The center of each CubeSat will be along the boresight of the deployer
    # tube
    # [cm]
    pos = [tube_origin[0], tube_origin[1], curr_back_wall]

    # shouldn't need to rotate the cubeSats
    # [rad]
    rot = [0, 0, 0]

    # the cubesat should start flush with the back of the deployer tube
    # [cm]
    currSep = 0

    n = len(cubeSatSizes_strs)

    for ii in range(0, n):

        # angular and linear velocity vectors of Cubesat
        # [cm/s]
        v_x = settings['CUBESAT_LIN_VELS']['v_x'][ii]
        v_y = settings['CUBESAT_LIN_VELS']['v_y'][ii]
        v_z = settings['CUBESAT_LIN_VELS']['v_z'][ii]
        v = [v_x, v_y, v_z]

        # [rad/s]
        omega_x = settings['CUBESAT_ROT_VELS']['omega_x'][ii]
        omega_y = settings['CUBESAT_ROT_VELS']['omega_y'][ii]
        omega_z = settings['CUBESAT_ROT_VELS']['omega_z'][ii]
        omega = [omega_x, omega_y, omega_z]

        # get a number (e.g. 1, 2, 3, etc.) corresponding to a size of ('1U',
        # '2U', '3U', etc.)
        # [U]
        CubeSatSize = cubeSatSizes_conversion[cubeSatSizes_strs[ii]]

        # Import the CubeSat Model
        cubesat = settings['CUBESAT_FILEPATHS'][cubeSatSizes_strs[ii]]
        fullFile = os.path.join(simLoc, cubesat)

        # get length of the CubeSat Model
        # [cm]
        cubesat_length = CubeSatSize * u_size + 2 * cubeSat_tab_size

        # Position the z-location of the CubeSat's origin (position in the
        # length of the tube)
        # [cm]
        pos[2] = curr_back_wall + cubesat_length / 2

        # update the separation between the current and next cubesat
        if ii < n - 1:
            currSep = settings['CUBESAT_SEPARATIONS'][ii]

        # Move the "back wall" to be the front of the previous cubesat, plus
        # any separation bewtween each cubesat
        curr_back_wall += cubesat_length + currSep

        # need to define all animation inputs
        posStart = [pos[abs(i)-1]*np.sign(i) for i in [3, -1, 2]]
        rotStart = rot[:]

        # convert [cm]->[km]
        posStart = [x/1e5 for x in posStart]
        v = [x/1e5 for x in v]

        # construct dict for this satellite's initial conditions
        this_sat_ICs = {"r0": posStart,
                        "v0": v,
                        "o0": rotStart,
                        "w0": omega}

        # add this satellite to the list of all satellites being simulated
        sat_ICs.append(this_sat_ICs)

    return sat_ICs


def setup_deployer(settings):

    # Construct deployer object
    deployer = Deployer()
    deployer.cartesian.frame = 'N';
    deployer.cartesian.relative = 'N'
    deployer.attitude.frame = 'D';
    deployer.attitude.relative = 'N'

    # Construct initial conditions for satellites in sensor frame relative to sensor
    mu = float(settings['ORBIT_MU'])
    # deployer.cartesian.v = [np.array([0, v_d/np.sqrt(2), v_d/np.sqrt(2)])] # [km/s]
    coe = settings['DEPLOYER_COE']
    deployer.coe = [coe['a'][0], coe['e'][0], coe['i'][0], coe['w'][0], coe['W'][0], coe['v'][0]]

    # Deployer inertial state from orbital elements (validated with example in VALLADO p.119)
    p = deployer.coe[0] * (1 - deployer.coe[1] ** 2)  # semi-latus rectum
    R_P = np.array([(p * np.cos(deployer.coe[5])) / (1 + deployer.coe[1] * np.cos(deployer.coe[5])),
                    (p * np.sin(deployer.coe[5])) / (1 + deployer.coe[1] * np.cos(deployer.coe[5])),
                    0])  # perifocal frame position inertial of satellite
    V_P = np.array([-np.sqrt(mu / p) * np.sin(deployer.coe[5]),
                    np.sqrt(mu / p) * (deployer.coe[1] + np.cos(deployer.coe[5])),
                    0])  # perifocal frame velocity inertial of satellite
    NP = deployer.from_euler('313',
                             [-deployer.coe[4], -deployer.coe[2], -deployer.coe[3]])  # DCM from perifocal to inertial

    # Set state elements in deployer object
    deployer.cartesian.r = [np.dot(NP, R_P)]
    deployer.cartesian.v = [np.dot(NP, V_P)]
    deployer.attitude.r = [np.deg2rad(np.array([0, 0, 0]))]  # [rad]
    deployer.attitude.v = [np.deg2rad(np.array([0, 0, 0]))]  # [rad/s]
    deployer.position = np.array([0, 0, 0])  # [km]
    deployer.orientation = np.deg2rad(np.array([0, 0, 0]))  # [rad]

    return deployer
