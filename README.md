# VISION_Simulation (WARNING: WIP, USE AT OWN RISK)
*For optimal viewing of this document (and all `*.md` files), try opening it in a text editor that supports syntax highlighting for markdown `*.md` files (e.g. Sublime Text 2+).*

![Alt](docs/assets/AstorTheOcelot.png "AstorTheVANTAGEOcelot")

**A YAML-powered Python project to interface with C4D / Blensor to simulate the deployment of CubeSats from a NanoRacks ISS deployer. Adapted from VANTAGE_Simulation.**

*This repository is the home for the VISION Simulation project, which is a component of the larger [VISION (2019-20) project](https://drive.google.com/drive/folders/1SbkY5wjO9he67aB8XnaSnfTiQf65CChv?usp=sharing) at the [CU Boulder Ann and H.J. Smead Aerospace Engineering Sciences Department](https://www.colorado.edu/aerospace/). Our project is sponsored by a collaboration between [CCAR](https://www.colorado.edu/ccar/) and [NanoRacks](http://nanoracks.com/). You can find more of the software developed for the VANTAGE project on the [VANTAGE organization page](https://github.com/vantagecu).*

---

## About this Repo

This repo contains the models, documentation, and code necessary to simulate any VISION use-case CubeSat deployment using both monochrome and ToF sensing. This repo also includes state estimation software developed for the VISION package.


We use [Cinema 4D R20 (C4D)](https://www.maxon.net/en-us/products/cinema-4d/overview/) to simulate our monochrome camera's properties and [Blensor](https://www.blensor.org/) to simulate our ToF flight sensor.

![Alt](docs/assets/c4D_logo.png "c4d")
![Alt](docs/assets/blensor2_0.png "blensor")

The important directories contained in this repo are:

* `config`: this dir contains the YAML configuration files used to completely automate the creation of a C4D animation case.

* `3d_assets`: this dir contains the 3D deployer model as well as the 3D cubesat models used in the simulation.

* `docs`: the homeland for wild simulation documentation. There are videos in here showing visualizations from both C4D and Blensor simulation of the same deployment, and actual ISS CubeSat deployment footage from [NanoRacks](http://nanoracks.com/), among other useful things.

---

## Directory Structure (OUT OF DATE, WIP)

The following directory structure shows the directory structure if you run the full simulation framework with a configuration file named: `configName = SPS_template.yaml`. The `runC4D.py` code will automatically build a "case" directory from this config file, named with the following convention `configName + '-' + '%H_%M_%S_%Y_%m_%d'`.

```bash
.
├── **4_Simulation_Cases**
│   ├── ...
│   └── *SPS_template-09_13_17_2019_04_19*
│       ├── *ToF_Data*
│       │   ├── Sample1
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00001.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00002.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00003.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00004.pcd
│       │   │    └── ...
│       │   ├── Sample2
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00001.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00002.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00003.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00004.pcd
│       │   │    └── ...
│       │   ├── Sample3
│       │   │    └── ...
│       │   ├── Sample4
│       │   │    └── ...
│       │   └── ...
│       ├── *Optical_Data*
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0000.png
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0001.png
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0002.png
│       │   └── ...
│       ├── SPS_template-09_13_17_2019_04_19.blend
│       ├── SPS_template-09_13_17_2019_04_19.c4d
│       ├── SPS_template-09_13_17_2019_04_19.fbx
│       ├── SPS_template_truth_data.json
│       └── SPS_template.yaml
└── **VANTAGE_Simulation**
    ├── 3d_assets
    │   ├── 1U_Slug.SLDPRT
    │   ├── 2U_Slug.SLDPRT
    │   ├── 3U_Slug.SLDPRT
    │   ├── 4U_Slug.SLDPRT
    │   ├── 5U_Slug.SLDPRT
    │   ├── 6U_Slug.SLDPRT
    │   └── NR_Dual_Quadpack_Simulation_Assem.SLDPRT
    ├── cleanCaseToFDirs.py
    ├── *config*
    │   ├── configArchive
    │   │   ├── config_simulation_Josh_CDR.yaml
    │   │   └── ...
    │   ├── final_vantage_sim_configs
    │   │   ├── VTube4_DTube3_CubeSats3U2U1U_Speed100cmps.yaml
    │   │   └── ...
    │   ├── config_simulation_template.yaml
    │   └── **SPS_template.yaml**
    ├── currSimConfigFile
    ├── docs
    │   ├── deployer_VANTAGE_geometry_and_coordinate_frame_defs.pdf
    │   ├── VANTAGE_tube4_cubesat_tube3_63cm_downrange.png
    │   └── ...
    ├── README.md
    ├── runBlensor.py
    └── runC4D.py
```

---

## Simulation Parameterization / Geometry Definition

The definition of deployer variables used in the parameterization and case creation can be found in (shown below):

`<SIMULATION>\docs\deployer_VANTAGE_geometry_and_coordinate_frame_defs.pdf`

The `docs` directory contains a lot of the relevant information used in the process of creating and validating the simulation.

![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-1.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-2.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-3.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-4.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-5.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-6.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-7.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-8.png)
![Alt](docs/assets/definitions/deployer_VANTAGE_geometry_and_coordinate_frame_defs-9.png)

---

### Description of Simulation Parameters

We have heavily scripted the creation of a C4D simulation case. Currently, the entire simulation is automated with the following being some major *settings you can change*:

* the classical orbital elements that represent the orbit of the deployer

* which deployment tube you are launching out of

* where the global coordinate system is centered - originally set to the center of tube 1 (11/5)

* the sequence and size of CubeSats that will be deployed

* The Optical / ToF camera parameters

* the initial motion of each individual CubeSat (linear and rotational velocities of each CubeSat centroid)

**WARNING**: we do not currently check for collision, so make sure you don't prescribe motion that will cause the CubeSats collide

*The description of what each parameter is should be evident in the location you set it, whether it be in the YAML config, or in the source itself.*

### How to Set the Simulation Parameters

**I would highly recommend using a text editor which supports YAML syntax highlighting - it will make editing the config file much less error prone and easier to read.**

In order to change these settings, make a copy of:
`<SIMULATION_LOCATION>/config/config_simulation_template.yaml`
in the `<SIMULATION_LOCATION>/config/` directory.

**USE `config_simulation_template.yaml` as your starter config NOT `SPS_template.yaml`**
*(I'm sorry I accidentally built this whole example with the `SPS_template` for some dumb reason and I'm scared to change it)*

Rename your new config file with a meaningful name, following a reasonable config naming convention.

Edit the config file variables as you see fit to create the correct C4D case. Make sure to visually inspect the case before you run a lengthy simulation.

**As I ran out of time, some of the ToF camera simulation parameters must be adjusted in runBlensor.py itself.**

#### Simulation Path Setting
***Make sure to set the path `SIM_DIR_LOCATION` in the YAML config file to be the full path string to wherever you cloned the `VISION_Simulation` directory.***

e.g. you cloned `VISION_Simulation` to your desktop: `/Users/<USERNAME>/Desktop`. Then, you should set the `SIM_DIR_LOCATION` key in the config file to be:
```yaml
---

SIM_DIR_LOCATION: '/Users/<USERNAME>/Desktop/VISION_Simulation'
```

If you encounter an error like:
`TypeError: 'NoneType' object is not callable`
while doing operations on objects, it likely means that you have set this path incorrectly and the simulation cannot find the 3d object files in `<SIMULATION_LOCATION>/3s_assets/`.

---

## Simulation Output Directory -- `4_Simulation_Cases`

Each time you run a simulation case in C4D and then in Blensor (the methodology for doing so is shown in [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) and [How to Render with C4D](#how-to-render-with-c4d) and [How to Run a Blensor Simulation](#how-to-run-a-blensor-simulation), an output "Case Directory" (an example case directory is shown throughout this document as `./4_Simulation_Cases/SPS_template-09_13_17_2019_04_19`) is created corresponding to the name and current time of the configuration file specified in `currSimConfigFile` (see [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) for more details on configuration).

This directory will contain the following files, as seen in the "Directory Structure" section of this document:

```bash
.
├── 4_Simulation_Cases
│   ├── ...
│   └── SPS_template-09_13_17_2019_04_19
│       ├── ToF_Data
│       │   ├── Sample1
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00001.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00002.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00003.pcd
│       │   │    ├── Sample1_SPS_template-09_13_17_2019_04_19_noisy00004.pcd
│       │   │    └── ...
│       │   ├── Sample2
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00001.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00002.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00003.pcd
│       │   │    ├── Sample2_SPS_template-09_13_17_2019_04_19_noisy00004.pcd
│       │   │    └── ...
│       │   ├── Sample3
│       │   │    └── ...
│       │   ├── Sample4
│       │   │    └── ...
│       │   └── ...
│       ├── Optical_Data
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0000.png
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0001.png
│       │   ├── SPS_template-09_13_17_2019_04_19_Camera_a0002.png
│       │   └── ...
│       ├── SPS_template-09_13_17_2019_04_19.blend
│       ├── SPS_template-09_13_17_2019_04_19.c4d
│       ├── SPS_template-09_13_17_2019_04_19.fbx
│       ├── SPS_template_truth_data.json
│       └── SPS_template.yaml
```

For the next description, I will generalize the specific string associated with the example case directory name - `SPS_template-09_13_17_2019_04_19`) - as `<CASE_DIR_NAME>` (e.g. `SPS_template-09_13_17_2019_04_19` <==> `<CASE_DIR_NAME>`). `<CASE_DIR_NAME>` refers to *any and all* case directory names generated during the simulation process.

`<CASE_DIR_NAME_LITE>` refers to the case directory name without the date string appended to it (e.g. `<CASE_DIR_NAME> = 'SPS_template-09_13_17_2019_04_19'`, `<CASE_DIR_NAME_LITE> = 'SPS_template'`)

A detailed description of the major case directory (`<CASE_DIR_NAME>`) components are as follows:

* `./4_Simulation_Cases/<CASE_DIR_NAME>/ToF_Data`: This directory contains pointcloud (`*.pcd`) data from the Blensor Simulation. Each "Sample" directory contains a full set of pointclouds generated from simulated noisy ToF scans over a simulated CubeSat deployment. The reason for having multiple scans is to have multiple full scans of the same deployment, each with different random noise, to test the robustness of our pointcloud processing algorithms against sensor noise. *This directory is only populated if you manually elect to produce ToF pointcloud in Blensor -- see [How to Run a Blensor Simulation](#how-to-run-a-blensor-simulation) for details*

* `./4_Simulation_Cases/<CASE_DIR_NAME>/Optical_Data`: This directory contains ray-traced renders (`*.png`) from the simulated VANTAGE monochrome camera generated by a C4D render. These simulated images have been calibrated and tested to be representative of images taken by the real monochrome camera we employ (see `*/docs/side_by_side_comparison.png`) *This directory is only populated if you manually elect to produce rendered images with C4D -- see [How to Render with C4D](#how-to-render-with-c4d) for details*

* `./4_Simulation_Cases/<CASE_DIR_NAME>/<CASE_DIR_NAME>.blend`: This is the blender file containing the full deployment animation and the full ToF simulation state after it has run. This is an optional, but recommended file to save so you can go back later and see exactly what ToF parameters were used. *see [How to Run a Blensor Simulation](#how-to-run-a-blensor-simulation)for details*

* `./4_Simulation_Cases/<CASE_DIR_NAME>/<CASE_DIR_NAME>.c4d`: This is the C4D file containing the C4D scenario with all objects, cameras, and C4D render settings. Saving this file is suggested to the user after the C4D simulation creation process. *see [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) for details*

* `./4_Simulation_Cases/<CASE_DIR_NAME>/<CASE_DIR_NAME>.fbx`: This filetype (`.fbx`) is a generic 3D file format that will contain all 3D objects, their animations, and their parameters (including the camera parameters). This file is created in C4D and then imported into Blensor to load in the same deployment scenario. Saving this file is suggested to the user after the C4D simulation creation process. *see [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) and [How to Run a Blensor Simulation](#how-to-run-a-blensor-simulation) for details*

* `./4_Simulation_Cases/<CASE_DIR_NAME>/<CASE_DIR_NAME_LITE>_truth_data.json`: This file, in the JSON data serialization format (please, please dear god use something like this (e.g. YAML, JSON, XML, etc.) *for all* file I/O) contains all of the actual 3D positions of each CubeSat's centroid in VCF ([cm]) at each time step ([s]). The JSON schema for the truth data is *roughly* this (below generated for `./4_Simulation_Cases/SPS_template-09_13_17_2019_04_19/SPS_template_truth_data.json`):
```json
  "$schema": "TruthData",
  # the entire truth data is an array of time values and the centroid locations
  # of all CubeSats at each time value
  "type": "array",
  "items": [
    {
      # time step object, only contains an actual number
      "type": "object",
      "properties": {
        # the simulation time step value
        "t": {
          "type": "number"
        },
        # the CubeSats State object
        "pos": {
          "type": "object",
          "properties": {
            # unique name given to each CubeSat based on the file used to
            # create the object and its deployment order
            "launch_num_3__3U_Slug.SLDPRT": {
              "type": "array",
              "items": [
                {
                  # v_hat_1 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_2 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_3 centroid location component in VCF [cm]
                  "type": "number"
                }
              ]
            },
            # unique name given to each CubeSat based on the file used to
            # create the object and its deployment order
            "launch_num_0__1U_Slug.SLDPRT": {
              "type": "array",
              "items": [
                {
                  # v_hat_1 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_2 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_3 centroid location component in VCF [cm]
                  "type": "number"
                }
              ]
            },
            # unique name given to each CubeSat based on the file used to
            # create the object and its deployment order
            "launch_num_1__1U_Slug.SLDPRT": {
              "type": "array",
              "items": [
                {
                  # v_hat_1 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_2 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_3 centroid location component in VCF [cm]
                  "type": "number"
                }
              ]
            },
            # unique name given to each CubeSat based on the file used to
            # create the object and its deployment order
            "launch_num_2__1U_Slug.SLDPRT": {
              "type": "array",
              "items": [
                {
                  # v_hat_1 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_2 centroid location component in VCF [cm]
                  "type": "number"
                },
                {
                  # v_hat_3 centroid location component in VCF [cm]
                  "type": "number"
                }
              ]
            }
          },
          # a CubeSat centroid array is req. for each CubeSat ALWAYS
          "required": [
            "launch_num_3__3U_Slug.SLDPRT",
            "launch_num_0__1U_Slug.SLDPRT",
            "launch_num_1__1U_Slug.SLDPRT",
            "launch_num_2__1U_Slug.SLDPRT"
          ]
        }
      },
      # must ALWAYS contain both timing and position for each time step
      "required": [
        "t",
        "pos"
      ]
    },

    # repeated t and pos object for each time step simulated
    ...
  ]
```

* `./4_Simulation_Cases/<CASE_DIR_NAME>/<CASE_DIR_NAME_LITE>.yaml`: This file contains the YAML configuration file that you specify in `./<SIMULATION_LOCATION>/currSimConfigFile` (see [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) for more details), which controls *almost* all of the simulation parameters (see [How to Run a Blensor Simulation](#how-to-run-a-blensor-simulation) for details on what parameters must be set outside the YAML config file). It is automatically copied to `<CASE_DIR_NAME>` during the C4D simulation setup so you know *exactly* what settings were used to create each simulation (for auditibility).

---

## How to Run a Truth Data Simulation

### `run_simulation.py`

1) Create or edit a config file to have the desired initial conditions.

2) Edit `./<SIMULATION_LOCATION>/currSimConfigFile` with the name of the config file you would like to run the simulation with (e.g. SPS_template to use SPS_template.yaml as your input config file).

3) Run `run_simulation.py`

4) The script should populate the folder `./<SIMULATION_LOCATION>/Data/Current/UnprocessedCentroids/` with 5 files: `deployer_data.csv`, `measurements.csv`, `mission_data.csv`, `true_intertial_frame.csv`, and `true_orbit_frame.csv`. You can use `measurements.csv` as truth data when centroids are processed later.

## How to Run a C4D Simulation

### `runC4D.py`

![Alt](docs/assets/usingC4D.PNG "usingC4D")

1) Open C4D (you can get an educational license from the Maxon website - see `./<SIMULATION_LOCATION>/docs/0 HowToGetCinema4d` for more detailed info)

2) Open the script manager (Script -> Script Manager) and the console (Script -> Console)

3) From the script manager, open `./<SIMULATION_LOCATION>/runC4D.py`

3.5) **If running everything for the first time, you will need to get the 'pyyaml' package on the c4d python path.**

* in the python script console (see image above) do the following:
```python
>>> import sys
>>> print(sys.path)
```

* now, unzip the contents of `./<SIMULATION_LOCATION>/site-packages.zip` and place them in a directory listed in the `print(sys.path)` call.

If on macOS, the directory to put the unzipped `./<SIMULATION_LOCATION>/site-packages.zip` could be:
`/Applications/MAXON/Cinema 4D R20 Education/resource/modules/python/libs/osx/python27.framework/lib/python2.7`

If on windows, the directory to put the unzipped `./<SIMULATION_LOCATION>/site-packages.zip` could be:
`C:\Program Files\MAXON\Cinema 4D R20 Education\resource\modules\python\libs\win64\python27.vs2008.framework\lib`

* you should now have the ability to `import yaml` and `import pip` from the c4d python script console.

* if you need to install more packages to the c4d python distribution (like `numpy`), now you can simply do (from the c4d python script console):

```python
>>> import pip
>>> pip.main(['install', 'numpy'])
>>> import numpy
```

4) Click the **Execute** button in the C4D Script Manager

5) The simulation will load everything in and set all necessary settings.

6) A window will pop up asking you to save the c4d file. Navigate to the "/Data/current/" directory (see the directory structure section for more details) and save the file here.

7) At this point you get to name the .c4d file. Save this as `trajectory.c4d`.

8) Now you will be asked to name the FBX output (animation / camera data) of the simulation. Save this as `trajectory.fbx`.

9) If you would like to actually generate representative images from the camera, you will need to **render** the camera's perspective of the deployment. See the [How to Render with C4D](#how-to-render-with-c4d) section for details.

---

## How to Render with C4D

![Alt](docs/assets/usingC4DRender.PNG "usingC4DRender")

1) After you have completed all steps in [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation), you must now edit the render settings. Go to Render -> Edit Render Settings...

2) In the Render settings, make sure to select the **ProRender** setting. You may have to toggle between render engines before ProRender activates.
You must use the ProRender engine to properly render everything. In R20.028, there is a bug that prevents automated choice of render engine. MAKE SURE that the ProRender menu is properly set!

3) In the ProRender settings, make sure you turn ON **Depth of Field**. All other render settings should be automatically set by the python module. Close the render settings menu.

4) To start a queue of renders, go to Render -> Render Queue.

![Alt](docs/assets/usingC4DRenderQueue.PNG "usingC4DRenderQueue")

5) "Open" the `trajectory.c4d` file in the directory `/Data/current/`.

6) Click the three dots on the right of the "output file" text field to select the output image file location and naming convention.

7) Navigate to the case directory for the `.c4d` file you are rendering, then navigate to the `/Optical/` directory.

8) Change the file name field to "camera".

9) Click "Start Render" and watch your CPU and GPU melt for an hour. :)

---

## How to Run a Blensor Simulation
### `runBlensor.py`

![Alt](docs/assets/usingBlensor.PNG "usingBlensor")

To begin this section, **you must have completed all steps in [How to Run a C4D Simulation](#how-to-run-a-c4d-simulation) first.** C4D is used for the simulation animation building, so you must have the .fbx file output to begin the Blensor simulation process.

FIRST TIME SETUP:

1) Navigate to the `./<SIMULATION_LOCATION>/Blender/` folder and open `Blender.exe`.

2) Navigate to the "Data Outliner" region editor. Right-click the default Cube object, then select "Delete." Navigate to File -> Save Startup File, and select this option. The cube should not appear on startup any more.

RUNNING A SIMULATION:

1) Start by opening `runBlensor.py` in your favorite text editor -- I prefer using desktop sticky notes.

2) Determine how many frames it takes in the simulation for the closest CubeSat to reach its desired distance away from the deployer. Enter this number as `MAX_FRAMES` in `runBlensor.py`. This will make the simulation take a ToF scan every frame from frame 1 to frame `MAX_FRAMES`.

3) **NOW you're ready to run!** yay. Navigate to `./<SIMULATION_LOCATION>/` and run `runBlensor.bat`.

4) This will open a command line window and a Blensor window. You can monitor the progress of the simulation from either window, but the command line window is more verbose.

***USE THE FOLLOWING AT YOUR OWN RISK***
*The following can be done more safely manually if you don't trust my file deletion regex or the absolute paths supplied.*

These steps should only be done once you are happy with your simulations and are ready to begin using the data for testing VANTAGE software.

5) Navigate to `*/<SIMULATION_LOCATION>/` from powershell / terminal and then run `cleanCaseToFDirs.py`. This removes all of the non-noisy ToF `.pcd` files from **ALL** `./4_Simulation_Cases/<ANY_CASE_NAME>/ToF_Data` directories.

6) OPTIONAL: Save the current scence as a `.blend` file in the case directory (e.g. `/TOF/<CURRENT_CASE_NAME>/<CURRENT_CASE_NAME>.blend`)

---

## How to Process ToF Data
### `VantageMain.m`

1) Ensure that there is clean data in the directory `/Data/current/TOF/`.

2) Open `VantageMain.m` in MATLAB. Run this script.

3) A new directory in `/Data/current/` should be created `/ProcessedCentroids/`. A `.csv` will appear for each identified satellite.

4) Copy and save the entire `/Data/current/` directory to the `/Data/` folder. Rename the copied folder. This contains all of the simulation metadata sans configs.

*Note: there is a certain amount of bias in the calculated centroids when compared to the truth data. This was subtracted out for the purpose of VISION's analysis work; however, it is likely an issue with frame definition and conversion between the simulation script, C4D, BlenSor, and the MATLAB processing script.*

---
## State Estimation Implementation
VISION's state estimation algorithm is comprised of a set of primary routines used in sequence:

1) data formatting

2) frame transformation from the Sensor frame to the deployer's orbit frame

3) state estimation using a nonlinear batch filter

4) frame transformation from deployer orbit frame to inertial frames

5) computation of orbital elements and TLE assembly

These processes are built into the class structures consisting of `Deployer.py`, `Satellite.py`, and `NLB_VISION.py` found in the `SateEstimation` folder. In operation, a single deployer object is made and a Satellite object is made for each satellite deployed.

### Class Structure
`Deployer` is the parent class containing methods used for frame transformations and cartesian state to orbital element conversion. It also contains all deployer state information for a deployment scenario as well as some mission meta-data.

Deployer Attributes:

1) cartesian: State() - cartesian state of the deployer

2) attitude: State() - attitude states of the  deployer

3) coe: list - deployer orbital elements at each time [SMA, ECC, INC, RAAN, AOP, TA] ([km, n/a, deg, deg, deg, deg])

4) orientation: np.array - orientation of sensor frame relative to the deployer's body frame in sensor frame coordinates.

5) position: np.array - position of sensor frame relative to the body frame in sensor frame coordinates.

Deployer Methods:

1) get_coe(): convert all cartesian states in deployer.cartesian into orbital elements and place in deployer.coe.

2) from_euler(): compute a directional cosine matrix (DCM) from euler angles using a specified sequence.

3) from_vec(): convert inertial cartesian orbital state to a DCM rotating from the inertial frame the orbit frame.

`Satellite` inherits from the deployer class and adds additional methods used for performing the state estimation process.

Satellite Attributes:

1) id: int - satellite identification designator

2) cov: list - covariances computed by the filter at each time

3) tle: str - two-line elements

Satellite Methods:

1) sensor_to_orbit(): convert all states in satellite.cartesian from the sensor frame relative to the sensor frame into the deployer orbit frame relative to the deployer orbit frame.

2) orbit_to_inertial(): convert all states in satellite.cartesian from the deployer orbit frame relative to the deployer orbit frame to the inertial frame relative to the inertial frame.

3) estimate_coe: select classical orbital elements (coe) used for tle

4) estimate_state(): implement nonlinear batch filter on satellite.cartesian and update to estimates. NOTE: this is not going to work unless satellite.cartesian is in the deployer orbit frame relative to the deployer orbit frame.

5) get_tle: assemble tle using estimates and designators. NOTE: this is incomplete. all quantities not computed using estimation results should be read in from a file that is defined prior to launch (i.e. international designators, drag params, n-dots, ...)

6) kalman(): linear kalman filter not used in implementation.

`VISION_NLB` is the nonlinear batch filter class containing the filter implementation and some useful plotting functions that should be removed for actual implementation. Note that ideally this should be modified to also estimate the deployer state along with the CubeSat states.

VISION_NLB Attributes:

1) odefun_dfdz: ode function used to integrate state and state transition matrix linearized about the state. Contains equations of motion and other accelerations.

2) dhdz: measurement sensitivity matrix (jacobian of nonlinear measurement function with respect to the state being estimated)

3) h: nonlinear measurement function (convert from states and system parameters to measurements)

VISION_NLB Methods:

1) run(): run the nonlinear batch filter!

2) propagate(): wrapper for propagation step within the filter, handling outputs from python's odeint().

3) PlotRVEstimateError(): given truth states, plot position and velocity estimate errors and covariance bounds. NOTE: remove for actual implementation

4) ShowPlots(): wrapper for plt.show().

`State` is a class used to generalize how state information is stored.

State Attributes:

1) frame: str -  frame of state representation

2) relative: str -  frame origin that states are relative to.

3) r: list - list of state position elements at each time.

4) v: list - list of state rate elements at each time.

5) t: list - time vector associated with states.

## Implementation
This section describes how the classes described above are used in actual implementation. A test implementation
 can be found in the `Implementation\state_estimation_implementation.py`. Note that this script also has a lot of plotting and analysis tools build into it and should not be used for actual mission implementation.

##### data formatting (step 1)
The state estimation algorithm expects to receive centroid measurements from the centroid determination algorithm developed by VANTAGE, the deployment manifest, and deployer inertial states and attitudes measured over the data collection period. Note that right now this information is passed into `Implementation\state_estimation_implementation.py` by files that are orginized in `Implementation\Data`. The current implementation expects the following files which can be produced by the deployment scenario simulation:

1) centroids.csv (i.e. `Implementation\Data\sats1_ecc0_00\centroids\Satellite0_01.csv`) - each row contains centroid information at a given measurement time. The columns contain [satellite id, x, y, z, time] where x,y,z are the sensor frame positions of the CubeSats in meters, and time is in seconds.

2) deployer_data.csv (i.e. `Implementation\Data\sats1_ecc0_00\scenario_data\deployer_data.csv`) - containing deployer state information during the deployment period. the colums are [x,y,z,xd,yd,zd,theta1,theta2,theta3,omega1,omega2,omega3,time] where x,y,z and xd,yd,zd are inertial position and velocity of the deployer in km and km/s, theta1,theta2,theta3 and omega1omega2omega3 are the deployer's inertial attitude and attitude rate in rad and rad/s. time is in seconds.

3) mission_data.csv (i.e. `Implementation\Data\sats1_ecc0_00\scenario_data\mission_data.csv`) - first row contains pointing position in meters and second row contains mounting orientation in rad. both represenated in the sensor frame and reflect the sensor frame relative to the deployer's orbit frame.

4) true_inertial_frame.csv (i.e. `Implementation\Data\sats1_ecc0_00\scenario_data\true_inertial_frame.csv`) - true CubeSat states relative to the inertial frame in the inertial frame. Columns are [satellite id, x,y,z, xd,yd,zd, time] with the same naming definitions as above.

5) true_orbit_Frame.csv (i.e. `Implementation\Data\sats1_ecc0_00\scenario_data\true_orbit_Frame.csv`) - true CubeSat states relative to the deployer's orbit frame in the deployer's orbit frame. Columns are [satellite id, x,y,z, xd,yd,zd, time] with the same naming definitions as above.

5) true_sensor_frame.csv (i.e. `Implementation\Data\sats1_ecc0_00\scenario_data\true_sensor_frame.csv`) - true CubeSat states relative to the sensor frame in the sensor frame. Columns are [satellite id, x,y,z, xd,yd,zd, time] with the same naming definitions as above.

The contents of these data files are passed into `format_deployer()` and `format_satellite_data()` in `Format.py` (NOTE: ignore read_csv() this is incomplete and not used bute could be used to make reading in files modular). These functions are described below:

1) format_deployer(): use `deployer_data.csv` and `mission_data.csv` to create a deployer object. This function also requires the measurement times because it interpolates the deployer's attitude and cartesian states to the measurement times.

2) format_satellite_data(): use `centroid.csv` to create a Satellite object for each CubeSat in the file, populating Satellite.cartesian, Satellite.t, and Satellite.id.

##### state estimation (steps 2-5)

For each satellite object produced by `format_satellite_data()` the following sequence must be run to estimate two line elements using the centroid measurements and deployer object.

`sensor_to_orbit()` -> `estimate_state()` -> `orbit_to_inertial()` -> `get_coe()` -> `estimate_coe()` -> `get_tle()`.

## State Estimation analysis

The files used to produce the results presented by VISION are in the `Implementation` folder.

1) `state_estimation_implementation.py` - read in data files for mission scenario run state estimation algorithm, and study filter performance. The performance study looks at propagation error and propagation error covariance in the CubeSat state estimates. NOTE: error is computed in the true satellites deployer frame to give each axis a useful physical interpretation. The error is computed using estimated and truth states, and state STM in the inertial frame.

2) `analysis_support.py` - methods that wrap useful functions used for running the state estimation algorithm and analyzing the results. NOTE: The functions here are complete but more could be added to clean up analysis code. This functions have good inline descriptions which are not repeated here.

3) `analysis.py` - perform normalized estimate error squared (NEES) testing for the nonlinear batch filter. This is very useful for tuning to make sure covariance and estimate error magnitudes match... by no means is the filter currently tuned and the tuning is sensitive to how well the initial state of the CubeSats is. Tuning should also be done with a much larger set of scenario data then was done by VISION.  


## Additional VISION scripts...
`additional_analysis` folder contains work done when studying the dynamics of deployed CubeSats and quantifying accuracy requiremnts. Note that this accuracy requirement quantification is not relevant as the estimations computed easily beat the origional requirements and propagation error should be compared to the current baseline performance as well as the standard propagation error for small satellite TLE which is 1000-2000 meters per data according to Celestrak.

1) `model_error` - study of how the model introduced by using the Hill-Clohesey-Whiltshire equations, a rectilinear projection, and the nonlinear equations of motion introduced different amounts of model error. Also looks at the effects of SMA and ECC of the deployer on this error.

2) `uncertainty_analysis` - study of if the centroids error computed using hardware measurements reflects a gaussian distribution using the Anderson-Darling test. NOTE: this should be done with way more data.

3) `requirements_quantification` - development of accuracy requirements. As stated before this should not be used and the estimate accuracy should be driven by propagation error.

---

## Contact Info
Questions or feature requests should be directed to:

    VANTAGE: Nicholas Renninger
    nicholas.renninger@colorado.edu

    VISION: Max Audick (simulation, C4D, Blensor, linear kalman filter)
    maximillian.audick@colorado.edu

    VISION: Ben Hagenau (true orbital state simulation, nonlinear batch filter, state estimation analysis and results)
    benhagenau@gmail.com
