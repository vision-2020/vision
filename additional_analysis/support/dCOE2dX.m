% ------------------------
% Construct linear mapping matrix to convert between small changes in
% orbital elements to changes in position (A*dCOE = dX)
% ------------------------
% INPUTS: 
%   f_in: 'dCOE or dX' specify input state 
%   X_in: input differential state vector
%   COE: chief classical orbital elements (km, s, radians)
%   dt: time past M0
%   f_out: 'dCOE or dX' specify input output 
% OUTPUT: 
%   X_out: output differential state vector
%   A: transition matrix (for dX to dCOE)
% NOTES: requires non zero i and w for chief

function [X_out, A] = dCOE2dX(f_in, X_in, COE, f_out)
%% Set Up
mu = 3.986e5; % [km^3/s^2]
% orbital elemets
a = COE(1); e = COE(2); i =  COE(3); RAAN =  COE(4); w = COE(5); M0 = COE(6);
q1 = e*cos(w); 
q2 = e*sin(w);

e = sqrt(q1^2 + q2^2);
mu = 398600.4418;
n = sqrt(mu/a^3);
% true lattitude
opts = optimset('TolX',1e-12);
E = fzero(@(E) M0 - E + e*sin(E), M0, opts);
f = 2*atan(sqrt((1+e)/(1-e))*tan(E/2));
theta = w + f;
% chief orbit geometry
p = a*(1-e^2);
h = sqrt(mu*p);
%r = p/(1+e*cos(f));
r = a*(1-q1^2-q2^2)/(1+q1*cos(theta) + q2*sin(theta));
% chief radial and tangential velocity components
Vr = (h/p)*(q1*sin(theta) - q2*cos(theta));
Vt = (h/p)*(1 + q1*cos(theta) + q2*sin(theta));
% useful simplifications
alpha = a/r;
v = Vr/Vt;
rho = r/p;
k1 = alpha*(1/rho - 1);
k2 = alpha*v^2/rho;
%% Define Transition Matrix (A inverse)
A11 = 2*alpha*(2 + 3*k1 + 2*k2);
A12 = -2*alpha*v*(1+ 2*k1 + k2);
A13 = 0;
A14 = 2*alpha^2*v*p/Vt;
A15 = (2*a/Vt)*(1 + 2*k1 + k2);
A16 = 0;

A21 = 0;
A22 = 1/r;
A23 = (cot(i)/r)*(cos(theta) + v*sin(theta));
A24 = 0;
A25 = 0;
A26 = -sin(theta)*cot(i)/Vt;
A31 = 0; 
A32 = 0; 
A33 = (sin(theta) - v*cos(theta))/r;
A34 = 0;
A35 = 0;
A36 = cos(theta)/Vt;
A41 = (1/(rho*r))*(3*cos(theta) + 2*v*sin(theta));
A42 = -(1/r)*((v^2/rho)*sin(theta) + q1*sin(2*theta) - q2*cos(2*theta));
A43 = -(q2*cot(i)/r)*(cos(theta) + v*sin(theta));
A44 = sin(theta)/(rho*Vt);
A45 = (1/(rho*Vt))*(2*cos(theta) + v*sin(theta));
A46 = q2*cot(i)*sin(theta)/Vt;
A51 = (1/(rho*r))*(3*sin(theta) - 2*v*cos(theta));
A52 = (1/r)*((v^2/rho)*cos(theta) + q2*sin(2*theta) + q1*cos(2*theta));
A53 = (q1*cot(i)/r)*(cos(theta) + v*sin(theta));
A54 = -cos(theta)/(rho*Vt);
A55 = (1/(rho*Vt))*(2*sin(theta) - v*cos(theta));
A56 = -(q1*cot(i)*sin(theta)/Vt);
A61 = 0;
A62 = 0;
A63 = -(cos(theta) + v*sin(theta))/(r*sin(i));
A64 = 0;
A65 = 0;
A66 = sin(theta)/(Vt*sin(i));

A_inv = [A11 A12 A13 A14 A15 A16;
     A21 A22 A23 A24 A25 A26;
     A31 A32 A33 A34 A35 A36;
     A41 A42 A43 A44 A45 A46;
     A51 A52 A53 A54 A55 A56;
     A61 A62 A63 A64 A65 A66];
 
%% Define Transition Matrix (A)
% x
A11 = r/a;
A12 = (Vr/Vt)*r;
A13 = 0;
A14 = -(r/p)*(2*a*q1 + r*cos(theta));
A15 = -(r/p)*(2*a*q2 + r*sin(theta));
A16 = 0;
% y
A21 = 0;
A22 = r;
A23 = 0;
A24 = 0;
A25 = 0;
A26 = r*cos(i);
% z
A31 = 0;
A32 = 0;
A33 = r*sin(theta);
A34 = 0;
A35 = 0;
A36 = -r*cos(theta)*sin(i);
% x_dot
A41 = -Vr/(2*a);
A42 = ((1/r)-(1/p))*h;
A43 = 0;
A44 = (Vr*a*q1 + h*sin(theta))/p;
A45 = (Vr*a*q2 - h*cos(theta))/p;
A46 = 0;
% y_dot
A51 = -(3*Vt)/(2*a);
A52 = -Vr;
A53 = 0;
A54 = (3*Vt*a*q1 + 2*h*cos(theta))/p;
A55 = (3*Vt*a*q2 + 2*h*sin(theta))/p;
A56 = Vr*cos(i);
% z_dot
A61 = 0;
A62 = 0;
A63 = (Vt*cos(theta)+Vr*sin(theta));
A64 = 0;
A65 = 0;
A66 = (Vt*sin(theta)-Vr*cos(theta))*sin(i);

A = [A11 A12 A13 A14 A15 A16;
     A21 A22 A23 A24 A25 A26;
     A31 A32 A33 A34 A35 A36;
     A41 A42 A43 A44 A45 A46;
     A51 A52 A53 A54 A55 A56;
     A61 A62 A63 A64 A65 A66];

if strcmp(f_in, 'dCOE') && strcmp(f_out,'dX')
    X_out = A*X_in;
elseif strcmp(f_in, 'dX') && strcmp(f_out,'dCOE')
    X_out = A_inv*X_in;
else 
    fprintf('Improper state specifications')
end

end

