% ----------------------------
% ASEN 6014: HW2
% ----------------------------
% Author: Benjamin. K. Hagenau
% Created: 9/29/19

% House Keeping
close all; clear all; clc
addpath C:\Users\Benha\OneDrive\Documents\MATLAB\functions

%% Problem 14.7
mu = 3.986e5; % [km^3/s^2]
a = 7500; e = 0; i = deg2rad(45); RAAN = deg2rad(20); w = deg2rad(30); M0 = deg2rad(20);
n = sqrt(mu/a^3);
r = -n*(2+e)/sqrt((1+e)*(1-e)^3);
x0 = 1; % [km]
X0_O = [x0, 0, 0, 0, r*x0, 0];

tspan = [0 1.2*2*pi/n];
params.n = n;
params.mu = mu;
% inertial chief
Xc_N = kep_cart('kep', [a e i RAAN w M0], 0, 'cart');
[ON] = Inertial2Hill(Xc_N(1:3), Xc_N(4:6));
Xd_N(1,1:3) = Xc_N(1:3)' + ON'*X0_O(1:3)';
omega = [0 0 norm(cross(Xc_N(1:3), Xc_N(4:6)))/norm(Xc_N(1:3))^2];
Xd_N(1,4:6) = Xc_N(4:6)' + ON'*(X0_O(4:6) + cross(omega,X0_O(1:3)))';

opts = odeset('RelTol',1e-9,'AbsTol',1e-9);
y0 = [Xc_N Xd_N X0_O];

[t,y] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);
for i = 1:length(t)
    R_rel_N = -y(i,1:3) + y(i,7:9);
    
    [ON] = Inertial2Hill(y(i,1:3), y(i,4:6));
    
    X_O(i,1:3) = ON*R_rel_N';
end
figure
plot3(y(:,13), y(:,14), y(:,15)); hold on
%plot3(X_O(:,1), X_O(:,2), X_O(:,3))
plot3(0,0,0,'ok')
title('Bounded Deputy Motion')
xlabel('Radial, [km]')
ylabel('Alongtrack, [km]')
zlabel('Orbit Normal, [km]')
legend('Deputy', 'Chief')

% ------------------------------------------------
% ------------------------------------------------
%% Problem 14.5
COE = [8000 0.1 deg2rad(10) deg2rad(10) deg2rad(10) deg2rad(10)];

dCOE = [.001 0 0 0 0 0]';
[dX, A] = dCOE2dX('dCOE', dCOE, COE, 0, 'dX');

% ------------------------------------------------
% ------------------------------------------------
%% Problem 14.3
for e = .001
    a = 7500;
    chief_COE = [a, e, deg2rad(45), deg2rad(20) deg2rad(30) deg2rad(20)];
    deputy_COE = chief_COE + [0 0.0001 deg2rad(0.01) 0 -deg2rad(0.01) deg2rad(0)];
    X0_c = kep_cart('kep', chief_COE, 0, 'cart');
    X0_d = kep_cart('kep', deputy_COE, 0, 'cart');
    X0_r_N = X0_d - X0_c; % (inertial)
    % conver to orbit frame
    omega_ON = [0 0 norm(cross(X0_c(1:3), X0_c(4:6)))/norm(X0_c(1:3))^2];
    ON = Inertial2Hill(X0_c(1:3), X0_c(4:6));
    X0_r(1:3) = ON*X0_r_N(1:3)'; % (chief orbit frame)
    X0_r(4:6) = ON*X0_r_N(4:6)'- cross(omega_ON, X0_r(1:3))';
    
    %% curvalinear IC
    r = norm(X0_c(1:3));
    r_dot = norm(X0_c(4:6));
    x = X0_r(1); y = X0_r(2); z = X0_r(3);
    u = X0_r(4); v = X0_r(5); w = X0_r(6);
    dtheta = atan2(y,r+x);

    dr = sqrt((r+x)^2 + y^2) - r;
    s = r*dtheta; 
    
    drdt = ((r+x)*(u+r_dot) + y*v)/(sqrt((r+x)^2 + y^2)) - r_dot;
    dsdt = r_dot*dtheta + r*((r+x)*v-y*(r_dot+u))/((r+x)^2 + y^2);
    
    X0_rc = [dr, s, z, drdt, dsdt, w];
    % define IC
    y0 = [X0_c X0_d X0_r X0_r X0_rc];
    
    params.mu =  3.986e5; % [km^3/s^2]
    params.n = sqrt(params.mu/a^3);
    tspan = [0 1*2*pi*sqrt(7500^3/params.mu)]; % [s]
    opts = odeset('RelTol',1e-9,'AbsTol',1e-9);
    
    [t,y] = ode45(@(t,y) odefun_comparison(t, y, params), tspan, y0, opts);
    
    %% from curvalinear to rectilinear
    dr = y(:,25); s = y(:,26); 
    r = vecnorm(y(:,1:3),2,2);
    dtheta = s./r;
    
    y(:,25) = (dr+r).*cos(dtheta) - r;
    y(:,26) = (dr+r).*sin(dtheta);
    
    X_r_N = y(:,7:9) - y(:,1:3);
    r_max = max(vecnorm(X_r_N,2,2));
    % inertial to Hill
    for i = 1:length(t)
        o_r = y(i,1:3)/norm(y(i,1:3));
        o_h = cross(y(i,1:3), y(i,4:6))/norm(cross(y(i,1:3), y(i,4:6)));
        o_theta = cross(o_h, o_r);
        ON = [o_r; o_theta; o_h;];
        X_r_O(i,1:3) = ON*X_r_N(i,1:3)';
    end
    figure(1)
    CW_error = vecnorm(y(:,19:21) - y(:,13:15),2,2);
    plot(e, CW_error(end),'ok'); hold on
    plot([0 .1], [1 1], 'k--')
    xlabel('$$e$$')
    ylabel('CW Error, [km]')
end
% plot: inertial motions
figure
plot3(y(:,1), y(:,2), y(:,3),'k'); hold on
plot3(y(:,7), y(:,8), y(:,9)); hold on
plot3(0, 0, 0, 'ob')
xlabel('X, [km]'); ylabel('Y, [km]'); zlabel('Z, [km]');
title('Inertial Orbital Motion')
legend('chief', 'deputy', 'Earth')
axis equal
% plot: relative orbit EOM in chief Hill
figure
plot3(y(:,13), y(:,14), y(:,15)); hold on
plot3(y(:,19), y(:,20), y(:,21)); hold on
plot3(y(:,25), y(:,26), y(:,27)); hold on
plot3(X_r_O(:,1), X_r_O(:,2), X_r_O(:,3))
plot3(0, 0, 0, 'ok')
% xlim([-r_max r_max])
% ylim([-r_max r_max])
% zlim([-r_max r_max])
xlabel('Crosstrack, [km]'); ylabel('Downtrack,[km]'); zlabel('Orbit Normal, [km]');
title('Relative Orbital Motion, Chief Hill Frame')
legend('Nonlinear EOM', 'CW rect.', 'CW curv.', 'truth', 'chief')

% ------------------------------------------------
% ------------------------------------------------
%% Problem 14.2
chief_COE = [7500, 0.01, deg2rad(45), deg2rad(20) deg2rad(30) deg2rad(20)];
deputy_COE = chief_COE + [0 0.0001 deg2rad(0.01) 0 -deg2rad(0.01) 0;];
X0_c = kep_cart('kep', chief_COE, 0, 'cart');
X0_d = kep_cart('kep', deputy_COE, 0, 'cart');
X0_r_N = X0_d - X0_c; % (inertial)
% conver to orbit frame
o_r = X0_c(1:3)/norm(X0_c(1:3));
o_h = cross(X0_c(1:3), X0_c(4:6))/norm(cross(X0_c(1:3), X0_c(4:6)));
o_theta = cross(o_h, o_r);
ON = [o_r; o_theta; o_h];
omega_ON = [0 0 norm(cross(X0_c(1:3), X0_c(4:6)))/norm(X0_c(1:3))^2];
X0_r(1:3) = ON*X0_r_N(1:3)'; % (chief orbit frame)
X0_r(4:6) = ON*X0_r_N(4:6)'- cross(omega_ON, X0_r(1:3))';
% define IC
y0 = [X0_c X0_d X0_r];

params.mu =  3.986e5; % [km^3/s^2]
tspan = [0 2*pi*sqrt(7500^3/params.mu)]; % [s]
opts = odeset('RelTol',1e-9,'AbsTol',1e-9);

[t,y] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);
X_r_N = y(:,7:9) - y(:,1:3);
r_max = max(vecnorm(X_r_N,2,2));
% inertial to Hill
for i = 1:length(t)
    o_r = y(i,1:3)/norm(y(i,1:3));
    o_h = cross(y(i,1:3), y(i,4:6))/norm(cross(y(i,1:3), y(i,4:6)));
    o_theta = cross(o_h, o_r);
    ON = [o_r; o_theta; o_h;];
    X_r_O(i,1:3) = ON*X_r_N(i,1:3)';
end

% plot: inertial motions 
figure
plot3(y(:,1), y(:,2), y(:,3),'k'); hold on
plot3(y(:,7), y(:,8), y(:,9)); hold on
plot3(0, 0, 0, 'ob')
xlabel('X, [km]'); ylabel('Y, [km]'); zlabel('Z, [km]');
title('Inertial Orbital Motion')
legend('chief', 'deputy', 'Earth')
axis equal
% plot: relative orbit EOM in chief Hill
figure
plot3(y(:,13), y(:,14), y(:,15)); hold on
plot3(X_r_O(:,1), X_r_O(:,2), X_r_O(:,3))
plot3(0, 0, 0, 'ok')
xlabel('Crosstrack, [km]'); ylabel('Downtrack,[km]'); zlabel('Orbit Normal, [km]');
title('Relative Orbital Motion, Chief Hill Frame')
legend('relative EOM','truth', 'chief')
axis equal
% model error
figure
error = y(:,13:15)-X_r_O;
plot(t,error)
title('Nonlinear EOM Error')
xlabel('time, [s]')
ylabel('model error, [km]')

% ------------------------------------------------
% ------------------------------------------------
%% Problem 14.1
params.n = 2;
params.mu =  3.986e5; % [km^3/s^2]
params.r_eq = 6371;   %[km]
% chief and deputy inertial IC [r,v]
X0_c = kep_cart('kep', [8000, 0, deg2rad(0), deg2rad(0), deg2rad(0), deg2rad(0)], 0, 'cart');
X0_d = kep_cart('kep', [8000, 0, deg2rad(.1), deg2rad(0.001), deg2rad(0.0002), deg2rad(.03)], 0, 'cart');
y0 = [X0_c, X0_d];
% integrate
tspan = [0 1*2*pi*sqrt(8000^3/params.mu)]; % [s]
opts = odeset('RelTol',1e-9,'AbsTol',1e-9);

[t,y] = ode45(@(t,y) odeFun_2body_J2(t, y, params), tspan, y0, opts);
% relative motion
X_r = y(:,7:9) - y(:,1:3);
r_max = max(vecnorm(X_r,2,2));
% plot: inertial motion
figure
plot3(y(:,1), y(:,2), y(:,3),'k'); hold on
plot3(y(:,7), y(:,8), y(:,9))
plot3(0, 0, 0, 'ob')
xlabel('[km]'); ylabel('[km]'); zlabel('[km]');
title('Inertial Orbital Motion')
legend('chief', 'deputy', 'Earth')
axis equal
% plot: relative motion
figure
plot3(0, 0, 0, 'ok'); hold on
plot3(X_r(:,1), X_r(:,2), X_r(:,3))
xlabel('[km]'); ylabel('[km]'); zlabel('[km]');
title('Relative Orbital Motion, chief Hill Frame')
legend('chief', 'deputy')
xlim([-r_max r_max])
ylim([-r_max r_max])
zlim([-r_max r_max])


