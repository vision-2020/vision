% ----------------------------
% Compare relative dynamics model performance
% ----------------------------
% VISION: CDR
% ----------------------------
% Author: Benjamin K. Hagenau
% Created: 10/24/19

%% House Keeping
clear all; close all; clc
addpath support ...
    C:\Users\Benha\OneDrive\Documents\MATLAB\functions
plotsettings

%% Set-Up
% environment
params.mu =  3.986e5; % [km^3/s^2]
opts = odeset('RelTol',1e-12,'AbsTol',1e-12);
% deployer [a e i RAAN w M]
deployer_COE   = [6371 + 2000, 0, deg2rad(10), deg2rad(10) deg2rad(10) deg2rad(10)];
deployer_X0_N  = kep_cart('kep', deployer_COE, 0, 'cart');
% rotation from inertial to deployer orbit
omega_ON = [0 0 norm(cross(deployer_X0_N(1:3), deployer_X0_N(4:6)))/norm(deployer_X0_N(1:3))^2];
ON = Inertial2Hill(deployer_X0_N(1:3), deployer_X0_N(4:6));
relative_X0_O = [0 0 0 1e-3 0 0]; % [km, km/s]
satellite_X0_N(1:3) = deployer_X0_N(1:3)' + ON'*relative_X0_O(1:3)';
satellite_X0_N(4:6) = deployer_X0_N(4:6)' + ON'*(relative_X0_O(4:6) + cross(omega_ON,relative_X0_O(1:3)))';

% initial conditions vector
y0 = [deployer_X0_N, satellite_X0_N, relative_X0_O, relative_X0_O];
% integration time
params.n  = sqrt(params.mu/deployer_COE(1)^3); % [rad/s]
% tspan     = [0 10*pi/params.n]; % [s]
tspan     = [0 100]; % [s]

%% Integrate
[t,y] = ode45(@(t,y) odefun_comparison(t, y, params), tspan, y0, opts);

%% Unpack Different Models
% inertial
deployer_N  = y(:,1:6);
satellite_N = y(:,7:12);
relative_N  = satellite_N(:,1:3) - deployer_N(:,1:3);
for i = 1:length(t)
    o_r = deployer_N(i,1:3)/norm(deployer_N(i,1:3));
    h = cross(deployer_N(i,1:3), deployer_N(i,4:6));
    o_h = h/norm(h);
    o_theta = cross(o_h, o_r);
    ON = [o_r; o_theta; o_h;];
    relative_O(i,1:3) = ON*relative_N(i,1:3)';
end
% general nonlinear
nonlinear_O = y(:,13:18);
nonlinear_error = vecnorm(nonlinear_O(:,1:3) - relative_O(:,1:3), 2,2);
% HCW
HCW_O = y(:,19:24);
HCW_error = vecnorm(HCW_O(:,1:3) - relative_O(:,1:3), 2,2);
% rectilinear
rectilinear_O = [zeros(length(t),1) nonlinear_O(:,2) zeros(length(t),1)];
for i = 1:length(t)
    r1 = nonlinear_O(i,1:3);
    r2 = [0 nonlinear_O(i,2) 0];
    theta = acos(dot(r1,r2)/(norm(r1)*norm(r2)));
    rectilinear_error(i) = norm(rectilinear_O(i,1:3)*sin(theta));
end

%% Plot
% trajectories
figure
plot3(0,0,0,'sk'); hold on
plot3(nonlinear_O(:,1)*1e3, nonlinear_O(:,2)*1e3, nonlinear_O(:,3)*1e3)
plot3(HCW_O(:,1), HCW_O(:,2)*1e3, HCW_O(:,3)*1e3)
plot3(rectilinear_O(:,1)*1e3, rectilinear_O(:,2)*1e3, rectilinear_O(:,3)*1e3)
plot3(relative_O(:,1)*1e3, relative_O(:,2)*1e3, relative_O(:,3)*1e3)
legend('Deployer', 'Nonlinear', 'HCW', 'Rectilinear', 'Inertial')
xlabel('Radial [m]')
ylabel('Alongtrack [m]')
zlabel('Orbit Normal [m]')
% error
figure
d = vecnorm(relative_O(:,1:3),2,2)*1e3; % [m]
plot(d, nonlinear_error*1e3); hold on
plot(d, HCW_error*1e3)
plot(d, rectilinear_error*1e3)
legend('Nonlinear', 'HCW', 'Rectilinear')
xlabel('Relative Distance [m]')
ylabel('Error [m]')









