% ----------------------------
% Compare relative dynamics model performance
% ----------------------------
% VISION: CDR
% ----------------------------
% Author: Benjamin K. Hagenau
% Created: 10/24/19

%% House Keeping
clear all; close all; clc
addpath support ...
    C:\Users\Benha\OneDrive\Documents\MATLAB\functions
plotsettings

%% Set-Up
% environment
params.mu =  3.986e5; % [km^3/s^2]
opts      = odeset('RelTol',1e-12,'AbsTol',1e-12);
tspan     = [0 100]; % [s]
dv = 1e-3;
% deployer [a e i RAAN w M]
for e = linspace(0, 0.2,10)
deployer_COE   = [6371 + 2000, e, deg2rad(10), deg2rad(10) deg2rad(10) deg2rad(10)];
deployer_X0_N  = kep_cart('kep', deployer_COE, 0, 'cart');
% rotation from inertial to deployer orbit
omega_ON = [0 0 norm(cross(deployer_X0_N(1:3), deployer_X0_N(4:6)))/norm(deployer_X0_N(1:3))^2];
ON = Inertial2Hill(deployer_X0_N(1:3), deployer_X0_N(4:6));
relative_X0_O = [0 0 0 0 dv 0]; % [km, km/s]
satellite_X0_N(1:3) = deployer_X0_N(1:3)' + ON'*relative_X0_O(1:3)';
satellite_X0_N(4:6) = deployer_X0_N(4:6)' + ON'*(relative_X0_O(4:6) + cross(omega_ON,relative_X0_O(1:3)))';

% initial conditions vector
y0 = [deployer_X0_N, satellite_X0_N, relative_X0_O, relative_X0_O];
% integration time
params.n  = sqrt(params.mu/deployer_COE(1)^3); % [rad/s]

%% Integrate
[t,y] = ode45(@(t,y) odefun_comparison(t, y, params), tspan, y0, opts);

%% Unpack Different Models
% inertial
deployer_N  = y(:,1:6);
satellite_N = y(:,7:12);
relative_N  = satellite_N(:,1:3) - deployer_N(:,1:3);
relative_O = zeros(length(t),3);
for i = 1:length(t)
    o_r = deployer_N(i,1:3)/norm(deployer_N(i,1:3));
    h = cross(deployer_N(i,1:3), deployer_N(i,4:6));
    o_h = h/norm(h);
    o_theta = cross(o_h, o_r);
    ON = [o_r; o_theta; o_h;];
    relative_O(i,1:3) = ON*relative_N(i,1:3)';
end
% general nonlinear
nonlinear_O = y(:,13:18);
nonlinear_error = vecnorm(nonlinear_O(:,1:3) - relative_O(:,1:3), 2,2);
% HCW
HCW_O = y(:,19:24);
HCW_error = vecnorm(HCW_O(:,1:3) - relative_O(:,1:3), 2,2);
% rectilinear
rectilinear_O = [zeros(length(t),1) relative_O(:,2) zeros(length(t),1)];
rectilinear_error = vecnorm([relative_O(:,1), relative_O(:,3)], 2,2);

%% Plot
% error
figure(1)
plot(deployer_COE(2), max(HCW_error)*1e3,'sk'); hold on
plot(deployer_COE(2), max(rectilinear_error)*1e3,'ok')
legend('HCW', 'Rectilinear')
title('$$e_{c}$$ Model Error')
xlabel('$$e_{c}$$')
ylabel('Error [m]')
end








