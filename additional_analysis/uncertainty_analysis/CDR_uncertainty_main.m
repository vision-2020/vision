% ------------------------
% analysis of TOF data processing results distribution
% ------------------------

clear all; close all; clc
addpath C:\Users\Benha\OneDrive\Documents\MATLAB\functions
addpath centroids

%% Parse
folders = {'Sim 085', 'Sim 140', 'Sim 195', 'Sim 250'};
% folders = {'Sim 250'};
    x_meas = []; y_meas = []; z_meas = [];
    x_truth = []; y_truth = []; z_truth = [];
    x_time = []; y_time = []; z_time = [];
for f = 1:length(folders)

    for sample = 1:9
        % load
        load(sprintf('centroids/%s/centroid_VCF_calcSample%d.mat', folders{f},sample))
        load(sprintf('centroids/%s/centroid_VCF_truthSample%d.mat', folders{f},sample))
        % match tixming
        centroid1 = centroid_calc.pos{1};
        time1 = centroid_calc.time{1};
        truth_time = centroid_truth.time;
        ind_i = find(truth_time == time1(1))-1;   %%% shift by -1 for magic
        ind_f = find(truth_time == time1(end))-1; %%% shift by -1 for magic
        truth1 = centroid_truth.pos(1).pos(ind_i:ind_f,:);
        % remove outliers
        [x_centroids,x_ind] = rmoutliers(centroid1(:,3));
        [y_centroids,y_ind] = rmoutliers(centroid1(:,2));
        [z_centroids,z_ind] = rmoutliers(centroid1(:,1));
        x_truth_temp = truth1(~x_ind,3);
        y_truth_temp = truth1(~y_ind,2);
        z_truth_temp = truth1(~z_ind,1);
        x_time_temp = time1(~x_ind,1);
        y_time_temp = time1(~y_ind,1);
        z_time_temp = time1(~z_ind,1);
        % store
        x_meas = [x_meas; x_centroids];
        y_meas = [y_meas; y_centroids];
        z_meas = [z_meas; z_centroids];
        x_truth = [x_truth; x_truth_temp];
        y_truth = [y_truth; y_truth_temp];
        z_truth = [z_truth; z_truth_temp];
        x_time = [x_time; x_time_temp];
        y_time = [y_time; y_time_temp];
        z_time = [z_time; z_time_temp];
        % plot data
        figure(1)
        subplot(3,1,1)
        plot(x_time,x_meas,'o'); hold on
        plot(x_time,x_truth)
        title('Truth and TOF Centeroids')
        ylabel('Z [m]')
        subplot(3,1,2)
        plot(y_time,y_meas,'o'); hold on
        plot(y_time,y_truth)
        ylabel('Y [m]')
        subplot(3,1,3)
        plot(z_time,z_meas,'o'); hold on
        plot(z_time,z_truth) 
        xlabel('time [s]')
        ylabel('X [m]')
    end
end
%% Analaysis (H=1: not gaussian, H=0: gaussian)
% stats.
x_variance = x_meas - x_truth; % mvnrnd(0,0.01,1000);
[x_variance,x_ind] = rmoutliers(x_variance);
x_std = std(x_variance);
x_mu = mean(x_variance);
x_zscore = (x_variance-x_mu)/x_std;
y_variance = y_meas - y_truth; %mvnrnd(0,0.01,500); 
[y_variance,y_ind] = rmoutliers(y_variance);
y_std = std(y_variance);
y_mu = mean(y_variance);
y_zscore = (y_variance-y_mu)/y_std;
z_variance = z_meas - z_truth; % mvnrnd(0,0.01,100);
[z_variance,z_ind] = rmoutliers(z_variance);
z_std = std(z_variance);
z_mu = mean(z_variance);
z_zscore = (z_variance-z_mu)/z_std;

% combined cross-plane
cross_variance = [x_variance; y_variance];
cross_std = std(cross_variance);
cross_mu = mean(cross_variance);

% ranges
num_std = 4;
x_range = linspace(x_mu-x_std*num_std, x_mu+x_std*num_std,100);
y_range = linspace(y_mu-y_std*num_std, y_mu+y_std*num_std,100);
z_range = linspace(z_mu-z_std*num_std, z_mu+z_std*num_std,100);
cross_range = linspace(cross_mu-cross_std*num_std, cross_mu+cross_std*num_std,100);
% pdf
pdf_x = pdf('Normal', x_range, x_mu, x_std);
pdf_y = pdf('Normal', y_range, y_mu, y_std);
pdf_z = pdf('Normal', z_range, z_mu, z_std);
pdf_cross = pdf('Normal', cross_range, cross_mu, cross_std);
% Anderson-Darling
[h_x, p_x, adstat_x, cv_x] = adtest(x_variance,'Distribution','norm');
[h_y, p_y, adstat_y, cv_y] = adtest(y_variance,'Distribution','norm');
[h_z, p_z, adstat_z, cv_z] = adtest(z_variance,'Distribution','norm');

[h_cross, p_cross, adstat_cross, cv_cross] = adtest(cross_variance);

fprintf('x: H = %d, p = %0.3f, cv = %.3f\n',h_x, p_x,cv_x)
fprintf('y: H = %d, p = %0.3f, cv = %.3f\n',h_y, p_y, cv_y)
fprintf('z: H = %d, p = %0.3f, cv = %.3f\n',h_z, p_z, cv_z)
fprintf('cross-plane: H = %d, p = %0.3f\n',h_cross, p_cross)

% probability density function
figure(f+10)
subplot(1,3,1)
histogram(x_variance, 'Normalization', 'pdf'); hold on
plot(x_range, pdf_x,'r')
title('Cross-Plane: X')
ylabel('Number of Samples')
subplot(1,3,2)
histogram(y_variance, 'Normalization', 'pdf'); hold on
plot(y_range, pdf_y,'r')
title('Cross-Plane: Y')
xlabel('Variance [m]')
subplot(1,3,3)
histogram(z_variance, 'Normalization', 'pdf'); hold on
plot(z_range, pdf_z,'r')
title('Along-Track: Z')
% % % along track and cross plane
% % figure(4)
% % subplot(1,2,1)
% % histogram(z_variance, 'Normalization', 'pdf'); hold on
% % plot(z_range, pdf_z,'r')
% % title('Along-track')
% % subplot(1,2,2)
% % histogram(cross_variance, 'Normalization', 'pdf'); hold on
% % plot(cross_range, pdf_cross,'r')
% % title('Cross-Plane')



