% ------------------------
% analysis of TOF data processing results distribution
% ------------------------

clear all; clc

load centroid_VCFSample1.mat

% load data
t = linspace(0, 100, length(centroids));
for i = 1:length(centroids)
    set = centroids{i};
    x(i) = set(1);
    y(i) = set(3);
    z(i) = set(2);
    f_truth(i) = deg2rad(25);
    f(i) = deg2rad(25)+normrnd(0, 0.05); % ~ 3 degrees
end

% fit data
[coef_x] = polyfit(t, x, 1); x_truth = polyval(coef_x,t);
[coef_y] = polyfit(t, y, 1); y_truth = polyval(coef_y,t);
[coef_z] = polyfit(t, z, 1); z_truth = polyval(coef_z,t);

% variance
e_x = x - x_truth; std_x = std(e_x);
e_y = y - y_truth; std_y = std(e_y);
e_z = z - z_truth; std_z = std(e_z);

% probability density function
x_range = linspace(-max(abs(e_x))-.1, max(abs(e_x))+.1,100);
y_range = linspace(-max(abs(e_y))-.1, max(abs(e_y))+.1,100);
z_range = linspace(-max(abs(e_z))-.1, max(abs(e_z))+.1,100);

pdf_x = pdf('Normal', x_range, 0, std_x);
pdf_y = pdf('Normal', y_range, 0, std_y);
pdf_z = pdf('Normal', z_range, 0, std_z);

% Anderson-Darling test to check how well a normal distribution fits data
% h = 0, fails to reject the hypothesis that they are normally distributed.
% p???
[h_x, p_x, adstat_x, cv_x] = adtest(e_x);
[h_y, p_y, adstat_y, cv_y] = adtest(e_y);
[h_z, p_z, adstat_z, cv_z] = adtest(e_z);
fprintf('x: H = %d, p = %0.3f\n',h_x, p_x)
fprintf('y: H = %d, p = %0.3f\n',h_y, p_y)
fprintf('z: H = %d, p = %0.3f\n',h_z, p_z)
%% Plot
% data
figure(1)
plot(t, x,'go'); hold on
plot(t, x_truth, 'g')
plot(t, y,'ro')
plot(t, y_truth, 'r')
plot(t, z,'bo')
plot(t, z_truth, 'b')
title('TOF data')
% variances
figure(2)
plot(t, e_x, 'go'); hold on
plot(t, e_y, 'ro')
plot(t, e_z, 'bo')
title('variances of TOF data')
% probability density function
figure(3)
subplot(1,3,1)
histogram(e_x, 'Normalization', 'pdf'); hold on
plot(x_range, pdf_x,'r')
title('X')
subplot(1,3,2)
histogram(e_y, 'Normalization', 'pdf'); hold on
plot(y_range, pdf_y,'r')
title('Y')
subplot(1,3,3)
histogram(e_z, 'Normalization', 'pdf'); hold on
plot(z_range, pdf_z,'r')
title('Z')

%% Uncertain DCM effects
% rotate state vector (rotated x,y,z = u,v,w)
for i = 1:length(t)
    C = angle2dcm(0, 0, f(i));
    C_truth = angle2dcm(0, 0, f_truth(i));
    vec = C*[x(i); y(i); z(i)];
    u(i) = vec(1); v(i) = vec(2); w(i) = vec(3);
    
    vec_truth = C_truth*[x_truth(i); y_truth(i); z_truth(i)];
    u_truth(i) = vec_truth(1); v_truth(i) = vec_truth(2); w_truth(i) = vec_truth(3);
end

% variance
e_u = u - u_truth; std_u = std(e_u);
e_v = v - v_truth; std_v = std(e_v);
e_w = w - w_truth; std_w = std(e_w);

% probability density function
u_range = linspace(-max(abs(e_u))-.1, max(abs(e_u))+.1,100);
v_range = linspace(-max(abs(e_v))-.1, max(abs(e_v))+.1,100);
w_range = linspace(-max(abs(e_w))-.1, max(abs(e_w))+.1,100);

pdf_u = pdf('Normal', u_range, 0, std_u);
pdf_v = pdf('Normal', v_range, 0, std_v);
pdf_w = pdf('Normal', w_range, 0, std_w);

% Anderson-Darling test to check how well a normal distribution fits data
[h_u, p_u, adstat_u, cv_u] = adtest(e_u);
[h_v, p_v, adstat_v, cv_v] = adtest(e_v);
[h_w, p_w, adstat_w, cv_w] = adtest(e_w);
fprintf('rotated x: H = %d, p = %0.3f\n',h_u, p_u)
fprintf('rotated y: H = %d, p = %0.3f\n',h_v, p_v)
fprintf('rotated z: H = %d, p = %0.3f\n',h_w ,p_w)
%% Plot
% data
figure(4)
plot(t, u,'go'); hold on
plot(t, u_truth, 'g')
plot(t, v,'ro')
plot(t, v_truth, 'r')
plot(t, w,'bo')
plot(t, w_truth, 'b')
title('rotated TOF data')
% variances
figure(5)
plot(t, e_u, 'go'); hold on
plot(t, e_v, 'ro')
plot(t, e_w, 'bo')
title('variances of rotated TOF data')
% probability density function
figure(6)
subplot(1,3,1)
histogram(e_u, 'Normalization', 'pdf'); hold on
plot(u_range, pdf_u,'r')
title('rotated X')
subplot(1,3,2)
histogram(e_v, 'Normalization', 'pdf'); hold on
plot(v_range, pdf_v,'r')
title('rotated Y')
subplot(1,3,3)
histogram(e_w, 'Normalization', 'pdf'); hold on
plot(w_range, pdf_w,'r')
title('rotated Z')


