clear all; clc

load uncertainty_ranges.mat


Z = uncertainty_ranges.bound(2:end,2:end);
[X,Y] = meshgrid(uncertainty_ranges.cross(2:end), uncertainty_ranges.along(2:end));

X_vec = X(:);
Y_vec = Y(:);
Z_vec = Z(:);

f = fit([X_vec, Y_vec], Z_vec, 'poly22');

figure
% surf(uncertainty_ranges.cross, uncertainty_ranges.along, uncertainty_ranges.bound); hold on
plot(f,[X_vec,Y_vec],Z_vec); hold on

[X_lim,Y_lim] = meshgrid(uncertainty_ranges.cross, uncertainty_ranges.along);
Z_lim = ones(length(X_lim), length(Y_lim))*82.843;
surf(X_lim,Y_lim,Z_lim)

xlabel('Crossplane $$\sigma_r$$ [m]')
ylabel('Alongtrack $$\sigma_r$$ [m]')
title('Uncertainty Ellispoid Largest Radius, [m]')





