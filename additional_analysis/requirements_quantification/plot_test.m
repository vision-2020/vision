close all

figure

fit_surf = plot(f); hold on
colormap([0.6 0.6 0.6])
fit_surf(1).LineStyle = 'none';
fit_surf(1).FaceAlpha = 1;
grid on

[X_lim,Y_lim] = meshgrid(grid_cross, grid_along);
Z_lim = ones(length(X_lim), length(Y_lim))*r_observ;
real_surf = surf(X_lim,Y_lim,Z_lim);
set(real_surf,'FaceColor',[0.2 1 0.2],'FaceAlpha',1)
grid on
