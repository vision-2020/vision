% ------------------------------
% Quantify inertial estimate accuracy requirement based on deployed cubesat
% trackability by ground station. 
% ------------------------------
% VISION: CDR
% ------------------------------
% Author: Benjamin K. Hagenau
% Created: 11/1/19
% ------------------------------
% ASSUMPTIONS:
%   (1) 2-body
%   (2) ground station FOV: 70 degrees (worst case Vallado)
%   (3) independent standard deviations
%   (4) assume sample rate of 1 second (no scaling of variance sum for
%       velocity)
%   (5) 10 min observation buffer

% CONTROLS: 
%   (1) orbit size (alt peri = 160, alt apo = 2000 (LEO)- WORST
%          a: slightly worse as semimajor axis increases:
%   (2) orbit deployment location (TA) - WORST - periapsis
%   (3) orbits before ground-based tracking (TOF:[1,10] orbits) - CHARACTERIZE
%   (4) relative uncertainty for each measurement element (3D) - SELECT

% House Keeping
clear all; close all; clc
addpath C:\Users\Benha\OneDrive\Documents\MATLAB\functions
plotsettings

%% Controls
% standard deviations [m, m/s]
std_along = linspace(0, 0.5, 8); % per axis
std_cross = linspace(0, 20, 8);
grid = zeros(length(std_cross), length(std_along));
bound_store = zeros(length(std_cross), length(std_along));

for along = 1:length(std_along)
    for cros = 1:length(std_cross)
std_r_along = std_along(along);
std_r_cross = std_cross(cros);

std_r_total = sqrt(std_r_along^2 + 2*std_r_cross^2);
std_v_along = sqrt(2)*std_r_along;
std_v_cross = sqrt(2)*std_r_cross;
std_v_total = sqrt(std_v_along^2 + 2*std_r_cross^2);
% propogation time
o = 3;               % [orbits]
% orbital elements
alt_peri = 200; % 160
alt_apo = 2000;
r_peri = alt_peri + 6371;
r_apo = alt_apo + 6371;
SMA  = (r_peri + r_apo)/2;  % [km]
ECC = 1-r_peri/SMA;
INC  = deg2rad(0);   % [rad] 
RAAN = deg2rad(0);   % [rad]  
AOP  = deg2rad(0);   % [rad] 
TA   = deg2rad(0);   % [rad]

%% Initialize
params.mu = 3.986e5; % [km^3/s^2]
% time for early pointing
n = sqrt(params.mu/SMA^3);
buffer = n*10*60;
% min FOV + buffer
FOV = deg2rad(70) + buffer;    % [rad]
r_observ = alt_peri*tan(FOV/2);
opts = odeset('RelTol',1e-9,'AbsTol',1e-9);
fprintf('Observable Radius: %0.3f [km]\n',r_observ)
fprintf('Field of View: %0.3f [deg]\n',rad2deg(FOV))
fprintf('-----------------\n')
P   = 2*pi*sqrt(SMA^3/params.mu);
%% Initial Conditions
% inertial
oe_truth = [SMA ECC INC RAAN AOP TA];
N0_truth = kep_cart('kep', oe_truth, 0, 'cart');

%% Analysis
% SMA: linspace(6771,8000,10)
% TA:  linspace(0,2*pi,10)
% o:   linspace(1,10,10)
% e: linspace(0, 0.25,10)
% for var = 1
    oe_truth = [SMA ECC INC RAAN AOP TA];
    N0_truth = kep_cart('kep', oe_truth, 0, 'cart');
    tspan = [0 86400]; %     tspan = [0 o*2*pi*sqrt(SMA^3/params.mu)];

for i = 1:500 % w
    N_truth = N0_truth;
    variance_r_along = normrnd(0, std_r_along);
    variance_r_cross = normrnd(0, std_r_cross);
    variance_v_along = variance_r_along + normrnd(0, std_r_along);
    variance_v_cross = variance_r_cross + normrnd(0, std_r_cross);
    
    N_meas  = N_truth + [variance_r_cross, variance_r_along, variance_r_cross,...
        variance_v_cross, variance_v_along, variance_v_cross]*1e-3;
    % initial error in truth hill frame
    r_N = N_meas(1:3) - N_truth(1:3);
    v_N = N_meas(4:6) - N_truth(4:6);
    h = cross(N_truth(1:3),N_truth(4:6));
    f_dot = norm(h)/norm(N_truth(1:3))^2;
    [ON] = Inertial2Hill(N_truth(1:3), N_truth(4:6));
    r_O = ON*r_N(1:3)';
    v_O = ON*(v_N)' - cross([0, 0, f_dot],r_O(1:3))';
    O_rel = [r_O' v_O'];
       
    y0 = [N_truth, N_meas, O_rel];
    % Integrate
    [t,y] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);
    N_truth = y(:,1:6);
    N_meas = y(:,7:12);
    O_rel = y(:,13:18);
    for j = 1:length(t)
        [ON] = Inertial2Hill(N_truth(j,1:3), N_truth(j,4:6));
        O_check(j,1:3) = ON*(N_meas(j,1:3) - N_truth(j,1:3))';
    end

    % Compute Error (in truth hill frame)
    [~,ind] = max(abs(O_rel(:,1)));
    radial_error(i) = O_rel(ind,1);
    [~,ind] = max(abs(O_rel(:,2)));
    alongtrack_error(i) = O_rel(ind,2);
    [~,ind] = max(abs(O_rel(:,3)));
    normal_error(i) = O_rel(ind,3);
end
% statistics
p = 0.95;
s = -2*log(1-p);
C = cov([radial_error', alongtrack_error', normal_error']);
m = [mean(radial_error), mean(alongtrack_error), mean(normal_error)];
[V,D] = eig(C); 
a = V*sqrt(D*s);
bound = max(vecnorm(a));

if bound < r_observ
    figure(1)
    plot(std_r_cross, std_r_along,'go'); hold on
else
    figure(1)
    plot(std_r_cross, std_r_along,'ro')
end
grid_cross(cros+1) = std_r_cross;
grid_along(along+1) = std_r_along;
bound_store(cros+1,along+1) = bound;

xlabel('planar $$\sigma$$ [m]')
ylabel('alongtrack $$\sigma$$ [m]')
    end
end
%% Plot
% % % figure(1) % inertial
% % % plot3(N_truth(:,1), N_truth(:,2), N_truth(:,3)); hold on
% % % plot3(N_meas(:,1), N_meas(:,2), N_meas(:,3))
% % % plot3(N_truth(1,1), N_truth(1,2), N_truth(1,3),'ok')
% % % plot3(0, 0, 0, 'ob')
% % % title('Inertial Motion')
% % % legend('truth', 'estimated', 'start')
% % % axis equal
% % % figure(2) % relative
% % % plot3(O_rel(:,1), O_rel(:,2), O_rel(:,3)); hold on
% % % plot3(O_check(:,1), O_check(:,2), O_check(:,3))
% % % plot3(O_rel(1,1), O_rel(1,2), O_rel(1,3))
% % % plot3(0, 0, 0, 'sb')
% % % title('Relative Motion (Truth Hill Frame)')
% % % legend('Relative EOM', 'Inertial EOM', 'start')
% % % axis equal
% % % figure(3) % Ecliptic plane uncertainty
% % % plot(radial_error, alongtrack_error,'ok'); hold on
% % % plot_gaussian_ellipsoid([m(1) m(2)], C(1:2,1:2), sqrt(s), 50);
% % % xlabel('Radial Error, [km]')
% % % ylabel('Along-track Error, [km]')
% % % title('Ecliptic Plane Error')
% % % figure(4) % Cross Plane uncertainty
% % % plot(radial_error, normal_error,'ok'); hold on
% % % plot_gaussian_ellipsoid([m(1) m(3)], [C(1,1) C(1,3); C(3,1) C(3,3)], sqrt(s), 50);
% % % xlabel('Radial Error, [km]')
% % % ylabel('Orbit Normal Error, [km]')
% % % title('Cross-Plane Error')
% % % figure(5) % 95% confidence ellipsoid
% % % plot3(radial_error, alongtrack_error, normal_error,'ok'); hold on
% % % plot3(a(1,1), a(2,1), a(3,1),'or')
% % % plot3(a(1,2), a(2,2), a(3,2),'or')
% % % plot3(a(1,3), a(2,3), a(3,3),'or')
% % % plot_gaussian_ellipsoid(m, C, sqrt(s), 20);
% % % xlabel('Radial Error, [km]');
% % % ylabel('Along-track Error, [km]')
% % % zlabel('Orbit Normal Error, [km]')
% % % title('Propogated Position Error')
% % % figure(6) % variable 
% % % plot(var, vecnorm(a(1,:)),'bo'); hold on
% % % plot(var, vecnorm(a(2,:)),'og')
% % % plot(var, vecnorm(a(3,:)),'or')
% % % xlabel('var')
% % % ylabel('ellipsoid axis [km]')
% % % title('Sensitivity')
% % 
% % %% Print
% % fprintf('radial confidence bound: %.3f [km]\n',norm(a(1,:)))
% % fprintf('alongtrack confidence bound: %.3f [km]\n',norm(a(2,:)))
% % fprintf('normal confidence bound: %.3f [km]\n',norm(a(3,:)))
% % fprintf('max error confidence bound: %.3f [km]\n',norm([a(1,:), a(2,:)]))
% end

Z = bound_store(2:end,2:end);
[X,Y] = meshgrid(grid_cross(2:end), grid_along(2:end));

X_vec = X(:);
Y_vec = Y(:);
Z_vec = Z(:);

f = fit([X_vec, Y_vec], Z_vec, 'poly22');

figure
% surf(grid_cross, grid_along, bound_store); hold on

fit_surf = plot(f); hold on
colormap([0.7 0.7 0.7])
fit_surf(1).LineStyle = 'none';
fit_surf(1).FaceAlpha = 1;

[X_lim,Y_lim] = meshgrid(grid_cross, grid_along);
Z_lim = ones(length(X_lim), length(Y_lim))*r_observ;
real_surf = surf(X_lim,Y_lim,Z_lim);
set(real_surf,'FaceColor','interp','FaceAlpha',0.5,'EdgeColor','none')

set(real_surf,'FaceColor',[0 0 1], ...
  'FaceAlpha',1,'FaceLighting','gouraud','EdgeColor','none')
set(fit_surf,'FaceColor','k')


