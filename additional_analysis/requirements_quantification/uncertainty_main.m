% -------------------------------
% Map inertial measurements to downtrack error
% -------------------------------
clear all; close all; clc
addpath C:\Users\Benha\OneDrive\Documents\MATLAB\functions
plotsettings
%% For ISS orbit...
% observable radius is 70 km -> fov = 20 degress (smallest in vallado)
% with 5 min buffer, observable radius is 178 km
%% environmental parameters
std_r = 1/sqrt(3); % m
std_v = 0.7/sqrt(3); % m/s
params.mu =  3.986e5; % [km^3/s^2] gravitational parameter of central body
params.r_eq = 6371; %[km] equitorial radius of Earth
params.n = 2;
dv = 0e-3; %[km/s] - deployment dV

%% Deployment conditions
vision.oe = [400+params.r_eq 0 deg2rad(10) deg2rad(0) deg2rad(10) deg2rad(10)];
vision.X0 = kep_cart('kep', vision.oe, 0, 'cart');
sc.X0_N = kep_cart('kep', vision.oe, 0, 'cart'); 
sc.X0_N(4:6) = sc.X0_N(4:6) + dv*sc.X0_N(4:6)/norm(sc.X0_N(4:6));

for i = 1:200
    %% Add uncertainty 
    Re_O = [normrnd(0,std_r) normrnd(0,std_r) normrnd(0,std_r)]'; % [crosstrack, along track, orbit normal]
    Ve_O = [normrnd(0,std_v) normrnd(0,std_v) normrnd(0,std_v)]'; % [crosstrack, along track, orbit normal]
    f_dot = [0, 0, norm(cross(sc.X0_N(1:3), sc.X0_N(4:6)))/norm(sc.X0_N(1:3))^2];
    ON = Inertial2Hill(sc.X0_N(1:3), sc.X0_N(4:6));
    Re_N(1,1:3) = ON'*Re_O;
    Ve_N(1,1:3) = ON'*(Ve_O + cross(f_dot, Re_O(1:3))');
    sc.X0_N_e = sc.X0_N + [Re_N, Ve_N]*1e-3; % error to km
    %% Get change in orbital from uncertainty
    [doe(i,1:6), ~] = dCOE2dX('dX', [Re_O; Ve_O], vision.oe, 0, 'dCOE');
    %% Relative Initial Conditions
    rel.X0_N = sc.X0_N_e - sc.X0_N;
    f_dot = norm(cross(sc.X0_N(1:3),sc.X0_N(4:6)))/norm(sc.X0_N(1:3))^2;
    [ON] = Inertial2Hill(sc.X0_N(1:3), sc.X0_N(4:6));
    rel.X0_O(1:3) = ON*rel.X0_N(1:3)';
    rel.X0_O(4:6) = ON*(sc.X0_N_e(4:6) - sc.X0_N(4:6))' - cross([0, 0, f_dot],rel.X0_O(1:3))';

    %% Integrate
    y0 = [sc.X0_N sc.X0_N_e rel.X0_O];
    tspan = [0 5*2*pi*sqrt(vision.oe(1)^3/params.mu)]; % [s]
    opts = odeset('RelTol',1e-9,'AbsTol',1e-9);

    [t,y] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);
    sc.R_N = y(:,1:3); sc.V_N = y(:,4:6);
    sc.Re_N = y(:,7:9); sc.Ve_N = y(:,10:12);
    rel.R_O = y(:,13:15);
    rel.V_O = y(:,16:18);
    
% %     % plot inertial
% %     figure(i+3)
% %     plot3(sc.R_N(:,1), sc.R_N(:,2), sc.R_N(:,3)); hold on
% %     plot3(sc.Re_N(:,1), sc.Re_N(:,2), sc.Re_N(:,3))
% %     axis equal
    
    for j = 1:length(t)
        [ON] = Inertial2Hill(sc.R_N(j,1:3), sc.V_N(j,1:3));
        sc.error_O(j,1:3) = ON*(sc.Re_N(j,1:3) - sc.R_N(j,1:3))';
    end
    %% Analysis
    % max error in hill frame
    [~,ind] = max(abs(rel.R_O(:,1)));
    crosstrack_error(i) = rel.R_O(ind,1);
    [~,ind] = max(abs(rel.R_O(:,2)));
    alongtrack_error(i) = rel.R_O(ind,2);
    [~,ind] = max(abs(rel.R_O(:,3)));
    normal_error(i)     = rel.R_O(ind,3);
    % plot error
% %     figure(i+3)
% %     plot(t/(3600), rel.R_O*1e3); hold on
% %     xlabel('time, [h]')
% %     ylabel('Error, [m]')
% %     title('Propogated Error, S/C Hill Frame')
% %     legend('Crosstrack', 'Along track', 'Orbit Normal')
end
%% Stats.
% scale factor (s, such that p is confidence interval)
p = 0.95;
s = -2*log(1-p);
% covariance
C = cov([crosstrack_error', alongtrack_error', normal_error']);
m = [mean(crosstrack_error), mean(alongtrack_error), mean(normal_error)];
%% Plot
p = 0.95;
s = -2*log(1-p);
figure(1)
plot(crosstrack_error, alongtrack_error,'ok'); hold on
xlabel('Crosstrack Error, [km]')
ylabel('Along-track Error, [km]')
title('Ecliptic Plane Error')
figure(2)
plot(alongtrack_error, normal_error,'ok'); hold on
xlabel('Along-track Error, [km]')
ylabel('Orbit Normal Error, [km]')
title('Orbit Normal Error')
figure(3)
plot3(crosstrack_error, alongtrack_error, normal_error,'ok'); hold on
[H, xp, yp, zp] = plot_gaussian_ellipsoid(m, C,sqrt(s));
xlabel('Crosstrack Error, [km]')
ylabel('Along-track Error, [km]')
zlabel('Orbit Normal Error, [km]')
title('Propogated Position Error')

% max error in the 95% ellipsoid
max_error = norm([max(max(H.XData)) max(max(H.YData))]);
doe = abs(doe);
doe(3:6) = rad2deg(doe(3:6));
% print info
fprintf('max error: %.6f [km]\n',max_error)
fprintf('---------------------\n')
fprintf('min da: %.6f [km]\n',min(doe(:,1)))
fprintf('mean da: %.6f [km]\n',mean(doe(:,1)))
fprintf('max da: %.6f [km]\n',max(doe(:,1)))
fprintf('---------------------\n')
fprintf('min de: %.6f \n',min(doe(:,2)))
fprintf('mean de: %.6f \n',mean(doe(:,2)))
fprintf('max de: %.6f \n',max(doe(:,2)))
fprintf('---------------------\n')
fprintf('min di: %.6f [deg]\n',min(doe(:,3)))
fprintf('mean di: %.6f [deg]\n',mean(doe(:,3)))
fprintf('max di: %.6f [deg]\n',max(doe(:,3)))
fprintf('---------------------\n')
fprintf('min dRAAN: %.6f [deg]\n',min(doe(:,4)))
fprintf('mean dRAAN: %.6f [deg]\n',mean(doe(:,4)))
fprintf('max dRAAN: %.6f [deg]\n',max(doe(:,4)))
fprintf('---------------------\n')
fprintf('min dw: %.6f [deg]\n',min(doe(:,5)))
fprintf('mean dw: %.6f [deg]\n',mean(doe(:,5)))
fprintf('max dw: %.6f [deg]\n',max(doe(:,5)))
fprintf('---------------------\n')
fprintf('min dM: %.6f [deg]\n',min(doe(:,6)))
fprintf('mean dM: %.6f [deg]\n',mean(doe(:,6)))
fprintf('max dM: %.6f [deg]\n',max(doe(:,6)))


% propogating 24 hours
% define STD so that 95% conidence interval is within bounds



