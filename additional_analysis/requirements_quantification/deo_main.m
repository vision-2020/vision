% -------------------------
% Quantify expected difference in cubesat COE.
% Estimate inertial seperation after orbits.
% -------------------------
% VISION: PDR
% -------------------------
% Author: Benjamin K. Hagenau
% Created: 10/2/19

% House Keeping
clear all; close all; clc
addpath C:\Users\Benha\OneDrive\Documents\MATLAB\functions

%% environmental parameters
params.mu =  3.986e5; % [km^3/s^2] gravitational parameter of central body
params.r_eq = 6371; %[km] equitorial radius of Earth
params.n = 2;
dt = 2; % [s] - time between deployments
dv = 2e-3; %[km/s] - deployment dV
o = 10; % number of chief orbits to propogate over
for deg = 1
%% VISION Initial Conditions
vision.oe = [8000 0 deg2rad(45) deg2rad(0) deg2rad(1) deg2rad(deg)];
vision.X0 = kep_cart('kep', vision.oe, 0, 'cart');

%% Chief Initial Conditions
chief.X0 = kep_cart('kep', vision.oe, 0, 'cart'); 
chief.X0(4:6) = chief.X0(4:6) + [-dv/2 -dv/2 0];
% propogate chief forward dt
chief.oe = kep_cart('cart', chief.X0, -dt, 'kep');
chief.X0 = kep_cart('kep', chief.oe, 0, 'cart'); 
%% Deputy Initial Conditions
% position off set
deputy.X0 = kep_cart('kep', vision.oe, dt, 'cart'); 
deputy.X0(4:6) = deputy.X0(4:6) + [-dv/2 -dv/2 0];%dv*deputy.X0(4:6)/norm(deputy.X0(4:6));
deputy.oe = kep_cart('cart', deputy.X0, 0, 'kep');
T = o*2*pi*sqrt(chief.oe(1)^3/params.mu);
tspan = [0 T]; % [s]
opts = odeset('RelTol',1e-9,'AbsTol',1e-9);

%% CHECK
fprintf('vision: %.8f, %.8f, %.8f, %.8f, %.8f, %.8f\n', vision.oe(1:2), rad2deg(vision.oe(3:6)))
fprintf('chief:  %.8f, %.8f, %.8f, %.8f, %.8f, %.8f\n', chief.oe(1:2), rad2deg(chief.oe(3:6)))
fprintf('deputy: %.8f, %.8f, %.8f, %.8f, %.8f, %.8f\n', deputy.oe(1:2), rad2deg(deputy.oe(3:6)))
% general relative equations check
[ON] = Inertial2Hill(chief.X0(1:3), chief.X0(4:6));
f_dot = norm(cross(chief.X0(1:3),chief.X0(4:6)))/norm(chief.X0(1:3))^2;
rel.X0_O(1:3) = ON*(deputy.X0(1:3) - chief.X0(1:3))';
rel.X0_O(4:6) = ON*(deputy.X0(4:6) - chief.X0(4:6))' - cross([0, 0, f_dot],rel.X0_O(1:3))';
y0 = [chief.X0 deputy.X0 rel.X0_O];
% [t_check,y_check] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);

%% Integrate Chief and Deputy
%y0 = [chief.X0 deputy.X0];
[t,y] = ode45(@(t,y) odefun_nonlinear(t, y, params), tspan, y0, opts);

%[t,y] = ode45(@(t,y) odeFun_2body_J2(t, y, params), tspan, y0, opts);
%% Unpack
chief.R = y(:,1:3); chief.V = y(:,4:6);
deputy.R = y(:,7:9); deputy.V = y(:,10:12);
rel.R_O = y(:,13:15); rel.V_O = y(:,16:18);
% error in inertial
rel_N = deputy.R(:,1:3) - chief.R(:,1:3);
% error in chief hill
for i = 1:length(t)
    [ON] = Inertial2Hill(chief.R(i,1:3), chief.V(i,1:3));
    rel_O(i,1:3) = ON*(deputy.R(i,1:3) - chief.R(i,1:3))';
end
%% Plot
% % % inertial orbits
figure
plot3(chief.R(:,1), chief.R(:,2), chief.R(:,3)); hold on
plot3(deputy.R(:,1), deputy.R(:,2), deputy.R(:,3))
plot3(chief.R(1,1), chief.R(1,2), chief.R(1,3),'ok'); hold on
plot3(deputy.R(1,1), deputy.R(1,2), deputy.R(1,3),'or')
plot3(0,0,0,'or')
xlabel('[km]')
ylabel('[km]')
zlabel('[km]')
title('Inertial Heliocentric Motion')
%position error
figure
plot(t/(T/o), vecnorm(rel.R_O,2,2)*1e3); hold on
xlabel('Time, [s]')
ylabel('Position Error, [m]')
title(sprintf('Absolute Position Error, %d',deg))

ind = find(vecnorm(rel.R_O(1:end,:),2,2)==min(vecnorm(rel.R_O(1:end,:),2,2)));
figure(1)
plot(deg, t(ind)/(T/o),'ok'); hold on
xlabel('Time, [s]')
ylabel('Position Error, [m]')
title(sprintf('Absolute Position Error, %d',deg))
%relative motion in chief hill
figure
plot3(y(:,13)*1e3, y(:,14)*1e3, y(:,15)*1e3); hold on
plot3(y(1,13)*1e3, y(1,14)*1e3, y(1,15)*1e3,'o')
plot3(rel_O(:,1)*1e3, rel_O(:,2)*1e3, rel_O(:,3)*1e3,'--')
plot3(0,0,0,'ko')
title('Relative Motion in Chief Orbit Frame')
xlabel('Cross track, [m]')
ylabel('Along track, [m]')
zlabel('Orbit Normal, [m]')
legend('Deputy', 'Initial Deputy', 'check', 'Chief')
%% Print
ind = find(t > T/o); ind = ind(1);
fprintf('Propogation time: %d orbits -> %.3f [h]\n', o, T/3600)
fprintf('Minimum position error: %.8f [m]\n', min(vecnorm(rel.R_O(ind:end,:),2,2))*1e3)
fprintf('Maximum position error: %.8f [m]\n', max(vecnorm(rel.R_O(ind:end,:),2,2))*1e3)
end
%% Assumptions
% deployed down track
% vision's velocity is constant over dt
% calculation error (mainly from fsolve) on the order of 1e-6 degrees
% difference in w and f for the two s/c are the equal and opposite because deploy changes that
%   to be the new periapsis or the s/c. 

% how fast are cubesats deployed and on what interval?

%% Variables of interest
% cubesat deploymenent speed
% cubesat deployment interval
% Vision eccentricity -> orbit deployment location
% Vision altitude 
% deplpoyment direction

%% Validation
% integrate CW with orbit and see what happens...
% error from coordinate set change is on the order of e-6 km
% integration error on the order of e-11 km










