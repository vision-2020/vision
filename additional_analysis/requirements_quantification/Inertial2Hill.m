% build DCM of from chief hill frame to inertial
% INPUTS: 
%   Rc_N: inertial chief position
%   Vc_N: inertial chief velocity

function [ON] = Inertial2Hill(Rc_N, Vc_N)
o_r_N = Rc_N/norm(Rc_N);
o_h_N = cross(Rc_N, Vc_N)/norm(cross(Rc_N, Vc_N));
o_theta_N = cross(o_h_N,o_r_N);
ON = [o_r_N; o_theta_N; o_h_N];
end

