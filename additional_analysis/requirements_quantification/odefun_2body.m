% ------------------------
% ODE45 function for the 2-body problem equation with J2 perturbations
% ------------------------
% ASEN 6014: HW 1 (problem 8)
% ------------------------
% Author: Ben Hagenau
% Created: 9/1/19
% ------------------------
% INPUTS:
%   params: 
%       u: gravitiational parameters for planets/Sun
%       a: perturbation force

function [dStatedt] = odefun_2body(t, y, params)
n = params.n;    % number of satellites
mu = params.mu;  % graviational parameter of chief
J2 = 1082.63e-6; % Earth's J2 parameter
r_eq = 6371;     % Earth's equitoria radius [km]
dStatedt = [];

for i = 1:n
    state = y(1+(i-1)*6:6+(i-1)*6);
    R = state(1:3);
    V = state(4:6);
    %% Perturbation calculation
    aJ2 = -(3/2)*J2*(mu/norm(R)^2)*(r_eq/norm(R))^2*...
    [(1-5*(R(3)/norm(R))^2)*(R(1)/norm(R));...
     (1-5*(R(3)/norm(R))^2)*(R(2)/norm(R));...
     (3-5*(R(3)/norm(R))^2)*(R(3)/norm(R))];
    %% ODEs
    dRdt = V;
    dVdt = -mu/norm(R)^3*R + aJ2;
    %% Store
    dStatedt = [dStatedt; dRdt; dVdt];
end

end

