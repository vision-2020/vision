% ------------------------
% ODE45 function for nonlinear relative orbit EOM
% ------------------------
% ASEN 6014: HW 2 (problem 14.2)
% ------------------------
% Author: Ben Hagenau
% Created: 9/1/19
% ------------------------
% INPUTS:
%   params: 
%       u: gravitiational parameters for planets/Sun
%       a: perturbation force

function [dStatedt] = odefun_nonlinear(t, Y, params)
mu = params.mu; 
R_c = Y(1:3); r_c = norm(R_c);
V_c = Y(4:6); v_c = norm(V_c);
R_d = Y(7:9); r_d = norm(R_d);
V_d = Y(10:12);
R_r = Y(13:15); x = R_r(1); y = R_r(2); z = R_r(3); 
V_r = Y(16:18); u = V_r(1); v = V_r(2); 

dfdt = norm(cross(R_c, V_c))/r_c^2;

%% Chief Inertial EOM
dRdt_c = V_c;
dVdt_c = -mu/r_c^3*R_c;
%% Deputy Inertial EOM
dRdt_d = V_d;
dVdt_d = -mu/r_d^3*R_d;
%% Nonlinear Relative Orbit EOM
v_c = dot(V_c, R_c/r_c); % (radial velocity of chief)
dRdt_r = V_r;
dVdt_r = [-(mu/r_d^3)*(r_c+x) + (mu/r_c^2) + x*dfdt^2 + 2*dfdt*(v-y*(v_c/r_c));...
         -(mu/r_d^3)*y + y*dfdt^2 - 2*dfdt*(u-x*(v_c/r_c));...
         -(mu/r_d^3)*z];


dStatedt = [dRdt_c; dVdt_c; dRdt_d; dVdt_d; dRdt_r; dVdt_r];


end

