function plotErrorEllipse(mu, Sigma, p)

    s = -2 * log(1 - p);

    [V, D] = eig(Sigma * s);

    t = linspace(0, 2 * pi);
    a = (V * sqrt(D)) * [cos(t(:))'; sin(t(:))'; cos(t(:))'];

    surf(a(1, :) + mu(1), a(2, :) + mu(2), a(3, :) + mu(3));