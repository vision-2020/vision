# ----------------
# State Estimation Main Script
# ----------------
# VISION: state estimation
# ----------------
# Author: Ben Hagenau
# Edited: 1/5/20

import numpy as np
import yaml
import os
from pathlib import Path
from StateEstimation.Deployer import Deployer
from StateEstimation.Satellite import Satellite
from StateEstimation.VISION_SE_Test import *
from StateEstimation.VISION_Simulation import Simulation
from StateEstimation.Utilities import *

# TODO: Get this to work for multiple cubesats
# 5) Create script StateEstimationRunner, takes in filenames and outputs TLEs
# 6) Create script AutoProcess, runs every possible script that can be automated
# TODO: Get TLEs
# TODO: Better function descriptors (see VANTAGE code for ideas)

# TEST ---------------------------------------------------------------
test_manager = unittest.TextTestRunner()
test_manager.run(test_suite())

# SIM ----------------------------------------------------------------

# get settings dict based on file name in settings pointer file
settings = get_settings()

# get translation vector ([cm], C4D coordinate frame)
transl_vec = get_transl_vec(settings)

# get origin position of launch tube ([cm], C4D coordinate frame)
sat_origin_pos = get_tube_origin(settings, transl_vec)

# get initial conditions of all cubesats being simulated ([km], sensor frame relative to deployer)
sat_ICs = setup_cubesats(settings, sat_origin_pos)

# construct deployer object
deployer = setup_deployer(settings)

# TODO: Get all of these values from .yaml
# deployment parameters
cubeSatSizes = settings['CUBESATS_SIZES']  # strings for satellite sizes
n = len(cubeSatSizes)                      # [satellites]
dt = 0      # [s]
t_sim = 50  # [s]
fps = settings['TOF_FPS']                  # [frames per second]

# set directory for file saving, create folder if it doesn't already exist
data_directory = os.getcwd()+'/Data/current/UnprocessedCentroids/'
Path(data_directory).mkdir(parents=True, exist_ok=True)

# generate measurement and truth files
sim = Simulation(sat_ICs, deployer, n, dt, t_sim, fps)
sim.generate_truth()
sim.generate_files(data_directory, 0)

# # process measurement and truth files
# sim.run_vision(data_directory+'/Data/current/ProcessedCentroids/Satellite0_01.csv', '/Data/current/UnprocessedCentroids/measurements.csv')
# sim.generate_results()
# sim.plot_results()
