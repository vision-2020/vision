# ------------------------------------------------------------------------------
# Operational implementation of VISION State Estimation Algorithms
# ------------------------------------------------------------------------------
# ASEN 4028: VISION
# ------------------------------------------------------------------------------
# Author: Ben Hagenau
# Created: 4/8/20
import numpy as np
from random import gauss
import statistics as stats
from itertools import product
import matplotlib.pyplot as plt
from matplotlib import rcParams
from itertools import combinations
from scipy.integrate import odeint
from mpl_toolkits import mplot3d
import sys
from StateEstimation.Deployer import Deployer
from StateEstimation.Satellite import Satellite
from StateEstimation.Format import read_csv, format_satellite_data, format_deployer
from StateEstimation.nonlinear_batch_filter.nonlinear_batch_functions import dfdz, dhdz, h, ode_relative_orbit
from StateEstimation.nonlinear_batch_filter.NLB_VISION import NLB

scenario = "Implementation/eccentric_data_250"
# --- import data --------------------------------------------------------------
deployer_data = np.genfromtxt(scenario+"/deployer_data.csv", delimiter=',')
mission_data = np.genfromtxt(scenario+"/mission_data.csv", delimiter=',')
true_sensor_frame = np.genfromtxt(scenario+"/true_sensor_frame.csv", delimiter=',')
true_orbit_frame = np.genfromtxt(scenario+"/true_orbit_frame.csv", delimiter=',')
true_inertial_frame = np.genfromtxt(scenario+"/true_inertial_frame.csv", delimiter=',')
measurements = np.genfromtxt(scenario+"/measurements5.csv", delimiter=',')
measurements[:,[1,2,3]] = measurements[:,[1,2,3]]/1000
# truncate truth data... start doing this in sim
rows = np.isin(true_sensor_frame[:,-1], measurements[:,-1])
true_sensor_frame = true_sensor_frame[rows,:]
true_orbit_frame = true_orbit_frame[rows,:]
true_inertial_frame = true_inertial_frame[rows,:]
rows = np.isin(measurements[:,-1], true_sensor_frame[:,-1])
measurements = measurements[rows,:]

# --- satellites ---------------------------------------------------------------
satellites = format_satellite_data(measurements, frame='S')
satellite = satellites[0]
# --- deployer -----------------------------------------------------------------
deployer = format_deployer(measurements, deployer_data, mission_data)
deployer.get_coe()
# --- satellite truth ----------------------------------------------------------
trueSatellites = format_satellite_data(true_inertial_frame, frame='N')
trueSatellite = trueSatellites[0]
trueSatellite.get_coe()
# --- unpack data --------------------------------------------------------------
t = measurements[:,-1]
# --- compute measurements -----------------------------------------------------
r_sig_meas = 0.3e-4 # [km]
r_sig_est = 1e-4 # [km]
v_sig_est = 7e-6 # [km/s]
# --- fitler inputs --------------------------------------------------------
R = np.identity(3)*(1e-3)**2
P0 = np.array([[r_sig_est**2, 0, 0, 0, 0, 0], [0, r_sig_est**2, 0, 0, 0, 0], [0, 0, r_sig_est**2, 0, 0, 0],\
             [0, 0, 0, v_sig_est**2, 0, 0], [0, 0, 0, 0, v_sig_est**2, 0], [0, 0, 0, 0, 0, v_sig_est**2]])
P0 = []
dx0 = np.array([gauss(0,1e-4), gauss(0,1e-4), gauss(0,1e-4),\
                gauss(0,1e-6), gauss(0,1e-6), gauss(0,1e-6)])
x0 = np.hstack((true_orbit_frame[0,[1,2,3]], true_orbit_frame[0,[4,5,6]]))# + dx0
# --- run VISIONS ----------------------------------------------------------
orbitFrameMeasurements = satellite.sensor_to_orbit(deployer).r; orbitFrameMeasurements = np.asarray(orbitFrameMeasurements)
nlb = satellite.estimate_state(deployer, x0, R, P0)
# nlb = satellite.kalman(deployer)
inertialFrameEstimates = satellite.orbit_to_inertial(deployer)
inertialFrameREstimates = inertialFrameEstimates.r
inertialFrameRdEstimates = inertialFrameEstimates.v
deployer.get_coe()
satellite.get_coe()
satellite.estimate_coe(deployer)
satellite.get_tle()
coeEstimateAbsoluteError = satellite.coe-trueSatellite.coe[0]
# --- TLE orbit analysis -------------------------------------------------------
# propogate orbits
def from_vec(r_vec, v_vec):
    r = np.linalg.norm(r_vec)
    # unit orbit radial in inertial coordinates
    r_hat = r_vec/r
    # unit orbit normal in inertial coordinates
    h_vec = np.cross(r_vec,v_vec)
    h = np.linalg.norm(h_vec)
    h_hat = h_vec/h
    # unit orbit tangent in inertial coordinates
    theta_hat = np.cross(h_hat, r_hat)
    # construct DCM
    ON = np.array([r_hat, theta_hat, h_hat])

    return ON
def dfdz(X, mu, J2, r_e): # X: [x,y,z,xd,yd,zd] (satellite ECI state)
    x = X[0]
    y = X[1]
    z = X[2]
    xd = X[3]
    yd = X[4]
    zd = X[5]
    A = np.array(([[0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1], [mu*(12*J2*r_e**2*x**4 + 9*J2*r_e**2*x**2*y**2 - 81*J2*r_e**2*x**2*z**2 - 3*J2*r_e**2*y**4 + 9*J2*r_e**2*y**2*z**2 + 12*J2*r_e**2*z**4 + 4*x**6 + 6*x**4*y**2 + 6*x**4*z**2 - 2*y**6 - 6*y**4*z**2 - 6*y**2*z**4 - 2*z**6)/(2*np.sqrt(x**2 + y**2 + z**2)*(x**8 + 4*x**6*y**2 + 4*x**6*z**2 + 6*x**4*y**4 + 12*x**4*y**2*z**2 + 6*x**4*z**4 + 4*x**2*y**6 + 12*x**2*y**4*z**2 + 12*x**2*y**2*z**4 + 4*x**2*z**6 + y**8 + 4*y**6*z**2 + 6*y**4*z**4 + 4*y**2*z**6 + z**8)), 3*mu*x*y*(-12*J2*r_e**2*z**2*(x**2 + y**2)**2 - 8*J2*r_e**2*z**2*(z**4 - 2*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) - 5*J2*r_e**2*(x**2 + y**2)**2*(-x**2 - y**2 + 2*z**2) + 2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**(9/2)), 3*mu*x*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0], [3*mu*x*y*(-12*J2*r_e**2*z**2*(x**2 + y**2)**2 - 8*J2*r_e**2*z**2*(z**4 - 2*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) - 5*J2*r_e**2*(x**2 + y**2)**2*(-x**2 - y**2 + 2*z**2) + 2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)**2*(x**2 + y**2 + z**2)**(9/2)), mu*(-3*J2*r_e**2*x**4 + 9*J2*r_e**2*x**2*y**2 + 9*J2*r_e**2*x**2*z**2 + 12*J2*r_e**2*y**4 - 81*J2*r_e**2*y**2*z**2 + 12*J2*r_e**2*z**4 - 2*x**6 - 6*x**4*z**2 + 6*x**2*y**4 - 6*x**2*z**4 + 4*y**6 + 6*y**4*z**2 - 2*z**6)/(2*np.sqrt(x**2 + y**2 + z**2)*(x**8 + 4*x**6*y**2 + 4*x**6*z**2 + 6*x**4*y**4 + 12*x**4*y**2*z**2 + 6*x**4*z**4 + 4*x**2*y**6 + 12*x**2*y**4*z**2 + 12*x**2*y**2*z**4 + 4*x**2*z**6 + y**8 + 4*y**6*z**2 + 6*y**4*z**4 + 4*y**2*z**6 + z**8)), 3*mu*y*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0], [3*mu*x*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), 3*mu*y*z*(-6*J2*r_e**2*z**2*(x**2 + y**2) + 6*J2*r_e**2*(x**2 + y**2)**2 - 5*J2*r_e**2*(x**2 + y**2)*(-x**2 - y**2 + 2*z**2) + 4*J2*r_e**2*(2*z**4 - 3*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 2*(x**2 + y**2)*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2)*(x**2 + y**2 + z**2)**(9/2)), mu*(36*J2*r_e**2*z**2*(x**2 + y**2) - 15*J2*r_e**2*z**2*(-x**2 - y**2 + 2*z**2) + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2)*(x**2 + y**2 + z**2) - 6*J2*r_e**2*(4*z**4 - 5*z**2*(x**2 + y**2 + z**2) + (x**2 + y**2 + z**2)**2) + 6*z**2*(x**2 + y**2 + z**2)**2 - 2*(x**2 + y**2 + z**2)**3)/(2*(x**2 + y**2 + z**2)**(9/2)), 0, 0, 0]]))
    return A
def odefun(z_vec, t, mu, J2, r_e):
    x = z_vec[0]
    y = z_vec[1]
    z = z_vec[2]
    xd = z_vec[3]
    yd = z_vec[4]
    zd = z_vec[5]
    Phi = np.reshape(z_vec[6:],(6,6))
    # nonlinear dynamics
    f = np.array([xd, yd, zd,\
         mu*x*(6*J2*r_e**2*z**2 + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2)),\
         mu*y*(6*J2*r_e**2*z**2 + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2)),\
         mu*z*(-6*J2*r_e**2*(x**2 + y**2) + 3*J2*r_e**2*(-x**2 - y**2 + 2*z**2) - 2*(x**2 + y**2 + z**2)**2)/(2*(x**2 + y**2 + z**2)**(7/2))])
    # propogate state transition matrix
    A = dfdz(z_vec[0:6], mu, J2, r_e)
    Phid = A@Phi
    zd = np.hstack((f, np.reshape(Phid,(-1))))
    return zd
params = {"mu" : 3.986004415e5, "r_e" : 6378, "J2" : 0.0010826269}
X0_estimate = np.hstack((satellite.cartesian.r[0], satellite.cartesian.v[0]))
X0_true = np.hstack((trueSatellite.cartesian.r[0], trueSatellite.cartesian.v[0]))
T = 2*np.pi*np.sqrt(trueSatellite.coe[0][0]**3/params["mu"])
# n = 10
days = 1
t_vec = np.linspace(0, days*86400, 1000)
day_vec = t_vec/86400
z0_estimate = np.hstack((X0_estimate, np.reshape(np.identity(6),(-1))))
z0_true = np.hstack((X0_true, np.reshape(np.identity(6),(-1))))
X_estimate = odeint(lambda X, t: odefun(X,t, **params), z0_estimate, t_vec, rtol=1e-10, atol=1e-10)
X_true = odeint(lambda X, t: odefun(X,t, **params), z0_true, t_vec, rtol=1e-10, atol=1e-10)
# rotate covariance estimate into truth orbit frame
DN = from_vec(np.asarray(deployer.cartesian.r[0]), np.asarray(deployer.cartesian.v[0]))
ON = from_vec(X_true[0,:3], X_true[0,[3,4,5]])
OD = ON @ DN.transpose()
P0 = nlb.P[0][:3,:3] # position P0 in deployer orbit frame
P0 = OD @ P0 @ OD.transpose() # position P0 in truth orbit frame
# propogation error in truth orbit frame
rho = []
P = []
std = []
e =[]
e_std = []
for i in range(0, len(t_vec)):
    Phi = np.reshape(X_true[i,6:], (6,6))
    # truth frame stm
    Pi = Phi[:3,:3] @ P0 @ Phi[:3,:3].transpose()
    std.append(np.sqrt(np.diag(Pi).tolist())*1000)
    # truth orbit frame error
    dR = X_estimate[i,:3] - X_true[i,:3]
    ON = from_vec(X_true[i,:3], X_true[i,[3,4,5]])
    rho.append(ON @ dR)
    e.append(np.linalg.norm(rho[-1]))
    e_std.append(np.linalg.norm(std[-1]))
std = np.asarray(std)
rho = np.asarray(rho)*1000
e = np.asarray(e)*1000
e_std = np.asarray(e_std)
# --- print results ------------------------------------------------------------
print('-------------- COE Absolute Error --------------')
print("SMA: {} [m]".format(np.asarray(coeEstimateAbsoluteError[0])*1000))
print("ECC: {}".format(coeEstimateAbsoluteError[1]))
print("INC: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[2])))
print("RAAN: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[3])))
print("AOP: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[4])))
print("TA: {} [deg]".format(np.rad2deg(coeEstimateAbsoluteError[5])))
# --- plot results -------------------------------------------------------------
font = {'size':'20'}
# --- filter
# nlb.PlotRVEstimateError(true_orbit_frame[:,[1,2,3]], true_orbit_frame[:,[4,5,6]]) # get true times at measurement times
# print((-true_orbit_frame[:,[4,5,6]] + nlb.estimates[:,[3,4,5]])[0]*1000)
# nlb.PlotPositionResiduals(orbitFrameMeasurements, r_sig_meas)
# # --- propogation error
# # net
# rcParams['axes.labelpad'] = 0
# title = 'Propogation Error'
# fig = plt.figure()
# plt.plot(day_vec*24, e, 'k', label='net error')
# plt.plot(day_vec*24, 3*e_std+e,'--k', label='+3$\sigma$')
# plt.plot(day_vec*24, -3*e_std+e,'--k')
# plt.xlabel('Hours Since Deployment', **font)
# plt.ylabel('Propogation Error [m]', **font)
# plt.legend()
# # by axis
# rcParams['axes.labelpad'] = 0
# title = 'Truth Orbit Frame Inertial Estimate Propogation Error'
# axes = ['Radial', 'Along-track', 'Orbit Normal']
# fig, ax = plt.subplots(1,3)
# for it in range(0, 3):
#     # spacecraft trajectory
#     ax[it].plot(day_vec, rho[:,it], label='error')
#     ax[it].plot(day_vec, 3*std[:,it]+rho[:,it],'--k', label='+3$\sigma$')
#     ax[it].plot(day_vec, -3*std[:,it]+rho[:,it],'--k')
#     ax[it].set_ylabel(axes[it] + ' [m]', **font)
#     ax[it].set_xlabel('Time [days]', **font)
# # fig.suptitle(title, **font)
# ax[it].legend()
# # # 3D
# # title = 'Truth Orbit Frame: ' + str(days) + ' Days'
# # rcParams['axes.labelpad'] = 20
# # fig = plt.figure()
# # ax = fig.add_subplot(111, projection='3d')
# # ax.plot3D(rho[:,0], rho[:,1], rho[:,2], label='propogation error')
# # ax.plot3D([0],[0],[0], '.k', label='Truth')
# # ax.set_title(title, **font)
# # ax.set_xlabel(axes[0] + ' [m]', **font)
# # ax.set_ylabel(axes[1] + ' [m]', **font)
# # ax.set_zlabel(axes[2] + ' [m]', **font)
# # ax.legend()
# # iso
# # rcParams['axes.labelpad'] = 0
# # fig, ax = plt.subplots(1,3)
# # for it, set in enumerate(combinations([1,2,3],2)):
# #     # spacecraft trajectory
# #     ax[it].plot([0],[0],[0], '.k')
# #     ax[it].plot(rho[:,set[0]-1], rho[:,set[1]-1],'b')
# #     ax[it].set_xlabel(axes[set[0]-1] + ' [m]', **font)
# #     ax[it].set_ylabel(axes[set[1]-1] + ' [m]', **font)
# #     fig.suptitle(title, **font)
# #     ax[it].set_title(axes[set[0]-1] + '-' + axes[set[1]-1], **font)
# # ax[it].legend(('truth', 'estimates'))
# # plt.subplots_adjust(wspace=0.24)
# # # --- inertial orbits
# # title = 'Earth Centered Inertial Frame: ' + str(days) + ' Days'
# # # 3D
# # rcParams['axes.labelpad'] = 20
# # fig = plt.figure()
# # ax = fig.add_subplot(111, projection='3d')
# # ax.plot3D(X_estimate[:,0], X_estimate[:,1], X_estimate[:,2], label='propogated estimates')
# # ax.plot3D([0],[0],[0], '.g', label='Earth')
# # ax.set_title(title, **font)
# # ax.set_xlabel(axes[0] + ' [km]', **font)
# # ax.set_ylabel(axes[1] + ' [km]', **font)
# # ax.set_zlabel(axes[2] + ' [km]', **font)
# # ax.legend()
# # # --- orbit frame
axes = ['Radial', 'Along-track', 'Orbit Normal']
title = 'Deployer Orbit Frame'
# # 3D
rcParams['axes.labelpad'] = 20
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot3D(orbitFrameMeasurements[:,0]*1000, orbitFrameMeasurements[:,1]*1000, orbitFrameMeasurements[:,2]*1000, '.', label='centroids')
ax.plot3D(nlb.estimates[:,0]*1000, nlb.estimates[:,1]*1000, nlb.estimates[:,2]*1000, '.', label='estimates', color='orange')
ax.plot3D(true_orbit_frame[:,1]*1000, true_orbit_frame[:,2]*1000, true_orbit_frame[:,3]*1000, 'k', label='truth')
ax.set_title(title, **font)
ax.set_xlabel(axes[0] + ' [m]', **font)
ax.set_ylabel(axes[1] + ' [m]', **font)
ax.set_zlabel(axes[2] + ' [m]', **font)
ax.legend()
# # iso
# rcParams['axes.labelpad'] = 0
# fig, ax = plt.subplots(1,3)
# for it, set in enumerate(combinations([1,2,3],2)):
#     # spacecraft trajectory
#     ax[it].plot(nlb.estimates[:,set[0]-1]*1000, nlb.estimates[:,set[1]-1]*1000, '.', label='estimates')
#     ax[it].plot(orbitFrameMeasurements[:,set[0]-1]*1000, orbitFrameMeasurements[:,set[1]-1]*1000, '.', label='centroids')
#     ax[it].plot(true_orbit_frame[:,set[0]]*1000, true_orbit_frame[:,set[1]]*1000, 'k', label='truth')
#     ax[it].set_xlabel(axes[set[0]-1] + ' [m]', **font)
#     ax[it].set_ylabel(axes[set[1]-1] + ' [m]', **font)
#     # fig.suptitle(title, **font)
#     ax[it].set_title(axes[set[0]-1] + '-' + axes[set[1]-1], **font)
# ax[it].legend()
# plt.subplots_adjust(wspace=0.24)
# # # # --- Sensor frame
# # axes = ['Viewing Axis', 'Normal', 'Bi-Normal']
# # title = 'Sensor Frame'
# # # 3D
# # rcParams['axes.labelpad'] = 20
# # font = {'size':'15'}
# # fig= plt.figure()
# # ax = fig.add_subplot(111, projection='3d')
# # ax.plot3D(measurements[:,1]*1000, measurements[:,2]*1000, measurements[:,3]*1000, '.', label='centroids')
# # ax.plot3D(true_sensor_frame[:,1]*1000, true_sensor_frame[:,2]*1000, true_sensor_frame[:,3]*1000, 'k', label='truth')
# # ax.set_title(title, **font)
# # ax.set_xlabel(axes[0] + ' [m]', **font)
# # ax.set_ylabel(axes[1] + ' [m]', **font)
# # ax.set_zlabel(axes[2] + ' [m]', **font)
# # ax.legend()
# # # isometric
# # rcParams['axes.labelpad'] = 0
# # fig, ax = plt.subplots(1,3)
# # for it, set in enumerate(combinations([1,2,3],2)):
# #     # spacecraft trajectory
# #     ax[it].plot(measurements[:,set[0]]*1000, measurements[:,set[1]]*1000, '.', label='centroids')
# #     ax[it].plot(true_sensor_frame[:,set[0]]*1000, true_sensor_frame[:,set[1]]*1000, 'k', label='truth')
# #     ax[it].set_xlabel(axes[set[0]-1] + ' [m]', **font)
# #     ax[it].set_ylabel(axes[set[1]-1] + ' [m]', **font)
# #     fig.suptitle(title, **font)
# #     ax[it].set_title(axes[set[0]-1] + '-' + axes[set[1]-1], **font)
# # ax[it].legend()
# # plt.subplots_adjust(wspace=0.24)

plt.show()

































#
